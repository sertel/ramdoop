/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.apache.hadoop.examples.WordCount.IntSumReducer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.junit.Test;

import com.ramdoop.tiled.io.TiledInputFormat;

public class testJobTiling {

	private class TestFileSplit extends FileSplit {

		public TestFileSplit(Path file, long start, long length, String[] hosts) {
			super(file, start, length, hosts);
		}

		@Override
		public boolean equals(Object other) {
			FileSplit otherFileSplit = (FileSplit) other;
			try {
				return otherFileSplit.getLength() == getLength()
						&& otherFileSplit.getPath().equals(getPath())
						&& otherFileSplit.getStart() == getStart()
						&& Arrays.deepEquals(otherFileSplit.getLocations(), getLocations());
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
	}

	@Test
	public void testSerialization() {
		InputSplit[] splits = new InputSplit[] {
				new TestFileSplit(
						new Path(
								"hdfs://localhost:54310/HiBench/Wordcount/Input/part-00000"),
						0, 547828, new String[] { "141.76.44.154" }),
				new TestFileSplit(
						new Path(
								"hdfs://localhost:54310/HiBench/Wordcount/Input/part-00001"),
						0, 548022, new String[] { "141.76.44.154" }),
				new TestFileSplit(
						new Path(
								"hdfs://localhost:54310/HiBench/Wordcount/Input/part-00002"),
						0, 547765, new String[] { "141.76.44.154" }) };
		String serialized = TiledInputFormat.serializeSplits(Arrays
				.asList(splits));
		System.out.println(serialized);
		Assert.assertEquals(
				"[141.76.44.154]hdfs://localhost:54310/HiBench/Wordcount/Input/part-00000:0+547828|[141.76.44.154]hdfs://localhost:54310/HiBench/Wordcount/Input/part-00001:0+548022|[141.76.44.154]hdfs://localhost:54310/HiBench/Wordcount/Input/part-00002:0+547765",
				serialized);
		List<InputSplit> deserialized = TiledInputFormat
				.deserializeSplits(serialized);
		Assert.assertEquals(splits.length, deserialized.size());
		for(int i=0;i<splits.length;i++){
			splits[i].equals(deserialized.get(i));
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testTiledReducer() throws Throwable {
		JobConf conf = new JobConf();
		conf.set("mapreduce.reduce.class", IntSumReducer.class.getName());
		Mapper mapper = new Mapper<>();
		final Text _key = new Text();
		final IntWritable _value = new IntWritable();
		Context context = mapper.new Context(conf, new TaskAttemptID(), null, new RecordWriter<Text, IntWritable>() {
			@Override
			public void write(Text key, IntWritable value) throws IOException,
					InterruptedException {
				_key.set(key);
				_value.set(value.get());
			}

			@Override
			public void close(TaskAttemptContext context) throws IOException,
					InterruptedException {
				// nothing
			}
		}, null, null, null);
		TiledReducer reducer = new TiledReducer<>();
		reducer.setup(context);
		List<IntWritable> values = new ArrayList<>();
		values.add(new IntWritable(10));
		values.add(new IntWritable(11));
		reducer.map(new Text("hallo"), values, context);
		reducer.cleanup(context);
		
		Assert.assertEquals("hallo", _key.toString());
		Assert.assertEquals(21, _value.get());
	}
}
