/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled.lib;

import java.io.File;
import java.io.IOException;

import junit.framework.Assert;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFilter;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.junit.Ignore;
import org.junit.Test;

import com.ramdoop.tiled.api.DynamicMapFileOutputFormat;
import com.ramdoop.tiled.io.TiledInputFormat;
import com.ramdoop.tiled.io.TiledInputFormat.FilteredFileSplit;

public class testMapFile {

	@SuppressWarnings("rawtypes")
	@Test
	public void testFiltering() throws Throwable {
		String inputMapFile = "test/testMapFile/part-00095";
		String prVectorFile = "test/testMapFile/pagerank_init_vector.temp";

		Job job = new Job();

		// create a dummy split for the map file
		FilteredFileSplit split = new FilteredFileSplit(new Path(prVectorFile), 0, 100,
				new String[0]);
		FileSplit fSplit = new FileSplit(new Path(prVectorFile), 0, 100, new String[0]);
		TiledInputFormat format = new TiledInputFormat<>();
		job.getConfiguration().set(TiledInputFormat.ORIGINAL_INPUT_FORMAT,
				SequenceFileInputFormat.class.getName());
		SequenceFileInputFilter.setFilterClass(job, KeyRangeFilter.class);
		job.getConfiguration().set(TiledInputFormat.FILTERED_INPUTS, prVectorFile);
		FileInputFormat.setInputPaths(job, new Path(inputMapFile), new Path(prVectorFile));
		TaskAttemptContext context = new TaskAttemptContext(job.getConfiguration(),
				new TaskAttemptID());
		RecordReader reader = format.createRecordReader(split, context);
		reader.initialize(fSplit, context);
		long key = -1;
		int rounds = 0;
		while (reader.nextKeyValue()) {
			key = ((LongWritable) reader.getCurrentKey()).get();
			System.out.println("Key: " + key + " Value: " + reader.getCurrentValue());
			rounds++;
		}
		reader.close();
		Assert.assertEquals(1, rounds);
		Assert.assertEquals(0, key);
	}

	@Ignore
	// run this only to generate the pr vector
	@Test
	public void genPRVectorFile() throws Throwable {
		gen_initial_vector(50, new Path("test/testMapFile"));
	}

	private Configuration getConf() {
		return new Configuration();
	}

	// generate initial pagerank vector
	private void gen_initial_vector(int number_nodes, Path vector_path) throws IOException {
		int i, j = 0;
		int milestone = number_nodes / 10;
		String file_name = "pagerank_init_vector.temp";
		SequenceFile.Writer writer = new SequenceFile.Writer(FileSystem.get(getConf()), getConf(),
				new Path(vector_path.toString() + "/" + file_name), LongWritable.class, Text.class);
		// FileWriter file = new FileWriter(file_name);
		// BufferedWriter out = new BufferedWriter (file);

		System.out.print("Creating initial pagerank vectors...");
		double initial_rank = 1.0 / (double) number_nodes;

		for (i = 0; i < number_nodes; i++) {
			writer.append(new LongWritable(i), new Text(i + "\tv" + initial_rank + "\n"));
			// out.write(i + "\tv" + initial_rank +"\n");
			if (++j > milestone) {
				System.out.print(".");
				j = 0;
			}
		}
		// out.close();
		writer.close();
		System.out.println("");

		// copy it to curbm_path, and delete temporary local file.
		// final FileSystem fs = FileSystem.get(getConf());
		// fs.copyFromLocalFile( true, new Path("./" + file_name), new Path
		// (vector_path.toString()+ "/" + file_name) );
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void testDynamicMapFile() throws Throwable{
		File f = new File("test-output/testDynamicMapFile");
		f.delete();

		// create a map file
		Job job = new Job();
		FileOutputFormat.setOutputPath(job, new Path("test-output/testDynamicMapFile"));
		DynamicMapFileOutputFormat format = new DynamicMapFileOutputFormat();
		TaskAttemptContext cntxt = new TaskAttemptContext(job.getConfiguration(), new TaskAttemptID());
		RecordWriter writer = format.getRecordWriter(cntxt);
		for(int i=0;i<10;i++) writer.write(new LongWritable(1), new Text("bla"));
		writer.write(new LongWritable(5), new Text("bla"));
		for(int i=0;i<5;i++) writer.write(new LongWritable(10), new Text("bla"));
		writer.write(new LongWritable(15), new Text("bla"));
		writer.close(cntxt);
		
		// load the index of the file and make sure it has the three desired entries
		Object[] index = MapFileHelper.loadIndex(job.getConfiguration(), new Path("test-output/testDynamicMapFile/_temporary/_attempt__0000_r_000000_0/part-r-00000"));
		Assert.assertEquals(4, index[MapFileHelper.COUNT]);
		WritableComparable[] keys = (WritableComparable[]) index[MapFileHelper.KEYS];
		Assert.assertEquals(1, ((LongWritable)keys[0]).get());
		Assert.assertEquals(5, ((LongWritable)keys[1]).get());
		Assert.assertEquals(10, ((LongWritable)keys[2]).get());
		Assert.assertEquals(15, ((LongWritable)keys[3]).get());
	}
}
