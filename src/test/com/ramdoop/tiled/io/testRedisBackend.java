/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled.io;

import java.util.Arrays;
import java.util.List;

import org.apache.hadoop.io.DataInputBuffer;
import org.apache.hadoop.io.DataOutputBuffer;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.serializer.Deserializer;
import org.apache.hadoop.io.serializer.SerializationFactory;
import org.apache.hadoop.io.serializer.Serializer;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.junit.Assert;
import org.junit.Test;

import redis.clients.jedis.Jedis;

import com.ramdoop.tiled.io.RedisInputFormat.RedisInputSplit;
import com.ramdoop.tiled.io.RedisInputFormat.Strategy;

public class testRedisBackend {

	@SuppressWarnings("unused")
	private void writeData(int amountKeys, int amountValues) throws Throwable {
		writeData("hallo-", 0, amountKeys, amountValues);
	}

	private void writeData(String prefix, int startKeys, int amountKeys, int amountValues)
			throws Throwable {
		JobConf conf = new JobConf();
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(Text.class);
		conf.set("io.file.buffer.size", "5");
		conf.set("ramdoop.output.prefix", "http://127.0.0.");
		// we need the partitioner to return 1
		conf.setNumReduceTasks(1);
		conf.setInt("ramdoop.partition.diff", 1);

		// use default hash partitioner
		TaskAttemptContext context = new TaskAttemptContext(conf, new TaskAttemptID());

		RedisOutputFormat<Text, Text> outFormat = new RedisOutputFormat<>();
		RecordWriter<Text, Text> writer = outFormat.getRecordWriter(context);

		// dispatching inside Hadoop is done via the partitioner so the values
		// of one partition end up at one reducer.
		for (int i = startKeys; i < startKeys + amountKeys; i++)
			for (int j = 0; j < amountValues; j++) {
				writer.write(new Text(prefix + i), new Text("ballo-" + j));
				// System.out.println(prefix + i);
			}
		writer.close(context);
	}

	private void readData() throws Throwable {
		readData(Strategy.TILE_KEYS);
	}

	private void readData(Strategy strat) throws Throwable {
		readData(strat, 1000);
	}

	private void readData(Strategy strat, int batchSize) throws Throwable {
		JobConf conf = new JobConf();
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(Text.class);
		conf.setEnum(RedisInputFormat.STRATEGY, strat);
		conf.setInt(RedisReader.BATCH_SIZE, batchSize);
		conf.set(RedisInputFormat.INTERMEDIATE_TILE_LOCATION, "http://127.0.0.1");
		TaskAttemptContext context = new TaskAttemptContext(conf, new TaskAttemptID());

		RedisInputFormat<Text, Text> inFormat = new RedisInputFormat<>();
		List<InputSplit> splits = inFormat.getSplits(context);
		for (InputSplit inSplit : splits) {
			System.out.println(inSplit);
			RecordReader<Text, Text> reader = inFormat.createRecordReader(inSplit, context);
			while (reader.nextKeyValue()) {
				System.out.println("key: " + reader.getCurrentKey() + " values: "
						+ reader.getCurrentValue());
			}
		}

	}

	@Test
	public void testRedisSplitSerialization() throws Throwable {
		RedisInputSplit inSplit = new RedisInputSplit(new String[] { "http://127.0.0.1" }, 0, 200,
				"some-script-id");
		DataOutputBuffer outBuffer = new DataOutputBuffer(100);

		// serialize
		inSplit.write(outBuffer);

		byte[] written = outBuffer.getData();
		int pos = outBuffer.getLength();
		DataInputBuffer inBuffer = new DataInputBuffer();
		inBuffer.reset(written, pos);
		RedisInputSplit newSplit = new RedisInputSplit();

		// deserialize
		newSplit.readFields(inBuffer);

		Assert.assertEquals(0, newSplit.getStart());
		Assert.assertEquals(200, newSplit.getStop());
		Assert.assertEquals("127.0.0.1", newSplit.getLocations()[0]);
	}

	/**
	 * This test case requires 1 instance of redis running on localhost:6379
	 * 
	 * @throws Throwable
	 */
	@Test
	public void testRedisWriter() throws Throwable {

		Jedis jedis = new Jedis("127.0.0.1");
		jedis.select(0);
		jedis.flushDB();

		// (reduce) task needed
		JobConf conf = new JobConf();
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(Text.class);
		conf.setInt("ramdoop.root.job.id", 11); // becomes the DB (0-15
												// pre-exist. databases can not
												// be created but pre-exist per
												// the redis.conf.)
		conf.set("ramdoop.output.prefix", "http://127.0.0.");
		conf.setInt("ramdoop.partition.diff", 1);
		conf.setNumReduceTasks(2); // we need the partitioner to return 1
		// use default hash partitioner
		TaskAttemptContext context = new TaskAttemptContext(conf, new TaskAttemptID());

		RedisOutputFormat<Text, Text> outFormat = new RedisOutputFormat<>();
		RecordWriter<Text, Text> writer = outFormat.getRecordWriter(context);

		// dispatching inside Hadoop is done via the partitioner so the values
		// of one partition end up at one reducer.

		writer.write(new Text("hallo"), new Text("ballo"));
		writer.close(context);

		// Assertions
		SerializationFactory serializationFactory = new SerializationFactory(
				context.getConfiguration());

		Serializer<Text> keySerializer = serializationFactory.getSerializer(Text.class);
		DataOutputBuffer buffer = new DataOutputBuffer();
		keySerializer.open(buffer);
		keySerializer.serialize(new Text("hallo"));
		byte[] serializedKey = buffer.getData();
		serializedKey = Arrays.copyOfRange(serializedKey, 0, buffer.getLength());
		List<byte[]> values = jedis.lrange(serializedKey, 0, -1);
		// the 2 types and the value
		Assert.assertEquals(3, values.size());

		Deserializer<Text> valueDeserializer = serializationFactory.getDeserializer(Text.class);
		DataInputBuffer dataIn = new DataInputBuffer();
		valueDeserializer.open(dataIn);
		// byte[] test = new byte[] { 5, 98, 97, 108, 108, 111, 0, 0, 0, 0, 0,
		// 0,
		// 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		// dataIn.reset(test, test.length);
		// System.out.println();
		// for(byte s : values.get(2)) System.out.print(s + ", ");
		dataIn.reset(values.get(2), values.get(2).length);
		Text value = valueDeserializer.deserialize(null);
		Assert.assertEquals("ballo", value.toString());
		jedis.disconnect();
	}

	/**
	 * This test case assumes that the writer test case was executed before.
	 * 
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testRedisReader() throws Throwable {

		// (reduce) task needed
		JobConf conf = new JobConf();
		conf.setInt("ramdoop.root.job.id", 11); // becomes the DB (0-15
												// pre-exist. databases can not
												// be created but pre-exist per
												// the redis.conf.)
		conf.set(RedisInputFormat.INTERMEDIATE_TILE_LOCATION, "http://127.0.0.1");
		TaskAttemptContext context = new TaskAttemptContext(conf, new TaskAttemptID());

		RedisInputFormat<Text, Text> inFormat = new RedisInputFormat<>();
		List<InputSplit> splits = inFormat.getSplits(context);
		Assert.assertEquals(1, splits.size());
		RedisInputSplit inSplit = (RedisInputSplit) splits.get(0);
		Assert.assertEquals(0, inSplit.getStart());
		Assert.assertEquals(-1, inSplit.getStop());
		RecordReader<Text, Text> reader = inFormat.createRecordReader(inSplit, context);
		Assert.assertTrue(reader.nextKeyValue());
		Assert.assertEquals("hallo", reader.getCurrentKey().toString());
		Assert.assertEquals("ballo", ((List<Text>) reader.getCurrentValue()).get(0).toString());
	}

	/**
	 * Too bad but the scan function really is unpredictable and therefore not
	 * usable for us :( (Who the fuck needs such a function?!?!?!)
	 * 
	 * @throws Throwable
	 */
	@Test
	public void testLuaScan() throws Throwable {
		Jedis jedis = new Jedis("127.0.0.1");
		jedis.flushAll();
		jedis.scriptFlush();

		String scanScript = "local db = ARGV[1] or 11 " + "local count = ARGV[2] or 100 "
				+ "local s=0 " + "redis.call('SELECT', '1') "
				+ "if redis.call('EXISTS', 'scan_cursor') == 1 then "
				+ "s=redis.call('GET', 'scan_cursor') " + "end "
				+ "redis.call('SET', 'scan_cursor', s+count) " + "redis.call('SELECT', db) "
				+ "if s+count > redis.call('DBSIZE') then " + "return nil " + "else "
				+ "local result = redis.call('SCAN', s, 'COUNT', count) "
				+ "return redis.call('MGET', unpack(result[2])) " + "end";
		String scriptID = jedis.scriptLoad(scanScript);

		// String scriptID = RedisInputFormat.registerLUAScript(jedis).get(1);

		for (int i = 0; i < 100; i++)
			jedis.set("key" + i, "value" + i);

		// List results = new ArrayList<>();
		Object result = jedis.evalsha(scriptID, 0, "11", "20");
		while (result != null) {
			// results.addAll(((List) result));
			System.out.println(result);
			System.out.println(((List) result).size());
			result = jedis.evalsha(scriptID, 0, "11", "20");
		}

		jedis.disconnect();
	}

	@Test
	public void testTileKeysScan() throws Throwable {
		Jedis jedis = new Jedis("127.0.0.1");
		jedis.flushAll();
		jedis.scriptFlush();

		// List<String> scripts = RedisInputFormat.registerLUAScript(jedis);
		// for (int i = 20; i < 30; i++)
		// jedis.rpush(("test" + i), ("value" + i),
		// ("value" + i + "-x"));
		// for (int i = 0; i < 10; i++)
		// jedis.rpush(("key" + i), ("value" + i),
		// ("value" + i + "-x"));
		// System.out.println(jedis.evalsha(scripts.get(0), 0));
		// int batch = 20;
		// for (int i = 0; i < 100; i = i + batch)
		// System.out.println(jedis.evalsha(scripts.get(2), 0,
		// Integer.toString(i), Integer.toString(i + 20 - 1)));

		writeData("test-", 50, 10, 2);
		writeData("hallo-", 30, 10, 2);
		writeData("something-", 70, 10, 2);
		readData();
	}

	@Test
	public void testDBMoveScan() throws Throwable {
		Jedis jedis = new Jedis("127.0.0.1");
		jedis.flushAll();
		jedis.scriptFlush();

		writeData("test-", 50, 10, 2);
		writeData("hallo-", 30, 10, 2);
		writeData("something-", 70, 10, 2);
		readData(Strategy.DB_MOVE);
	}

	@Test
	public void testDBMoveScanMultiDB() throws Throwable {
		Jedis jedis = new Jedis("127.0.0.1");
		jedis.flushAll();
		jedis.scriptFlush();

		writeData("test-", 50, 10, 2);
		writeData("hallo-", 30, 10, 2);
		writeData("something-", 70, 10, 2);
		readData(Strategy.DB_MOVE, 10);
	}

	@Test
	public void testBenchmarking() throws Throwable {
		Jedis jedis = new Jedis("127.0.0.1");
		jedis.flushAll();
		jedis.scriptFlush();

		int count = 1000;
		writeData("test-", 0, count, 2);
		readData(Strategy.DB_MOVE, count / 10);
	}

	@Test
	public void testLongKey() throws Throwable {
		Jedis jedis = new Jedis("127.0.0.1");
		jedis.flushAll();
		jedis.scriptFlush();

		JobConf conf = new JobConf();
		conf.setOutputKeyClass(LongWritable.class);
		conf.setOutputValueClass(Text.class);
		conf.set("io.file.buffer.size", "5");
		conf.set("ramdoop.output.prefix", "http://127.0.0.");
		// we need the partitioner to return 1
		conf.setNumReduceTasks(1);
		conf.setInt("ramdoop.partition.diff", 1);

		// use default hash partitioner
		TaskAttemptContext context = new TaskAttemptContext(conf, new TaskAttemptID());

		RedisOutputFormat<LongWritable, Text> outFormat = new RedisOutputFormat<>();
		RecordWriter<LongWritable, Text> writer = outFormat.getRecordWriter(context);

		// dispatching inside Hadoop is done via the partitioner so the values
		// of one partition end up at one reducer.
		for (int j = 0; j < 10; j++) {
			writer.write(new LongWritable(j), new Text("ballo-" + j));
			// System.out.println(prefix + i);
		}
		writer.close(context);
	}

}
