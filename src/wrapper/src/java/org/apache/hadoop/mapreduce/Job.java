/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package org.apache.hadoop.mapreduce;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.hadoop.SubprocessExecution;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.InputFormat;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.JobID;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.OutputFormat;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * This is a wrapper for the old map/reduce API.
 * 
 * @author sertel
 * 
 */
public class Job extends JobContext {

	protected org.apache.hadoop.mapred.JobConf _config;

	public Job(org.apache.hadoop.conf.Configuration config) {
		this(config, null);
	}

	public Job(org.apache.hadoop.conf.Configuration config, String id) {
		super(config, new JobID());
		_config = new org.apache.hadoop.mapred.JobConf(config);
		_config.setJobName(id);
		System.out.println("Wrapper instantiated.");
	}

	public boolean waitForCompletion(boolean verbose) throws IOException,
			InterruptedException, ClassNotFoundException {
		return SubprocessExecution.getInstance().execute(_config);
	}

	/**
	 * WRAPPER API METHODS BELOW
	 */

	/**
	 * Set the number of reduce tasks for the job.
	 * 
	 * @param tasks
	 *            the number of reduce tasks
	 * @throws IllegalStateException
	 *             if the job is submitted
	 */
	public void setNumReduceTasks(int tasks) throws IllegalStateException {
		_config.setNumReduceTasks(tasks);
	}

	/**
	 * Set the current working directory for the default file system.
	 * 
	 * @param dir
	 *            the new current working directory.
	 * @throws IllegalStateException
	 *             if the job is submitted
	 */
	public void setWorkingDirectory(Path dir) throws IOException {
		_config.setWorkingDirectory(dir);
	}

	/**
	 * Set the {@link InputFormat} for the job.
	 * 
	 * @param cls
	 *            the <code>InputFormat</code> to use
	 * @throws IllegalStateException
	 *             if the job is submitted
	 */
	public void setInputFormatClass(Class<? extends InputFormat> cls)
			throws IllegalStateException {
		_config.setClass(JobContext.INPUT_FORMAT_CLASS_ATTR, cls,
				InputFormat.class);
	}

	/**
	 * Set the {@link OutputFormat} for the job.
	 * 
	 * @param cls
	 *            the <code>OutputFormat</code> to use
	 * @throws IllegalStateException
	 *             if the job is submitted
	 */
	public void setOutputFormatClass(Class<? extends OutputFormat> cls)
			throws IllegalStateException {
		_config.setClass(JobContext.OUTPUT_FORMAT_CLASS_ATTR, cls,
				OutputFormat.class);
	}

	/**
	 * Set the {@link Mapper} for the job.
	 * 
	 * @param cls
	 *            the <code>Mapper</code> to use
	 * @throws IllegalStateException
	 *             if the job is submitted
	 */
	public void setMapperClass(Class<? extends Mapper> cls)
			throws IllegalStateException {
		_config.setClass(JobContext.MAP_CLASS_ATTR, cls, Mapper.class);
	}

	/**
	 * Set the Jar by finding where a given class came from.
	 * 
	 * @param cls
	 *            the example class
	 */
	public void setJarByClass(Class<?> cls) {
		_config.setJarByClass(cls);
	}

	/**
	 * Set the combiner class for the job.
	 * 
	 * @param cls
	 *            the combiner to use
	 * @throws IllegalStateException
	 *             if the job is submitted
	 */
	public void setCombinerClass(Class<? extends Reducer> cls)
			throws IllegalStateException {
		_config.setClass(JobContext.COMBINE_CLASS_ATTR, cls, Reducer.class);
	}

	/**
	 * Set the {@link Reducer} for the job.
	 * 
	 * @param cls
	 *            the <code>Reducer</code> to use
	 * @throws IllegalStateException
	 *             if the job is submitted
	 */
	public void setReducerClass(Class<? extends Reducer> cls)
			throws IllegalStateException {
		_config.setClass(JobContext.REDUCE_CLASS_ATTR, cls, Reducer.class);
	}

	/**
	 * Set the key class for the job output data.
	 * 
	 * @param theClass
	 *            the key class for the job output data.
	 * @throws IllegalStateException
	 *             if the job is submitted
	 */
	public void setOutputKeyClass(Class<?> theClass)
			throws IllegalStateException {
		_config.setOutputKeyClass(theClass);
	}

	/**
	 * Set the value class for job outputs.
	 * 
	 * @param theClass
	 *            the value class for job outputs.
	 * @throws IllegalStateException
	 *             if the job is submitted
	 */
	public void setOutputValueClass(Class<?> theClass)
			throws IllegalStateException {
		_config.setOutputValueClass(theClass);
	}

	/**
	 * Return the configuration for the job.
	 * 
	 * @return the shared configuration object
	 */
	public Configuration getConfiguration() {
		return _config;
	}

	/**
	 * Set the key class for the map output data. This allows the user to
	 * specify the map output key class to be different than the final output
	 * value class.
	 * 
	 * @param theClass
	 *            the map output key class.
	 * @throws IllegalStateException
	 *             if the job is submitted
	 */
	public void setMapOutputKeyClass(Class<?> theClass)
			throws IllegalStateException {
		_config.setMapOutputKeyClass(theClass);
	}

	/**
	 * Set the value class for the map output data. This allows the user to
	 * specify the map output value class to be different than the final output
	 * value class.
	 * 
	 * @param theClass
	 *            the map output value class.
	 * @throws IllegalStateException
	 *             if the job is submitted
	 */
	public void setMapOutputValueClass(Class<?> theClass)
			throws IllegalStateException {
		_config.setMapOutputValueClass(theClass);
	}
	
	  /**
	   * Gets the counters for this job.
	   * 
	   * @return the counters for this job.
	   * @throws IOException
	   */
	  public Counters getCounters() throws IOException {
		return SubprocessExecution.getInstance().loadCounters();
	  }

	
}
