/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package org.apache.hadoop;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;

import javax.net.SocketFactory;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.retry.RetryPolicy;
import org.apache.hadoop.ipc.RPC;
import org.apache.hadoop.ipc.VersionedProtocol;
import org.apache.hadoop.security.UserGroupInformation;

/**
 * The proxy function changed from 1.1.1 to 1.2.0. So we need to load
 * reflectively.
 * 
 * @author sertel
 * 
 */
public class ProxyHelper {

	public static VersionedProtocol getProxy (
			Class<? extends VersionedProtocol> protocol, long clientVersion,
			InetSocketAddress addr, UserGroupInformation ticket,
			Configuration conf, SocketFactory factory, int rpcTimeout,
			RetryPolicy connectionRetryPolicy)  throws IOException {
		Method m = null;
		try {
			// version 1.1.x
			m = RPC.class.getDeclaredMethod("getProxy", Class.class,
					long.class, InetSocketAddress.class,
					UserGroupInformation.class, Configuration.class,
					SocketFactory.class, int.class, RetryPolicy.class);
			System.out.println("HADOOP version 1.1.x detected.");
			return (VersionedProtocol) m.invoke(protocol, clientVersion, addr,
					ticket, conf, factory, rpcTimeout, connectionRetryPolicy);

		} catch (NoSuchMethodException e) {
			System.out.println("HADOOP version 1.2.x detected.");
			// version 1.2.x
			try {
				m = RPC.class.getDeclaredMethod("getProxy", Class.class,
						long.class, InetSocketAddress.class,
						UserGroupInformation.class, Configuration.class,
						SocketFactory.class, int.class, RetryPolicy.class,
						boolean.class);
				return (VersionedProtocol) m.invoke(protocol, clientVersion,
						addr, ticket, conf, factory, rpcTimeout,
						connectionRetryPolicy, false);
			} catch (Exception e1) {
				System.out.println("RECEIVED WEIRD EXCEPTION!");
				e1.printStackTrace();
				throw new RuntimeException(e1);
			}
		} catch (Exception e) {
			System.out.println("RECEIVED WEIRD EXCEPTION!2");
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
