/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package org.apache.hadoop;

import java.util.List;

import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Counters;

public class AccumulatedCounters extends Counters {
	private List<Counters> _miniJobCounters = null;
	
	private class PCounter extends Counter{
		PCounter(String key, String displayName){
			super(key, displayName);
		}
	}

	public AccumulatedCounters(List<Counters> miniJobCounters) {
		_miniJobCounters = miniJobCounters;
	}

	public synchronized long getCounter(@SuppressWarnings("rawtypes") Enum key) {
		long acc = 0;
		for (Counters miniJobCounter : _miniJobCounters) {
			System.out.println("Counter: " + key + " Value: "
					+ miniJobCounter.findCounter(key).getValue());
			acc += miniJobCounter.findCounter(key).getValue();
		}
		return acc;
	}

	@Override
	public synchronized Counter findCounter(Enum<?> key) {
		Counter c = new PCounter(key.name(), key.name());
		c.setValue(getCounter(key));
		return c;
	}

}
