/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package org.apache.hadoop;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;

public interface IExecutor {
	public boolean execute(Configuration config) throws IOException;
}
