/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package org.apache.hadoop;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.RunningJob;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.JobID;

import com.ramdoop.tiled.JobExecutor;
import com.ramdoop.tiled.JobExecutor.ExecutionStrategy;

public class SubprocessExecution implements IExecutor {

	public static String JOB_LIST = "ramdoop.tiled.job.list";

	private Set<JobID> _miniJobs = null;

	private List<Configuration> _gathered = new ArrayList<>();
	
	private static SubprocessExecution _me = new SubprocessExecution();

	public static SubprocessExecution getInstance() {
		return _me;
	}

	private SubprocessExecution() {
		// singleton
	}

	public Set<JobID> getMiniJobs() {
		return _miniJobs;
	}

	public boolean execute(Configuration config) throws IOException {
		System.out.println("RAMdoop received job configuration ...");
		switch (config.getEnum(JobExecutor.EXECUTION_STRATEGY, ExecutionStrategy.SINGLE)) {
			case MULTI:
				String[] jobs = config.get(JOB_LIST).split("\\|");
				System.out.println("Job list: " + Arrays.deepToString(jobs));
				if(_gathered.size() == jobs.length) _gathered.clear(); // another round or just a different job
				String jobName = config.get("mapred.job.name");
				System.out.println("Submitted job: " + jobName);
				Set<String> s = new HashSet<>();
				Collections.addAll(s, jobs);
				if (s.contains(jobName)) {
					_gathered.add(config);
					if (_gathered.size() == jobs.length) {
						return forkComputation(_gathered);
					} else {
						System.out.println("Delaying job execution.");
						// delay execution
						return true;
					}
				} else {
					return forkComputation(Collections.singletonList(config));
				}
			case PURE:
			case SINGLE:
				return forkComputation(Collections.singletonList(config));
		}
		throw new RuntimeException("Impossible!");
	}

	protected boolean forkComputation(List<Configuration> configs) throws IOException {
		System.out.println("Forking RAMdoop computation...");
		File result = prepareResultFile();
		List<String> serializedConfigs = new ArrayList<>();
		for (Configuration config : configs) {
			config.set("ramdoop.result.file", result.getAbsolutePath());
			serializedConfigs.add(serializeConfig(config));
		}

		ProcessBuilder builder = new ProcessBuilder(prepareCommand(serializedConfigs));
		builder.redirectErrorStream(true);
		// execute
		Process p = builder.start();
		drainIO(p);
		try {
			p.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		_miniJobs = readResults(result);

		return p.exitValue() == 0;
	}

	private String[] prepareCommand(List<String> confs) {
		ClassLoader cl = ClassLoader.getSystemClassLoader();
		URL[] urls = ((URLClassLoader) cl).getURLs();
		StringBuffer cp = new StringBuffer();
		for (URL url : urls) {
			if (!url.getFile().contains("RAMdoop-wrapper")) {
				cp.append(url.getFile() + ":");
				// System.out.println(url.getFile());
			}
		}
		cp.deleteCharAt(cp.length() - 1);
		// System.out.println("Constructed classpath: " + cp);

		String[] javaPrelude = new String[] { "java", "-cp", cp.toString(),
				JobExecutor.class.getCanonicalName() };
		String[] result = Arrays.copyOf(javaPrelude, javaPrelude.length + confs.size());
		System.arraycopy(confs.toArray(), 0, result, javaPrelude.length, confs.size());

		return result;
	}

	private void drainIO(Process p) {
		drainIO(p.getInputStream());
	}

	private void drainIO(final InputStream is) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				String line = null;
				try {
					while ((line = br.readLine()) != null)
						System.out.println(line);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
	}

	private String serializeConfig(Configuration config) throws IOException {
		// System.out.println("mapred.child.java.opts: (before)" +
		// config.get("mapred.child.java.opts"));
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(bout);
		config.write(out);
		out.close();
		// return bout.toString("UTF-8");//.getBytes(Charset.forName("UTF-8"))

		// return bout.toByteArray();

		File f = File.createTempFile("original_config_" + config.get("mapred.job.name"), "tmp");
		f.deleteOnExit();
		FileUtils.writeByteArrayToFile(f, bout.toByteArray());
		System.out.println("Serialized config: " + f.toURI().getPath());
		return f.toURI().getPath();
	}

	private File prepareResultFile() throws IOException {
		File f = File.createTempFile("ramdoop_result", "tmp");
		f.deleteOnExit();
		return f;
	}

	protected static Set<JobID> readResults(File result) throws IOException {
		Set<JobID> miniJobs = new HashSet<JobID>();
		String jobIDs = FileUtils.readFileToString(result);
		System.out.println(jobIDs);
		String[] miniJobIDs = jobIDs.split("\\|");
		for (String miniJobID : miniJobIDs) {
			System.out.println("Mini job ID: " + miniJobID);
			miniJobs.add(JobID.forName(miniJobID));
		}
		return miniJobs;
	}

	public Counters loadCounters() throws IOException {
		JobClient client = new JobClient();
		client.init(new JobConf(_gathered.get(0)));
		List<Counters> counters = new ArrayList<Counters>();
		for (JobID miniJob : _miniJobs) {
			RunningJob j = client.getJob(new org.apache.hadoop.mapred.JobID(miniJob
					.getJtIdentifier(), miniJob.getId()));
			counters.add(new Counters(j.getCounters()));
		}
		AccumulatedCounters counter = new AccumulatedCounters(counters);
		return counter;
	}
}
