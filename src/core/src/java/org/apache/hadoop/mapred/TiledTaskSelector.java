package org.apache.hadoop.mapred;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * In order to enable this we need to fix the following bug in Hadoop:
 * 
 * https://issues.apache.org/jira/browse/MAPREDUCE-4849
 * 
 * @author sertel
 * 
 */
// The PoolSchedulable iterates over all jobs in the pool until either of these
// returns a task to be executed. The problem is that this class has no
// knowledge whether later on there comes a job that runs local to me.
public class TiledTaskSelector extends DefaultTaskSelector {

	public static String TILE_PRIMARY = "ramdoop.tiled.local.primary";
	public static String SENTINEL = "ramdoop.tiled.sentinel";
	public static String LOGGING = "ramdoop.tiled.logging";
	private static int LOG = 0;
	private static int OUT = 1;

	/**
	 * We are interested to understand what are the currently running jobs.
	 * 
	 * @author sertel
	 * 
	 */
	private class TiledJobInProgressListener extends JobInProgressListener {

		@Override
		public void jobAdded(JobInProgress job) throws IOException {
			assignJobToPreferredHost(job);
			log(getClass().getCanonicalName(), ": new job registered! (", job.getJobConf().getJobName(), ")");
		}

		@Override
		public void jobRemoved(JobInProgress job) {
			// this callback seems not to be used by the Hadoop framework
			removeJob(job);
			log(getClass().getCanonicalName(), ": job removed. (", job.getJobConf().getJobName(), ")");
		}

		@Override
		public void jobUpdated(JobChangeEvent event) {
			// this API is a bit strange but there is only one concrete type of
			// this event, so the cast is ok.
			JobStatusChangeEvent changeEvent = (JobStatusChangeEvent) event;
			switch (changeEvent.getEventType()) {
				case PRIORITY_CHANGED:
					break;
				case RUN_STATE_CHANGED:
					if (changeEvent.getNewStatus().getRunState() == JobStatus.SUCCEEDED
							|| changeEvent.getNewStatus().getRunState() == JobStatus.KILLED
							|| changeEvent.getNewStatus().getRunState() == JobStatus.FAILED) {
						jobRemoved(event.getJobInProgress());
					}
					break;
				case START_TIME_CHANGED:
					break;
				default:
					break;

			}
		}
	}

	// host -> jobs
	// TODO make it a concurrent hash map just for safety's sake
	private Map<String, Set<JobID>> _runningJobs = new HashMap<String, Set<JobID>>();

	private final JobID _sentinelID = new JobID("sentinel", -1);
	
	private int _logging = -1;

	private void assignJobToPreferredHost(JobInProgress job) {
		String host = job.getJobConf().get(TILE_PRIMARY);
		if (host == null) {
			// none of our job. the below implementation will fall back to the
			// default one.
		} else {
			log(getClass().getCanonicalName(), ": primary tiling host: ", host);
			registerJob(host, job.getJobID());
			
			// atomic tile submission
			String gang = job.getJobConf().get(SENTINEL, null);
			if(gang != null){
				log(getClass().getCanonicalName(), "Found sentinel: ", gang);
				// remove the brackets: [...]
				gang = gang.substring(1, gang.length()-1);
				String[] hosts = gang.split(", ");
				for(String h : hosts){
					registerJob(h.trim(), _sentinelID);
				}
			}else{
				_runningJobs.get(host).remove(_sentinelID);
			}
		}
	}
	
	private void registerJob(String host, JobID jobID){
		log(getClass().getCanonicalName(), "Registering: ", host, " ", jobID.toString());
		if (!_runningJobs.containsKey(host)) {
			_runningJobs.put(host, new HashSet<JobID>());
		}
		_runningJobs.get(host).add(jobID);
	}

	private void removeJob(JobInProgress job) {
		String host = job.getJobConf().get(TILE_PRIMARY);
		if (host != null) {
			_runningJobs.get(host).remove(job.getJobID());
		}
	}

	/**
	 * Do all preparation here.
	 */
	public void start() throws IOException {
		super.start();

		_logging = super.conf.getInt(LOGGING, 0);

		log("Starting TiledTaskSelector ...");

		// track job submissions
		super.taskTrackerManager
				.addJobInProgressListener(new TiledJobInProgressListener());
		log("Job listener added.");
	}

	public Task obtainNewMapTask(TaskTrackerStatus taskTracker,
			JobInProgress job, LocalityLevel localityLevel) throws IOException {
		// TODO if multiple mini jobs are assigned to the same node than make
		// sure that we select a task from the mini job that is currently
		// executed.
		log(getClass().getCanonicalName(), "- (" + job.getJobConf().getJobName() + ")", ": obtaining new map task for host ", taskTracker.getHost(), " ...");
		log(getClass().getCanonicalName(), "- (" + job.getJobConf().getJobName() + ")", ": trying host: "
				+ taskTracker.getHost());
		// FIXME Jobs are not submitted atomically together. None-locality is produced when the first job starts scheduling although the others are not registered yet!
		if (_runningJobs.containsKey(taskTracker.getHost())
				&& !_runningJobs.get(taskTracker.getHost()).isEmpty()) {
			if (_runningJobs.get(taskTracker.host).contains(job.getJobID())) {
				// this is a local job. run the selection.
				log(getClass().getCanonicalName(), "- (", job.getJobConf().getJobName(), ")", "local task");
				return super.obtainNewMapTask(taskTracker, job, localityLevel);
			} else {
				// wait until the superior loop (PoolSchedulable.assignTask())
				// selects a job that can be executed locally.
				log(getClass().getCanonicalName(), "- (", job.getJobConf().getJobName() + ")", "reject");
				return null;
			}
		} else {
			// no node local tasks available anymore
			log(getClass().getCanonicalName(), "- (", job.getJobConf().getJobName(), ")", "no node local tasks available anymore");
			return super.obtainNewMapTask(taskTracker, job, localityLevel);
		}
	}

	/**
	 * Locality for reduce tasks is implemented here.
	 */
	@Override
	public Task obtainNewReduceTask(TaskTrackerStatus taskTracker,
			JobInProgress job) throws IOException {
		log(getClass().getCanonicalName(), "- (", job.getJobConf().getJobName(), ")", ": obtaining new reduce task for host ", taskTracker.getHost(), " ...");
		if (_runningJobs.containsKey(taskTracker.getHost())
				&& !_runningJobs.get(taskTracker.getHost()).isEmpty()) {
			if (_runningJobs.get(taskTracker.host).contains(job.getJobID())) {
				// this is a local job. run the selection.
				log(getClass().getCanonicalName(), "- (", job.getJobConf().getJobName(), ")", "local task");
				Task t = super.obtainNewReduceTask(taskTracker, job);
//				if (t != null) {
//					boolean isNodeGroupAware = conf.getBoolean(
//							"net.topology.nodegroup.aware", false);
//					LocalityLevel localityLevel = LocalityLevel.fromTask(job,
//							t, taskTracker, isNodeGroupAware);
//					log("Found locality level for task: " + localityLevel);
//				}
				return t;
			} else {
				// wait until the superior loop (PoolSchedulable.assignTask())
				// selects a job that can be executed locally.
				log(getClass().getCanonicalName(), "- (", job.getJobConf().getJobName(), ")", "reject");
				return null;
			}
		} else {
			// no node local tasks available anymore
			log(getClass().getCanonicalName(), "- (", job.getJobConf().getJobName(), ")", "no node local tasks available anymore");
			return super.obtainNewReduceTask(taskTracker, job);
		}
	}

	private void log(String... msg) {
		if (_logging == LOG) {
			// printed to jobtracker log file
			FairScheduler.LOG.info(convert(msg));
		} else if (_logging == OUT) {
			// printed to jobtracker out file
			System.out.println(convert(msg));
		}
	}
	
	private String convert(String[] msg){
		StringBuffer buffer = new StringBuffer();
		for(String s : msg){
			buffer.append(s);
		}
		return buffer.toString();
	}
}
