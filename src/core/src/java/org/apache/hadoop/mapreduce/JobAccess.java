/*
 * Copyright (c) Sebastian Ertel 2013-2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package org.apache.hadoop.mapreduce;

/**
 * There exist cases where we need to parameter names protected in the Job class.
 * @author sebastianertel
 *
 */
public abstract class JobAccess {
	public static String mapClass(){return Job.MAP_CLASS_ATTR;}
	public static String reduceClass(){return Job.REDUCE_CLASS_ATTR;}
	public static String combineClass(){return Job.COMBINE_CLASS_ATTR;}
	public static String inputFormat() {return Job.INPUT_FORMAT_CLASS_ATTR;}
	public static String outputFormat() {return Job.OUTPUT_FORMAT_CLASS_ATTR;}
}
