/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package org.apache.hadoop.mapreduce;

import java.io.IOException;

import org.apache.hadoop.mapreduce.Reducer.Context;

/**
 * Increase the visibility of the internal methods to drive this reducer from
 * the outside.
 * 
 * @author sertel
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class ManualReducer {
	public static void setup(Reducer reducer, Context context) throws IOException,
			InterruptedException {
		reducer.setup(context);
	}

	public static <KEYIN, VALUEIN> void reduce(Reducer reducer, KEYIN key, Iterable<VALUEIN> values, Context context)
			throws IOException, InterruptedException {
		reducer.reduce(key, values, context);
	}

	public static void cleanup(Reducer reducer, Context context) throws IOException,
			InterruptedException {
		reducer.cleanup(context);
	}
}
