/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.input;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import com.ramdoop.input.DataSource.DataImport;
import com.ramdoop.input.DataSource.DataReader;

public abstract class TextTransfer {

	public static class Reader extends DataReader {
		private long _chunkSize = 0;
		private FSDataInputStream _inputStream = null;

		@Override
		public void initialize(FileSystem targetFS, Path fh, long chunkSize)
				throws IOException {
			_inputStream = targetFS.open(fh, (int) chunkSize);
			_chunkSize = chunkSize;
		}

		@Override
		public Object getData() throws IOException {
			ByteBuffer buf = ByteBuffer.allocate((int) _chunkSize);
			int read = _inputStream.read(buf.array());
			if (read < 0)
				return null;
			else {
				buf.limit(read);
				return buf;
			}
		}

		@Override
		public void closeReader() throws IOException {
			_inputStream.close();
		}
	}

	public static class Writer extends DataImport {

		private WritableByteChannel _channel = null;
		private FSDataOutputStream _outputStream = null;

		public Writer(DataReader reader) {
			super(reader);
		}

		public Path[] recycle(FileSystem targetFS, String newInputFileName) throws IOException {
			Path[] newFile = super.recycle(targetFS, newInputFileName);
			assert newFile.length == 1;
			if (_channel != null) {
				close();
			}
			_outputStream = targetFS.create(newFile[0]);
			_channel = Channels.newChannel(_outputStream);
			return newFile;
		}

		@Override
		public long write(Object data) throws IOException {
			long bytesWritten = ((ByteBuffer) data).remaining();
			_channel.write((ByteBuffer) data);
			return bytesWritten;
		}

		@Override
		public void close() throws IOException {
			_channel.close();
			_outputStream.close();
		}
	}
}
