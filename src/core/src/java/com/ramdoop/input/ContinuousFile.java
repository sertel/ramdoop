/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.input;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.JobContext;

/**
 * This is meant to be used when the data is not yet located in the HDFS!
 * @author sertel
 *
 */
public class ContinuousFile extends DataSource {

	private static Map<Class<?>, Class<?>> _supportedInputFormats = new HashMap<Class<?>, Class<?>>();
	private static Map<Class<?>, Class<?>> _supportedOutputFormats = new HashMap<Class<?>, Class<?>>();
	static {
		_supportedInputFormats.put(
				org.apache.hadoop.mapred.TextInputFormat.class,
				TextTransfer.Reader.class);
		_supportedInputFormats.put(
				org.apache.hadoop.mapreduce.lib.input.TextInputFormat.class,
				TextTransfer.Reader.class);
		_supportedInputFormats.put(
				org.apache.hadoop.mapred.SequenceFileInputFormat.class,
				SequenceFileTransfer.Reader.class);
		_supportedInputFormats
				.put(org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat.class,
						SequenceFileTransfer.Reader.class);

		_supportedOutputFormats.put(TextTransfer.Reader.class,
				TextTransfer.Writer.class);
		_supportedOutputFormats.put(SequenceFileTransfer.Reader.class,
				SequenceFileTransfer.Writer.class);
	}

	protected static final String MR_OUTPUT = "ramdoop.continuous.mrout";

	private Path _file = null;
	private DataReader _reader = null;

	private long _size = 0;

	public ContinuousFile(String file, long chunkSize) {
		this(new Path(file), chunkSize);
	}

	public ContinuousFile(Path file, long chunkSize) {
		_file = file;
		_size = chunkSize;
	}

	public void initialize(final Configuration config) throws IOException {
		// the input format is detected via the config. but we have to check for
		// both versions: "mapred.input.format.class" and
		// "mapreduce.inputformat.class"
		JobConf conf = new JobConf(config);
		JobContext context = new JobContext(config, null);
		String inputFormat = conf.getRaw("mapred.input.format.class");
		Class<?> input = null;
		if (inputFormat == null) {
			try {
				input = context.getInputFormatClass();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}else
		{
			input = conf.getInputFormat().getClass();
		}
		Class<?> cls = _supportedInputFormats.get(input);
		if (cls == null) {
			throw new RuntimeException("Input format '"
					+ conf.getInputFormat().getClass() + "' not supported.");
		}

		try {
			_reader = (DataReader) cls.newInstance();
		} catch (Exception e) {
			throw new RuntimeException("Impossible exception occured: ", e);
		}

		_reader.initialize(_file.getFileSystem(config), _file, _size);
	}

	@Override
	public Object getData() throws IOException {
		return _reader.getData();
	}

	@Override
	public void close() {
		System.out.println("Input source '" + _file + "' closed.");
		try {
			_reader.closeReader();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public DataTransfer createDataTransfer() {
		Class<?> cls = _supportedOutputFormats.get(_reader.getClass());
		if (cls == null) {
			throw new RuntimeException("Input format '" + _reader.getClass()
					+ "' not supported.");
		}
		System.out
				.println("Data reader class: " + _reader.getClass().getName());
		System.out.println("Data transfer class: " + cls.getName());
		try {
			return (DataTransfer) cls.getConstructor(DataReader.class)
					.newInstance(_reader);
		} catch (Exception e) {
			throw new RuntimeException("Impossible exception occured: " + cls,
					e);
		}
	}
}
