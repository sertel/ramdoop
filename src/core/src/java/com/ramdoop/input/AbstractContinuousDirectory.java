/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.input;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;

public abstract class AbstractContinuousDirectory extends DataSource {

	protected Path _dir = null;
	protected long _size = 0;

	protected FileSystem _fs = null;
	protected List<Path> _files = null;

	protected Configuration _config = null;

	public AbstractContinuousDirectory(Path dir, long chunkSize) {
		_dir = dir;
		_size = chunkSize;
	}

	public void initialize(Configuration config) throws IOException {
		_fs = FileSystem.get(config);
		_config = config;

		_files = collectFiles(_fs, _dir, new PathFilter() {
			@Override
			public boolean accept(Path path) {
				// don't collect meta data files!
				return !path.getName().startsWith(".")
						&& !path.getName().startsWith("_");
			}
		});
	}
	
	public static List<Path> collectFiles(FileSystem fs, Path p,
			PathFilter filter) throws IOException {
		List<Path> paths = new ArrayList<Path>();
		FileStatus[] files = fs.listStatus(p, filter);
		for (FileStatus file : files) {
			if (file.isDir()) {
				paths.addAll(collectFiles(fs, file.getPath(), filter));
			} else {
				if (!file.getPath().getName().startsWith("_"))
					paths.add(file.getPath());
			}
		}
		return paths;
	}

}
