/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.input;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;

public class ContinuousDirectory extends AbstractContinuousDirectory {

	private ContinuousFile _currentFile = null;

	public ContinuousDirectory(Path dir, long chunkSize) {
		super(dir, chunkSize);
	}

	public void initialize(Configuration config) throws IOException {
		super.initialize(config);
		createNewContinuousFile();
	}

	public static void print(Path[] files, Configuration config) {
		try {
			// debug info
			for (Path file : files) {
				System.out.println("FOUND FILE: "
						+ file
						+ " size: "
						+ file.getFileSystem(config).getFileStatus(file)
								.getLen());
			}
		} catch (Exception e) {
			// just ignore
		}
	}

	private void createNewContinuousFile() throws IOException {
		_currentFile = new ContinuousFile(_files.remove(0), _size);
		_currentFile.initialize(_config);
	}

	@Override
	public Object getData() throws IOException {
		Object data = _currentFile.getData();
		// this needs to be a loop to cope with empty result files that might
		// exist.
		while (data == null && !_files.isEmpty()) {
			close();
			createNewContinuousFile();
			data = _currentFile.getData();
		}
		return data;
	}

	@Override
	public void close() {
		_currentFile.close();
	}

	@Override
	public DataTransfer createDataTransfer() {
		return _currentFile.createDataTransfer();
	}

}
