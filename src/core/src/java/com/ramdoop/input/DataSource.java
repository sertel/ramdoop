/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.input;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

/**
 * 
 * @author sertel
 * 
 */
public abstract class DataSource {

	static abstract class DataReader {

		protected DataReader() {
			// nothing
		}

		abstract void initialize(FileSystem targetFS, Path fh, long chunkSize)
				throws IOException;

		abstract Object getData() throws IOException;

		abstract void closeReader() throws IOException;
	}

	public static abstract class DataTransfer {

		protected DataTransfer(DataReader reader) {
			// nothing
		}

		/**
		 * 
		 * @param targetFS
		 * @param newInputFile
		 *            Suggestion for a new input file.
		 * @throws IOException
		 */
		abstract public Path[] recycle(FileSystem targetFS, String newInputFile)
				throws IOException;

		/**
		 * 
		 * @param data
		 * @return number of bytes written.
		 * @throws IOException
		 */
		abstract public long write(Object data) throws IOException;

		abstract public void close() throws IOException;
	}

	public static abstract class DataImport extends DataTransfer {

		protected DataImport(DataReader reader) {
			super(reader);
		}

		public Path[] recycle(FileSystem targetFS, String newInputFileName)
				throws IOException {
			// create the input file
			Path fh = new Path(newInputFileName);
			if (targetFS.exists(fh))
				targetFS.delete(fh, true);
			return new Path[] { fh };
		}
	}

	private String _id = null;

	/**
	 * This function will be called by the framework in order to assign an
	 * identifier to a data source.
	 */
	public final void assignId(String id) {
		_id = id;
	}

	public String getId() {
		return _id;
	}

	public abstract void initialize(Configuration config) throws IOException;

	public abstract DataTransfer createDataTransfer();

	/**
	 * This call must be realized by every implementation as a non-blocking
	 * call.
	 * 
	 * @return
	 */
	public abstract Object getData() throws IOException;

	/**
	 * An indication that this resource is no longer being drained.
	 */
	public abstract void close();

	public static DataSource classify(Configuration config, String inputPath,
			long jobInputSize) throws IOException {
		Path p = new Path(inputPath);
		return classify(config, p, jobInputSize);
	}

	public static DataSource classify(Configuration config, Path p,
			long jobInputSize) throws IOException {
		boolean isInHDFS = p.toUri().getScheme().equals("hdfs");
		if (isInHDFS) {
			System.out.println("HDFS input detected!");
			return new ContinuousHDFSDirectory(p, jobInputSize);
		} else {
			boolean isDir = p.getFileSystem(config).getFileStatus(p).isDir();
			if (isDir) {
				return new ContinuousDirectory(p, jobInputSize);
			} else {
				return new ContinuousFile(p, jobInputSize);
			}
		}
	}

}
