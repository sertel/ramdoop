/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.input;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class ContinuousHDFSDirectory extends AbstractContinuousDirectory {

	private class CompleteFileDataTransfer extends DataTransfer {
		protected CompleteFileDataTransfer(DataReader reader) {
			super(reader);
		}

		@Override
		public Path[] recycle(FileSystem targetFS, String newInputFileName)
				throws IOException {
			return _currentJobData;
		}

		@Override
		public long write(Object data) throws IOException {
			assert data == _currentJobData;
			return _currentJobSize;
		}

		@Override
		public void close() throws IOException {
			// nothing to be closed
		}
	}

	private long _currentJobSize = 0;
	private Path[] _currentJobData = null;
	private long _jobSize = 0;

	public ContinuousHDFSDirectory(Path dir, long chunkSize) {
		super(dir, chunkSize);
		_jobSize = chunkSize;
	}

	@Override
	public DataTransfer createDataTransfer() {
		// no reader required
		return new CompleteFileDataTransfer(null);
	}

	@Override
	public Object getData() throws IOException {
		Iterator<Path> it = _files.iterator();
		long jobSize = 0;
		List<Path> jobInput = new ArrayList<Path>();
		while (it.hasNext() && jobSize < _jobSize) {
			Path p = it.next();
			jobSize += p.getFileSystem(_config).getContentSummary(p)
					.getLength();
			jobInput.add(p);
			it.remove();
		}
		_currentJobSize = jobSize;
		_currentJobData = jobInput.toArray(new Path[jobInput.size()]);
		if (jobSize == 0)
			return null;
		else
			return _currentJobData;
	}

	@Override
	public void close() {
		// nothing to close
	}
}
