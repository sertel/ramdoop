/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.input;

import java.io.IOException;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Writable;

import com.ramdoop.input.DataSource.DataImport;
import com.ramdoop.input.DataSource.DataReader;

public abstract class SequenceFileTransfer {

	/*
	 * Reader side starts here:
	 */
	public static class Reader extends DataReader {
		private SequenceFile.Reader _reader = null;
		private int _headerSize = 0;

		private Writable _key = null;
		private Writable _value = null;

		@Override
		public void initialize(FileSystem targetFS, Path fh, long chunkSize)
				throws IOException {
			_reader = new SequenceFile.Reader(targetFS, fh, targetFS.getConf());
			_headerSize = (int) _reader.getPosition();

			try {
				_key = (Writable) _reader.getKeyClass().newInstance();
				_value = (Writable) _reader.getValueClass().newInstance();
			} catch (Exception e) {
				throw new IOException(e);
			}
		}

		public Object getData() throws IOException {
			long pos = _reader.getPosition() - _headerSize;
			_headerSize = 0;
			if (_reader.next(_key, _value)) {
				return new Object[] { _key, _value,
						(_reader.getPosition() - pos) };
			} else {
				return null;
			}
		}

		@Override
		public void closeReader() throws IOException {
			_reader.close();
		}
	}

	/*
	 * Writer side starts here:
	 */

	public static class Writer extends DataImport {

		private SequenceFile.Writer _writer = null;

		private Class<?> _key = null;
		private Class<?> _value = null;

		public Writer(DataReader reader) {
			super(reader);
			_key = ((Reader) reader)._reader.getKeyClass();
			_value = ((Reader) reader)._reader.getValueClass();
		}

		@Override
		public long write(Object data) throws IOException {
			_writer.append((Writable) ((Object[]) data)[0],
					(Writable) ((Object[]) data)[1]);
			long written = (Long) ((Object[]) data)[2];
//			System.out.println("Bytes written: " + written);
			return written;
		}

		@Override
		public Path[] recycle(FileSystem targetFS, String newInputFileName) throws IOException {
			Path[] newFile = super.recycle(targetFS, newInputFileName);
			assert newFile.length == 1;
			close();
			_writer = new SequenceFile.Writer(targetFS, targetFS.getConf(), newFile[0],
					_key, _value);
			return newFile;
		}

		@Override
		public void close() throws IOException {
			if (_writer != null) {
				_writer.close();
			}
		}
	}	
}
