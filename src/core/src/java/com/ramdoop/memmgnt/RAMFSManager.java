/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.memmgnt;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;

/**
 * This component is responsible for the management of all ram fs that were
 * created on this node.
 * 
 * @author sertel
 * 
 */
// FIXME This component is not clear anymore because the RAMFSClusterManager
// already mounts the ram folders. Maybe the RAMFSClusterManager should spawn
// one of these guys on each of the cluster nodes. Once we make further
// progress, we shall see the exact purpose of this component.
public class RAMFSManager implements IRAMFSStatistics {

	private class RAMFSInfo {
		// long _size = 0;
	}

	private Map<String, RAMFSInfo> _mounted = new HashMap<String, RAMFSInfo>();
	private Configuration _config = null;

	public RAMFSManager(Configuration config) {
		_config = config;
	}

	public void mountRamFolder(String path) throws IOException {
		if (!_mounted.containsKey(path)) {
			RAMFS.createRAMFolder(path);
			_mounted.put(path, new RAMFSInfo());
		}
	}

	public void unmountRamFolder(String path) throws IOException {
		if (_mounted.containsKey(path)) {
			RAMFS.deleteRAMFolder(path);
			_mounted.remove(path);
		}
	}

	public void shutdown() {
		for (Map.Entry<String, RAMFSInfo> mounted : _mounted.entrySet()) {
			try {
				unmountRamFolder(mounted.getKey());
			} catch (IOException e) {
				// print but keep unmounting
				e.printStackTrace();
			}
		}
	}

	@Override
	public long getSize(String folder) {
		// TODO on initialization this should just be computed by checking each
		// of the cluster nodes!
		return _config.getLong("ramdoop.ram.available", 1280000000);
	}
}
