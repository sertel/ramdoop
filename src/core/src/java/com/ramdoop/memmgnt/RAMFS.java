/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.memmgnt;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.util.Shell.ShellCommandExecutor;

/**
 * 
 * Create a RAMFS for the specified path.
 * 
 * @author sertel
 * 
 */
public final class RAMFS {

	private static final Map<String, Class<? extends IRAMFS>> OS_SUPPORT = new HashMap<String, Class<? extends IRAMFS>>();
	private static IRAMFS _ramfs = null;
	
	/*
	 * Reference: http://www.osgi.org/Specifications/Reference
	 */
	static {
		OS_SUPPORT.put("Linux", LinuxRAMFS.class);
		OS_SUPPORT.put("Mac OS", MacOSXRAMFS.class);
		OS_SUPPORT.put("Mac OS X", MacOSXRAMFS.class);
		initialize();
	}

	private interface IRAMFS {
		void createRAMFolder(String path) throws IOException;

		void deleteRAMFolder(String path) throws IOException;
	}

	/**
	 * For Linux there exist multiple ways to create a file system in RAM. The
	 * two most common options are ramfs and tmpfs. The difference is that tmpfs
	 * can be limited in size. When the size is exhausted then data is
	 * automatically swapped to disk. With ramfs the file system grows
	 * dynamically with the data written. This can exhaust the memory entirely
	 * and kill the system. A manager needs to written that makes sure that this
	 * will never happen.
	 * 
	 * @author sertel
	 * 
	 */
	public static class LinuxRAMFS implements IRAMFS {
		@Override
		public void createRAMFolder(String path) throws IOException {
			ShellCommandExecutor.execCommand(
					"./src/shell/linux_create_ram_fs.sh", path);
		}

		@Override
		public void deleteRAMFolder(String path) throws IOException {
			ShellCommandExecutor.execCommand(
					"./src/shell/linux_delete_ram_fs.sh", path);
		}
	}

	public static class MacOSXRAMFS implements IRAMFS {
		@Override
		public void createRAMFolder(String path) throws IOException {
			String output = ShellCommandExecutor.execCommand(
					"./src/shell/mac_os_x_create_ram_fs.sh", path);
			System.out.println(output);
		}

		@Override
		public void deleteRAMFolder(String path) throws IOException {
			String output = ShellCommandExecutor.execCommand(
					"./src/shell/mac_os_x_delete_ram_fs.sh", path);
			System.out.println(output);
		}
	}

	private RAMFS() {
		/* singleton */
	}

	private static void initialize() {
		String os = System.getProperty("os.name");
		os = os.trim();
		if (!OS_SUPPORT.containsKey(os)) {
			throw new RuntimeException("Unsupported OS detected! =>" + os);
		} else {
			try {
				_ramfs = OS_SUPPORT.get(os).getConstructor().newInstance();
			} catch (Exception e) {
				throw new RuntimeException("Impossible", e);
			}
		}
	}

	public static void createRAMFolder(String path) throws IOException {
		_ramfs.createRAMFolder(path);
	}

	public static void deleteRAMFolder(String path) throws IOException {
		_ramfs.deleteRAMFolder(path);
	}

}
