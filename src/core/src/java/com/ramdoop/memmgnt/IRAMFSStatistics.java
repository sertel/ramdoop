/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.memmgnt;

public interface IRAMFSStatistics {

	/**
	 * Returns the size of the ram fs folder.
	 * 
	 * @return
	 */
	public long getSize(String folder);

	// public long getAvailableMemory();
}
