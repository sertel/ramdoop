/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.memmgnt;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.util.Shell.ShellCommandExecutor;

/**
 * Started once in the hadoop cluster, this entity is responsible for mounting
 * and unmounting all the RAMFS on the cluster nodes. Additionally, it provides
 * a distributed service that allows to get cluster-wide RAMFS statistics. </p>
 * Input to this component is the slaves file of the hadoop cluster
 * configuration. </p> TODO When we select a ramfs instead of a tmpfs then this
 * component should also be responsible to take countermeasures whenever we have
 * a runnaway node that overloads the memory due to a bug in a mapper or
 * reducer.
 * 
 * @author sertel
 * 
 */
public class RAMFSClusterManager implements Runnable {

	private String _hadoopHome = null;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		RAMFSClusterManager manager = new RAMFSClusterManager();
		manager._hadoopHome = args[0];
		manager.run();
	}

	public void run() {
		registerShutdown();
		try {
			mountAllSlaves(findSlaves());
			while (true) {
				// TODO request handling for statistics goes here!
				Thread.sleep(10000);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void mountAllSlaves(List<String> slaves) throws IOException {
		String args = createArgsString(slaves);
		String output = ShellCommandExecutor.execCommand(_hadoopHome
				+ "/lib/ramdoop/cluster_mount_ram_fs.sh", args);
		System.out.println(output);
	}

	private void unmountAllSlaves(List<String> slaves) throws IOException {
		String args = createArgsString(slaves);
		String output = ShellCommandExecutor.execCommand(_hadoopHome
				+ "/lib/ramdoop/cluster_umount_ram_fs.sh", args);
		System.out.println(output);
	}

	private void registerShutdown() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				try {
					unmountAllSlaves(findSlaves());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}

	private String createArgsString(List<String> slaves) {
		StringBuffer args = new StringBuffer();
		for (String slave : slaves) {
			args.append(slave);
			args.append(" ");
		}
		return args.toString();
	}

	private List<String> findSlaves() throws IOException {
		return FileUtils.readLines(new File(_hadoopHome + "/conf/slaves"));
	}

}
