/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.JobAccess;

public class JobExecutor {

	public static String EXECUTION_STRATEGY = "ramdoop.tiled.exec.strategy";

	public enum ExecutionStrategy {
		/*
		 * Takes a single job and executes a map-combiner mini job and then a
		 * job with the reducer. Produces a final global result across the mini
		 * jobs.
		 */
		SINGLE,

		/*
		 * Takes multiple (currently 2) as input. Executes the first as a mini
		 * job and later uses the reducer of the follow-up job for the second
		 * job. This assumes that the mapper of the second job is just an
		 * Identity function.
		 */
		MULTI,

		/*
		 * Executes the given job in a pure mini job fashion. That is, no final
		 * job is executed. It assumes that the input is tiled in such a way
		 * that a final job is not required.
		 */
		PURE
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			JobExecutor executor = new JobExecutor();
			for (String jobConfig : args)
				executor.deserializeJobConfig(jobConfig);
			executor.compute();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	private JobRegistry _registry = new JobRegistry();

	private void deserializeJobConfig(String serializedJobConfig) throws IOException {
		System.out.println("Deserializing config: " + serializedJobConfig);

		File f = new File(serializedJobConfig);
		byte[] conf = FileUtils.readFileToByteArray(f);
		Job jConf = new Job();
		DataInputStream in = new DataInputStream(new ByteArrayInputStream(conf));
		jConf.getConfiguration().readFields(in);
		_registry.register(jConf);
	}

	private void compute() throws IOException, InterruptedException, ClassNotFoundException {
		assert _registry.getAllRegisteredJobs().size() > 0;
		Job conf = _registry.getAllRegisteredJobs().iterator().next();
		ExecutionStrategy strat = conf.getConfiguration().getEnum(EXECUTION_STRATEGY,
				ExecutionStrategy.SINGLE);
		switch (strat) {
			case MULTI:
				executeMultiJob();
				break;
			case SINGLE:
				executeSingleJob(conf);
				break;
			case PURE:
				executePureJob(conf);
				break;
		}
	}

	private void executePureJob(Job conf) throws IOException, InterruptedException {
		System.out.println("RAMdoop starts execution ...");
		TiledExecution execution = new TiledExecution(conf);
		execution.prepareExecution();
		execution.runTiledComputation();
		execution.finalizeJob();
	}

	private void executeSingleJob(Job conf) throws ClassNotFoundException, IOException,
			InterruptedException {
		Job job = prepareSingleJobConfig(conf);
		System.out.println("RAMdoop starts execution ...");
		TiledExecution execution = new TiledExecution(job);
		execution.runRAMdoopComputation();
		execution.finalizeJob();
	}

	private Job prepareSingleJobConfig(Job conf) throws ClassNotFoundException {
		String combinerClass = conf.getConfiguration().get(JobAccess.combineClass(), null);
		if (combinerClass != null)
			conf.getConfiguration().set(TiledExecution.FIRST_PHASE_REDUCER, combinerClass);
		return conf;
	}

	private void executeMultiJob() throws IOException, InterruptedException {
		assert _registry.getAllRegisteredJobs().size() == 2;
		Iterator<Job> it = _registry.getAllRegisteredJobs().iterator();
		Job firstJob = it.next();
		Job secondJob = it.next();

		firstJob = prepareFirstJob(firstJob);
		secondJob = prepareSecondJob(secondJob);

		System.out.println("RAMdoop starts execution ...");
		TiledExecution execution = new TiledExecution(firstJob);
		execution.prepareExecution();
		execution.runTiledComputation();
		execution.setRootJob(secondJob);
		execution.runFinalTiledReduceJobs();
		execution.finalizeJob();
	}

	// the below to functions might turn into an API later on

	private Job prepareFirstJob(Job firstJob) {
		// note: we can not rely on the fact that we have the job jar at this
		// point!
		assert firstJob.getConfiguration().get(JobAccess.combineClass(), null) == null;

		// assumes that the mapper of the second job is identity (or similar)
		Configuration config = firstJob.getConfiguration();
		config.set(TiledExecution.FIRST_PHASE_REDUCER, config.get(JobAccess.reduceClass()));
		return firstJob;
	}

	/**
	 * There is nothing to prepare here because the TiledExecution will just use
	 * the specified reducer, which is exactly what we want.
	 * 
	 * @param secondJob
	 * @return
	 */
	private Job prepareSecondJob(Job secondJob) {
		return secondJob;
	}
}
