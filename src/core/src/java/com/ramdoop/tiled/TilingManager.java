/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.util.ReflectionUtils;

import com.ramdoop.tiled.api.Tiler;
import com.ramdoop.tiled.io.TiledInputFormat;

public class TilingManager {

	public static String INITIAL_TILE_SIZE = "ramdoop.input.job.size";
	public static String TILER = "ramdoop.input.tiler";

	private long _tileSize = -1;
	private Tiler _tiler = null;

	public TilingManager(Configuration conf) {
		_tileSize = conf.getLong(TilingManager.INITIAL_TILE_SIZE, 2000000);
		_tiler = getTiler(conf);
	}

	private Tiler getTiler(Configuration conf) {
		try {
			return (Tiler) ReflectionUtils.newInstance(conf.getClass(TILER, DefaultTiler.class),
					conf);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Should never reach here.", e);
		}
	}

	public Map<String, List<String>> tile(Job job) throws IOException, InterruptedException {
		long start = System.currentTimeMillis();
		List<InputSplit> splits = retrieveInputSplits(job);
		Map<String, List<List<InputSplit>>> tiles = _tiler.tile(splits, _tileSize);
		Map<String, List<String>> serializedTiles = new HashMap<>();
		for (Map.Entry<String, List<List<InputSplit>>> entry : tiles.entrySet()) {
			List<String> pTiles = new ArrayList<>();
			for (List<InputSplit> tile : entry.getValue()) {
				pTiles.add(TiledInputFormat.serializeSplits(tile));
			}
			serializedTiles.put(entry.getKey(), pTiles);
		}
		System.out.println("Tiling took: " + (System.currentTimeMillis() - start) + " ms");
		return serializedTiles;
	}

	private List<InputSplit> retrieveInputSplits(Job job) {
		try {
			org.apache.hadoop.mapreduce.InputFormat<?, ?> inputFormat = (org.apache.hadoop.mapreduce.InputFormat<?, ?>) ReflectionUtils
					.newInstance(job.getInputFormatClass(), job.getConfiguration());
			// System.out.println("Input paths: "
			// + Arrays.deepToString(FileInputFormat.getInputPaths(job)));
			Path[] inputPaths = supportTiledInput(FileInputFormat.getInputPaths(job), job.getConfiguration());
			FileInputFormat.setInputPaths(job, inputPaths);
			return inputFormat.getSplits(job);
		} catch (IOException | ClassNotFoundException | InterruptedException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * This function assures that we support input that was produces by a
	 * previous tiling job. Therefore, we check if there exists a subfolder
	 * tile-0. If so then we return all subfolders as input folders.
	 * 
	 * @throws IOException
	 */
	private Path[] supportTiledInput(Path[] inputs, Configuration job) throws IOException {
		FileSystem fs = FileSystem.get(job);
		List<Path> paths = new ArrayList<>();
		for (Path input : inputs) {

			if (fs.isFile(input)) {
				paths.add(input);
			} else {
				if (fs.exists(new Path(input, "tile-0"))) {
					FileStatus[] status = fs.listStatus(input);
					for (FileStatus stat : status) {
						if (stat.isDir())
							paths.add(stat.getPath());
					}
				} else {
					paths.add(input);
				}
			}
		}
		return paths.toArray(new Path[paths.size()]);
	}
}
