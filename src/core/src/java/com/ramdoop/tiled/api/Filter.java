/*
 * Copyright (c) Sebastian Ertel 2013-2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled.api;

/**
 * This filter is meant to be used when initial tiling does not work because the
 * input data is too small.
 * 
 * @author sertel
 * 
 */
public interface Filter<K, V> {

	/**
	 * Check if this input should be filtered. Only if this function evaluates
	 * to true key-value pairs are dispatched to this filter.
	 * 
	 * @param input
	 * @return
	 */
	public boolean isAssociateInput(String input);

	/**
	 * Check whether this key-value pair should be filtered.
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public boolean filter(K key, V value);
}
