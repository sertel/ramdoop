/*
 * Copyright (c) Sebastian Ertel 2013-2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.mapreduce.InputSplit;

/**
 * This interface is meant to be used whenever a specific tiling strategy is
 * required on the input data.
 * 
 * @author sertel
 * 
 */
public abstract class Tiler {
	
	/**
	 * A function which assigns each split to a tile.
	 * 
	 * @param splits
	 * @return a map of tile primary and the according tiled job(s).
	 * @throws InterruptedException
	 * @throws IOException
	 */
	abstract public Map<String, List<List<InputSplit>>> tile(List<InputSplit> splits, long tileSize) throws IOException, InterruptedException;
	
	/**
	 * Get all locations (nodes) that store this split.
	 * @param split
	 * @return
	 */
	protected final String[] getLocations(InputSplit split) {
		try {
			return split.getLocations();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Group splits according to their location (node).
	 * 
	 * @param splits
	 * @return
	 */
	protected final Map<String, List<InputSplit>> grpByLocation(List<InputSplit> splits) {

		// try to balance the mini jobs
		Map<String, List<InputSplit>> localizedSplits = new HashMap<String, List<InputSplit>>();
		for (InputSplit split : splits) {
			String[] locations = getLocations(split);
			String minLoaded = null;
			int minLoad = Integer.MAX_VALUE;
			for (String location : locations) {
				if (!localizedSplits.containsKey(location)) {
					localizedSplits.put(location, new ArrayList<InputSplit>());
				}

				if (minLoad >= localizedSplits.get(location).size()) {
					minLoaded = location;
					minLoad = localizedSplits.get(location).size();
				}
			}
			localizedSplits.get(minLoaded).add(split);

			// this reference consists of the file path, the start and the end
			// System.out.println("Locations: " + Arrays.toString(locations));
			// System.out.println("Split reference: " + split.toString());
		}
//		 System.out.println("Locations and load:");
//		 for(Map.Entry<String, List<InputSplit>> entry :
//		 localizedSplits.entrySet()){
//		 System.out.println("Location: " + entry.getKey() + " Load: " +
//		 entry.getValue().size());
//		 }
		verify(splits.size(), getNumSplits(Collections.singletonList(localizedSplits.values())));
		return localizedSplits;
	}

	protected final void verify(int expected, int actual) {
		if (expected != actual) {
			throw new RuntimeException("Unusual amount of splits created! " + expected + " vs. "
					+ actual);
		}
	}

	protected final int getNumSplits(Collection<? extends Collection<List<InputSplit>>> results) {
		int size = 0;
		for (Collection<List<InputSplit>> tile : results) {
			for (List<InputSplit> t : tile) {
				size += t.size();
			}
		}
		return size;
	}
	
	protected final int getNumTiles(Map<String, List<List<InputSplit>>> results){
		int size = 0;
		for(List<List<InputSplit>> tiles : results.values())
			size += tiles.size();
		return size;
	}

}
