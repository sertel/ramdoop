package com.ramdoop.tiled.api;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.SequenceFile.CompressionType;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.DefaultCodec;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.output.MapFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.ReflectionUtils;

public class DynamicMapFileOutputFormat extends MapFileOutputFormat {
		
	@SuppressWarnings("rawtypes")
	private static class WrongWritableComparable implements WritableComparable{

		@Override
		public void readFields(DataInput arg0) throws IOException {
			throw new UnsupportedOperationException();
		}

		@Override
		public void write(DataOutput arg0) throws IOException {
			throw new UnsupportedOperationException();
		}

		@Override
		public int compareTo(Object o) {
			return -1;
		}
		
	}

	/**
	 * Code copied straight from super class as the clojure restricts access to
	 * the MapFile.Writer
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public RecordWriter<WritableComparable<?>, Writable> getRecordWriter(TaskAttemptContext context)
			throws IOException {
		Configuration conf = context.getConfiguration();
		CompressionCodec codec = null;
		CompressionType compressionType = CompressionType.NONE;
		if (getCompressOutput(context)) {
			// find the kind of compression to do
			compressionType = SequenceFileOutputFormat.getOutputCompressionType(context);

			// find the right codec
			Class<?> codecClass = getOutputCompressorClass(context, DefaultCodec.class);
			codec = (CompressionCodec) ReflectionUtils.newInstance(codecClass, conf);
		}

		Path file = getDefaultWorkFile(context, "");
		FileSystem fs = file.getFileSystem(conf);
		// ignore the progress parameter, since MapFile is local
		final MapFile.Writer out = new MapFile.Writer(conf, fs, file.toString(), context
				.getOutputKeyClass().asSubclass(WritableComparable.class), context
				.getOutputValueClass().asSubclass(Writable.class), compressionType, codec, context);

		return new RecordWriter<WritableComparable<?>, Writable>() {
			private WritableComparable _prev = new WrongWritableComparable();
			private int _size = 1;

			/**
			 * Enforce an index entry whenever the key changes.
			 */
			public void write(WritableComparable key, Writable value) throws IOException {
				if (_prev.compareTo(key) != 0){
					System.out.println("Requesting index entry");
					// need to special case the first entry.
					if(_size == 1)	out.setIndexInterval(_size);
					else out.setIndexInterval(_size-1);
				}else{
					System.out.println("Skipping index entry");
					out.setIndexInterval(_size);
				}
				out.append(key, value);
				_prev = key;
				_size++;
			}

			public void close(TaskAttemptContext context) throws IOException {
				out.close();
			}
		};
	}
}
