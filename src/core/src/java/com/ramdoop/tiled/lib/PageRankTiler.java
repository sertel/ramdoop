/*
 * Copyright (c) Sebastian Ertel 2013-2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled.lib;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.mapreduce.InputSplit;

/**
 * A tiler specifically for the (naive) page rank algorithm.
 * 
 * @author sertel
 * 
 */
public class PageRankTiler extends FileIDTiler {

	private List<InputSplit> _prVectorSplits = null;

	@Override
	public Map<String, List<List<InputSplit>>> tile(List<InputSplit> splits, long tileSize)
			throws IOException, InterruptedException {
		Map<String, List<List<InputSplit>>> results = super.tile(splits, tileSize);
		System.out.println("Calculated tile count: " + getNumTiles(results));
		verify(splits.size() - _prVectorSplits.size(), getNumSplits(results.values()));

		// the page rank vector needs to be included in all tiles!
		results = populatePageRankVector(results);

		System.out.println("Calculated tile count: " + getNumTiles(results));
		System.out.println("Found PR vector splits: " + _prVectorSplits.size());
		System.out.println("Keys: " + Arrays.deepToString(results.keySet().toArray()));
		verify((splits.size() - _prVectorSplits.size()) + (getNumTiles(results) * _prVectorSplits.size()),
				getNumSplits(results.values()));
		return results;
	}

	protected Map<String, List<InputSplit>> filter(Map<String, List<InputSplit>> grped) {
		System.out.println("Grouped: " + grped.size());
		Map<String, List<InputSplit>> filtered = new HashMap<>();
		_prVectorSplits = new ArrayList<>();
		for (Map.Entry<String, List<InputSplit>> entry : grped.entrySet()) {
			if (entry.getKey().contains("pr_vector"))
				_prVectorSplits.addAll(entry.getValue());
			else
				filtered.put(entry.getKey(), entry.getValue());
		}

		if (_prVectorSplits.isEmpty()) {
			System.out.println(Arrays.deepToString(grped.keySet().toArray()));
			throw new RuntimeException("Page rank vector splits not found!");
		}
		return filtered;
	}

	private Map<String, List<List<InputSplit>>> populatePageRankVector(
			Map<String, List<List<InputSplit>>> results) {

		for (Map.Entry<String, List<List<InputSplit>>> entry : results.entrySet()) {
			for (List<InputSplit> tile : entry.getValue()) {
				tile.addAll(_prVectorSplits);
			}
		}
		return results;
	}
}
