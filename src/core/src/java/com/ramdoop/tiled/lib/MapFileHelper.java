/*
 * Copyright (c) Sebastian Ertel 2013-2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled.lib;

import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

@SuppressWarnings("rawtypes")
public class MapFileHelper {
	private static final Log LOG = LogFactory.getLog(MapFile.class);

	/**
	 * This code was taken straight from MapFile.Reader because it did not allow
	 * enough control for our purposes.
	 * 
	 * @author sertel
	 * 
	 */
	public static class Reader implements java.io.Closeable {

		/**
		 * Number of index entries to skip between each entry. Zero by default.
		 * Setting this to values larger than zero can facilitate opening large
		 * map files using less memory.
		 */
		private int INDEX_SKIP = 0;

		private WritableComparator comparator;

		// the data, on disk
		private SequenceFile.Reader data;
		private SequenceFile.Reader index;

		// whether the index Reader was closed
		private boolean indexClosed = false;

		// the index, in memory
		private int count = -1;
		private WritableComparable[] keys;
		private long[] positions;
		private long dataFileLength = -1;

		/** Returns the class of keys in this file. */
		public Class<?> getKeyClass() {
			return data.getKeyClass();
		}

		/** Returns the class of values in this file. */
		public Class<?> getValueClass() {
			return data.getValueClass();
		}

		/** Construct a map reader for the named map. */
		public Reader(FileSystem fs, String dirName, Configuration conf) throws IOException {
			this(fs, dirName, null, conf);
			INDEX_SKIP = conf.getInt("io.map.index.skip", 0);
		}

		/** Construct a map reader for the named map using the named comparator. */
		public Reader(FileSystem fs, String dirName, WritableComparator comparator,
				Configuration conf) throws IOException {
			this(fs, dirName, comparator, conf, true);
		}

		/**
		 * Hook to allow subclasses to defer opening streams until further
		 * initialization is complete.
		 * 
		 * @see #createDataFileReader(FileSystem, Path, Configuration)
		 */
		protected Reader(FileSystem fs, String dirName, WritableComparator comparator,
				Configuration conf, boolean open) throws IOException {

			if (open) {
				open(fs, dirName, comparator, conf);
			}
		}

		protected synchronized void open(FileSystem fs, String dirName,
				WritableComparator comparator, Configuration conf) throws IOException {
			Path dir = new Path(dirName);
			Path dataFile = new Path(dir, MapFile.DATA_FILE_NAME);
			Path indexFile = new Path(dir, MapFile.INDEX_FILE_NAME);

			// open the data
			this.data = createDataFileReader(fs, dataFile, conf);

			if (comparator == null)
				this.comparator = WritableComparator.get(data.getKeyClass().asSubclass(
						WritableComparable.class));
			else
				this.comparator = comparator;

			// open the index
			this.index = new SequenceFile.Reader(fs, indexFile, conf);
		}

		/**
		 * Override this method to specialize the type of
		 * {@link SequenceFile.Reader} returned.
		 */
		protected SequenceFile.Reader createDataFileReader(FileSystem fs, Path dataFile,
				Configuration conf) throws IOException {
			dataFileLength = fs.getFileStatus(dataFile).getLen();
			return new SequenceFile.Reader(fs, dataFile, conf);
		}
		
		private void readIndex() throws IOException {
			// read the index entirely into memory
			if (this.keys != null)
				return;
			this.count = 0;
			this.keys = new WritableComparable[1024];
			this.positions = new long[1024];
			try {
				int skip = INDEX_SKIP;
				LongWritable position = new LongWritable();
				WritableComparable lastKey = null;
				while (true) {
					WritableComparable k = comparator.newKey();
					
					if (!index.next(k, position)){
						break;
					}

					// check order to make sure comparator is compatible
					if (lastKey != null && comparator.compare(lastKey, k) > 0)
						throw new IOException("key out of order: " + k + " after " + lastKey);
					lastKey = k;

					if (skip > 0) {
						skip--;
						continue; // skip this entry
					} else {
						skip = INDEX_SKIP; // reset skip
					}

					if (count == keys.length) { // time to grow arrays
						int newLength = (keys.length * 3) / 2;
						WritableComparable[] newKeys = new WritableComparable[newLength];
						long[] newPositions = new long[newLength];
						System.arraycopy(keys, 0, newKeys, 0, count);
						System.arraycopy(positions, 0, newPositions, 0, count);
						keys = newKeys;
						positions = newPositions;
					}
					
					keys[count] = k;
					positions[count] = position.get();
					count++;
				}
			} catch (EOFException e) {
				LOG.warn("Unexpected EOF reading " + index + " at entry #" + count + ".  Ignoring.");
			} finally {
				indexClosed = true;
				index.close();
			}
		}

		@Override
		public void close() throws IOException {
			if (!indexClosed) {
				index.close();
			}
			data.close();
		}
	}
	
	protected static int KEYS = 0;
	protected static int POSITIONS = 1;
	protected static int TOTAL_LENGTH = 2;
	protected static int COUNT = 3;
	
	protected static Object[] loadIndex(Configuration conf, Path path) {
		try {
			Reader reader = new Reader(path.getFileSystem(conf), path.toUri().getPath(), conf);
			reader.readIndex();
			reader.close();
			Object[] reduced = reduce(reader.keys, reader.positions, reader.count);
			return new Object[] {reduced[0], reduced[1], reader.dataFileLength, reader.count};
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
	}
	
	protected static long getLength(Object[] index, int item){
		if(item == (int) index[COUNT]-1)
			return ((Long)index[TOTAL_LENGTH]) - ((Long[])index[POSITIONS])[item];
		else
			return ((Long[])index[POSITIONS])[item+1] - ((Long[])index[POSITIONS])[item];
	}
	
	private static Object[] reduce(WritableComparable[] keys, long[] positions, int count){
		Object[][] result = new Object[2][];
		List<Object> ks = new ArrayList<Object>();
		List<Long> pos = new ArrayList<Long>();
		WritableComparable prev = keys[0];
		ks.add(keys[0]);
		pos.add(positions[0]);
		for(int i=1; i<count;i++){
			if(prev.compareTo(keys[i]) != 0){
				ks.add(keys[i]);
				pos.add(positions[i]);
			}
			prev = keys[i];
		}
		result[0] = ks.toArray(new WritableComparable[ks.size()]);
		result[1] = pos.toArray(new Long[pos.size()]);
		return result;
	}
	
	protected static void print(Object[] idx){
		System.out.println("Found keys: " + Arrays.deepToString((Object[]) idx[KEYS]));
		System.out.println("Positions: " + Arrays.toString((long[]) idx[POSITIONS]));
		System.out.println("File length: " + idx[TOTAL_LENGTH]);
		System.out.println("Count: " + idx[COUNT]);
	}
}
