/*
 * Copyright (c) Sebastian Ertel 2013-2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled.lib;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.hadoop.conf.Configurable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.BlockLocation;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

/**
 * A tiler for jobs where all input comes straight from map files.
 * <p>
 * Note: A file split is nothing but an offset and a length into a file. Even
 * for a sequence file the meta data is loaded first from its header and then
 * the pointer is set to the offset.
 * 
 * @author sertel
 * 
 */
public class MapFileTiler extends FileIDTiler implements Configurable {

	private Configuration _config = null;

	@Override
	public void setConf(Configuration conf) {
		_config = conf;
	}

	@Override
	public Configuration getConf() {
		return _config;
	}

	@Override
	public Map<String, List<List<InputSplit>>> tile(List<InputSplit> splits, long tileSize)
			throws IOException, InterruptedException {
		Map<String, List<InputSplit>> grped = super.grpByFile(splits);

		// for each file we would like to understand what keys are included
		Map<String, List<List<InputSplit>>> grpedSplits = grpByKey(grped);

		return adjustTileSize(grpedSplits, tileSize);
	}

	/**
	 * Finally create the tiles according to the predefined size.
	 * 
	 * @param grpedSplits
	 * @return
	 */
	private Map<String, List<List<InputSplit>>> adjustTileSize(
			Map<String, List<List<InputSplit>>> grpedSplits, long tileSize) {
		for (Map.Entry<String, List<List<InputSplit>>> entry : grpedSplits.entrySet()) {
			List<List<InputSplit>> keyTiles = entry.getValue();
			// perform a reduce()
			for (int i = 0; i < keyTiles.size() - 1; i++) {
				while (getLength(keyTiles.get(i)) < tileSize)
					keyTiles.get(i).addAll(keyTiles.remove(i + 1));
			}
		}
		return grpedSplits;
	}

	// TODO this might be totally non-performing!-> crazy thought: How about
	// turning this into an MR job :)
	// FIXME in fact this really is heavy duty.
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Map<String, List<List<InputSplit>>> grpByKey(Map<String, List<InputSplit>> grpedByFile)
			throws IOException {
		System.out.println("Entries: " + grpedByFile.size());
		// get the keys for each file
		Map<String, Object[]> fileToKeys = new HashMap<>();
		for (Map.Entry<String, List<InputSplit>> entry : grpedByFile.entrySet()) {
			// the path in the key is the raw path to the file but we need the
			// full path including the namespace of the filesystem.
			assert !entry.getValue().isEmpty();
			Path dataFilePath = ((FileSplit) entry.getValue().get(0)).getPath();
			// this path points to the data file but we need the directory (to
			// load the index)!
			Object[] keys = MapFileHelper.loadIndex(_config, dataFilePath.getParent());
			System.out.println("File: " + entry.getKey());
			System.out.println("Keys: " + Arrays.deepToString((Object[])keys[MapFileHelper.KEYS]));
			fileToKeys.put(entry.getKey(), keys);
		}

		// grp by key -> get the splits for each key
		Map<WritableComparable, Object[]> keyToFile = new HashMap<>();
		for (Map.Entry<String, Object[]> entry : fileToKeys.entrySet()) {
			WritableComparable[] keys = (WritableComparable[]) entry.getValue()[MapFileHelper.KEYS];
			for (int i = 0; i < (int) entry.getValue()[MapFileHelper.COUNT]; i++) {
				WritableComparable key = keys[i];
				if (!keyToFile.containsKey(key))
					keyToFile.put(key, new Object[] { new ArrayList<Integer>(),
							new ArrayList<String>() });
				((List<Integer>) keyToFile.get(key)[0]).add(i);
				((List<String>) keyToFile.get(key)[1]).add(entry.getKey());
			}
		}

		Map<String, List<List<InputSplit>>> grpKeyTiles = new HashMap<String, List<List<InputSplit>>>();
		while (!keyToFile.isEmpty()) {
			WritableComparable key = keyToFile.keySet().iterator().next();
			Object[] entry = keyToFile.get(key);
			List<Integer> idxes = (List<Integer>) entry[0];
			List<String> files = (List<String>) entry[1];
			System.out.println("Indexes: " + Arrays.deepToString(idxes.toArray()));
			System.out.println("Files: " + Arrays.deepToString(files.toArray()));

			// determine the first splits
			List<InputSplit> splits = new ArrayList<>();
			for (int i = 0; i < idxes.size(); i++) {
				String file = files.get(i);
				int idx = idxes.get(i);
				System.out.println(file);
				long pos = ((Long[]) fileToKeys.get(file)[MapFileHelper.POSITIONS])[idx];
				long length = MapFileHelper.getLength(fileToKeys.get(file), idx);
				if (pos == 0) {
					MapFileHelper.print(fileToKeys.get(file));
					throw new RuntimeException();
				}
				if (length < 0) {
					System.out.println("Requested index: " + idx + " Count: "
							+ fileToKeys.get(file)[MapFileHelper.COUNT]);
					System.out.println("Position[idx]: " + pos + " Position[idx+1]: "
							+ ((Long[]) fileToKeys.get(file)[MapFileHelper.POSITIONS])[idx + 1]);
					throw new RuntimeException();
				}
				// at this point we would like to create key based tiles. that
				// is, we want to first create a group which has all the ranges
				// for a given key. then we would like to see what size it has
				// and how much we miss until we have a size of 64MB again. that
				// means we expand the splits which requires more keys to be
				// added to that tile.

				// TODO figure out if it can happen that an entry inside a map
				// file (a key range) spans across multiple blocks
				// -> actually, that does not matter. the locations here are
				// just hints for the scheduler nothing else. the record reader
				// will read through the HDFS API which means that if the block
				// is located somewhere else then it will automatically retrieve
				// it from there!
				Path p = new Path(file);
				System.out.println("Retrieving block locations: offset=" + pos + " length="
						+ length);
				BlockLocation[] locs = getBlockLocations(p, pos, length);
				Set<String> hosts = new HashSet<>();
				for (BlockLocation loc : locs)
					hosts.addAll(Arrays.asList(loc.getHosts()));
				splits.add(new FileSplit(p, pos, length, hosts.toArray(new String[hosts.size()])));
			}

			// TODO extend the splits to be at the size of the normal splits
			// (64MB)
			keyToFile.remove(key);

			String tilePrimary = super.identifyPrimary(splits);
			if (!grpKeyTiles.containsKey(tilePrimary))
				grpKeyTiles.put(tilePrimary, new ArrayList<List<InputSplit>>());

			grpKeyTiles.get(tilePrimary).add(splits);
		}

		return grpKeyTiles;
	}

	private BlockLocation[] getBlockLocations(Path path, long start, long length)
			throws IOException {
		FileSystem fs = path.getFileSystem(_config);
		FileStatus file = fs.getFileStatus(path);
		return fs.getFileBlockLocations(file, start, length);
	}
}
