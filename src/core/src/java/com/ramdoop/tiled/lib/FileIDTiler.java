/*
 * Copyright (c) Sebastian Ertel 2013-2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled.lib;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import com.ramdoop.tiled.api.Tiler;

/**
 * This tiler will not group primarily on split location but will map input
 * splits from a single input file to the same tile.
 * 
 * @author sertel
 * 
 */
public class FileIDTiler extends Tiler {

	@Override
	public Map<String, List<List<InputSplit>>> tile(List<InputSplit> splits, long tileSize)
			throws IOException, InterruptedException {
		Map<String, List<InputSplit>> grped = grpByFile(splits);
		verify(splits.size(), getNumSplits(Collections.singletonList(grped.values())));
		grped = filter(grped);
		return tile(grped, tileSize);
	}

	protected Map<String, List<InputSplit>> filter(Map<String, List<InputSplit>> grped) {
		return grped;
	}

	private Map<String, List<List<InputSplit>>> tile(Map<String, List<InputSplit>> grped,
			long tileSize) {
		Map<String, List<List<InputSplit>>> tiles = new HashMap<>();
		Map<String, Long> tileLength = new HashMap<>();
		for (Map.Entry<String, List<InputSplit>> entry : grped.entrySet()) {
			String primary = identifyPrimary(entry.getValue());
			long length = getLength(entry.getValue());
			if (!tiles.containsKey(primary)) {
				List<List<InputSplit>> t = new ArrayList<List<InputSplit>>();
				t.add(new ArrayList<InputSplit>());
				tiles.put(primary, t);
				tileLength.put(primary, 0L);
			}
			tiles.get(primary).get(0).addAll(entry.getValue());
			length = tileLength.get(primary) + length;
			if (length > tileSize) {
				length = 0;
				tiles.get(primary).add(0, new ArrayList<InputSplit>());
			}
			tileLength.put(primary, length);
		}
		return tiles;
	}

	protected final long getLength(List<InputSplit> splits) {
		long length = 0;
		for (InputSplit split : splits)
			try {
				length += split.getLength();
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		return length;
	}

	protected final String identifyPrimary(List<? extends InputSplit> splits) {
		Map<String, Integer> locations = new HashMap<>();
		for (InputSplit split : splits) {
			for (String location : super.getLocations(split)) {
				if (!locations.containsKey(location))
					locations.put(location, 0);
				locations.put(location, locations.get(location) + 1);
			}
		}
		String primary = null;
		int max = 0;
		for (Map.Entry<String, Integer> entry : locations.entrySet()) {
			if (max < entry.getValue()) {
				max = entry.getValue();
				primary = entry.getKey();
			}
		}
		return primary;
	}

	protected Map<String, List<InputSplit>> grpByFile(List<InputSplit> splits) {
		Map<String, List<InputSplit>> result = new HashMap<>();
		for (InputSplit split : splits) {
			String fileName = ((FileSplit) split).getPath().toUri().getPath();
			if (!result.containsKey(fileName))
				result.put(fileName, new ArrayList<InputSplit>());
			result.get(fileName).add(split);
		}
		return result;
	}

}
