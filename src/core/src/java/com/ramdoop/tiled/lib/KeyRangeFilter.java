/*
 * Copyright (c) Sebastian Ertel 2013-2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled.lib;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.JobID;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFilter.FilterBase;

import com.ramdoop.tiled.io.TiledInputFormat;

/**
 * A filter that is based on the tile. It works based on the assumption that the
 * other files are MapFiles that can be queried for their keys. The filter then
 * will only allow keys that are present in the other files.
 * 
 * @author sertel
 * 
 */
public class KeyRangeFilter extends FilterBase {

	private Set<Object> _allowedKeys = new HashSet<Object>();

	/**
	 * Fetch the keys from the other files that participate in the computation
	 * of this tile.
	 */
	@Override
	public void setConf(Configuration conf) {
		Path[] paths = FileInputFormat.getInputPaths(new JobContext(conf, new JobID()));
		String currentSplit = conf.get(TiledInputFormat.CURRENT_SPLIT);
		for (Path path : paths) {
			if (!path.getName().equals(currentSplit)) {
				loadKeys(conf, path);
			}
		}
		System.out.println("Allowed keys: " + Arrays.deepToString(_allowedKeys.toArray()));
	}

	@SuppressWarnings("rawtypes")
	private class FakeWritableComparator extends WritableComparator {

		protected FakeWritableComparator(Class<? extends WritableComparable> keyClass) {
			super(keyClass);
		}

		public int compare(WritableComparable one, WritableComparable two) {
			// it never matches
			_allowedKeys.add(one);
			_allowedKeys.add(two);
			return -1;
		}
	}

	/**
	 * @param conf
	 * @param path
	 * @throws IOException
	 */
	private void loadKeys(Configuration conf, Path path) {
		try {
			// The MapFile.Reader API is once more very conservative and won't
			// allow us access to the keys. Not even by subclassing. So we use
			// this trick here: We use a comparator to get access to the keys
			// loaded from the index file. A dummy seek will help us to touch
			// all keys.
			MapFile.Reader reader = new MapFile.Reader(path.getFileSystem(conf),  path.toUri().getPath(),
					new FakeWritableComparator(LongWritable.class), conf);
			reader.seek(new LongWritable(0));
			reader.close();
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
	}

	@Override
	public boolean accept(Object key) {
		return _allowedKeys.contains(key);
	}
}
