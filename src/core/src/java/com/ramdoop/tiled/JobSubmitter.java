/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.JobID;

/**
 * This class is responsible for submitting new jobs to Hadoop.
 * 
 * @author sertel
 * 
 */
public class JobSubmitter {

	public interface IJobDoneListener {
		public void done(Job job, Exception error);
	}

	private class JobHandler implements Runnable {

		private Job _job = null;
		private Exception _error = null;

		private JobHandler(Job job) {
			_job = job;
		}

		@Override
		public void run() {
			submit(_job);
			try {
				if (_job.getConfiguration().getBoolean("ramdoop.monitor.job",
						false))
					monitorProgress();
				else
					_job.waitForCompletion(true);
			} catch (Exception e) {
				_error = e;
			}
			_runningJobs.remove(_job.getJobID());
			int runningJobs = _runningJobsCount.decrementAndGet();

			notifyListeners(_job, _error);

			if (runningJobs == 0) {
				_lock.lock();
				try {
					_doneCondition.signalAll();
				} finally {
					_lock.unlock();
				}
			}
		}

		private void monitorProgress() {
			String dir = _job.getConfiguration().get("ramdoop.monitor.dir");
			Writer writer = null;

			try {
				writer = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(dir + "/" + _job.getJobID()
								+ ".log"), "utf-8"));
				writer.write("[");
				String currentReport = null;
				String lastReport = null;
				while (!_job.isComplete()) {
					if (currentReport != null
							&& !currentReport.equals(lastReport)) {
						writer.write(currentReport);
						lastReport = currentReport;
					}

					Thread.sleep(1000);

					currentReport = ("{ \"time\" : "
							+ System.currentTimeMillis() + ", \"map\" : "
							+ _job.mapProgress() + ", \"reduce\" : "
							+ _job.reduceProgress() + "},");
				}

				if (currentReport != null) {
					writer.write(currentReport.substring(0,
							currentReport.length() - 1));
				}

				writer.write("]");
			} catch (IOException ex) {
				// report
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					writer.close();
				} catch (Exception ex) {
				}
			}

		}
	}

	private ReentrantLock _lock = new ReentrantLock();
	private Condition _doneCondition = _lock.newCondition();
	private AtomicInteger _runningJobsCount = new AtomicInteger(0);

//	private JobRegistry _registry = null;
	private Map<String, List<JobHandler>> _submittedJobs = new HashMap<String, List<JobHandler>>();

	private Map<JobID, Job> _runningJobs = new HashMap<JobID, Job>();

	private List<IJobDoneListener> _listeners = new ArrayList<IJobDoneListener>();

//	private long _tileCounter = 0;

//	public JobSubmitter(JobRegistry registry) {
//		_registry = registry;
//	}

	public void addListener(IJobDoneListener listener) {
		_listeners.add(listener);
	}

	private void notifyListeners(Job job, Exception error) {
		for (IJobDoneListener listener : _listeners) {
			listener.done(job, error);
		}
	}

	public void notifyReadyJob(String continuousJobID, Path[] inputData,
			Path outputDir) throws IOException {
//		Job job = _registry.getNewJobCopy(continuousJobID);
//		FileInputFormat.setInputPaths(job, inputData);
//		FileOutputFormat.setOutputPath(job, outputDir);
//		job.getConfiguration().setLong(OutputFormatProxyNewAPI.TILE_ID_PARAM,
//				_tileCounter++);
//		executeJob(continuousJobID, job);
	}

	public void executeJob(String sourceID, Job job) {
		final Job readyJob = job;
		JobHandler finishHandler = new JobHandler(readyJob);

		if (!_submittedJobs.containsKey(sourceID)) {
			_submittedJobs.put(sourceID, new ArrayList<JobHandler>());
		}
		_submittedJobs.get(sourceID).add(finishHandler);
		// FIXME might override things if jobs have no id (=null)!
		_runningJobs.put(readyJob.getJobID(), readyJob);
		_runningJobsCount.incrementAndGet();
		new Thread(finishHandler, "job-" + job.getJobName() + "-finish-handler")
				.start();
	}

	/**
	 * Every time that a new job is submitted to the Job Tracker, it gets
	 * assigned a job ID. Intermediate results are stored by default into the
	 * following directory:<br>
	 * ${mapred.local.dir}/taskTracker/$user/jobcache/$jobid/$taskid/output<br>
	 * Hence, what we need is the jobID and the taskID. These are quite hard to
	 * come by. A way to get the jobID is to submit another task and make it
	 * depending on this one. As soon as this has been accomplished, the first
	 * task is responsible for populating the according ramFS dir and only
	 * afterwards the job is allowed to execute.
	 * <p>
	 * I skip this part for now because I'm just not sure if it makes sense to
	 * keep all the other parts on disk.
	 * 
	 * @param job
	 */
	private void submit(Job job) {
		try {
			job.submit();
			// TODO use this ID to construct the according ram FS dir to be
			// used. Problem: we receive the job ID only after we have already
			// submitted the job.
			// JobID id = job.getJobID();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		System.out.println("Job submitted: " + job.getJobID());
	}

	/**
	 * This method blocks until all jobs are done.
	 */
	public void awaitAllJobsDone() {
		_lock.lock();
		try {
			while (_runningJobsCount.get() > 0) {
				_doneCondition.awaitUninterruptibly();
			}
		} finally {
			_lock.unlock();
		}
	}

	public Map<String, List<Exception>> getErrors() {
		Map<String, List<Exception>> errors = new HashMap<String, List<Exception>>();
		for (Map.Entry<String, List<JobHandler>> source : _submittedJobs
				.entrySet()) {
			List<Exception> sourceErrors = new ArrayList<Exception>();
			for (JobHandler handler : source.getValue()) {
				if (handler._error != null)
					sourceErrors.add(handler._error);
			}
			if (!sourceErrors.isEmpty())
				errors.put(source.getKey(), sourceErrors);
		}
		return errors;
	}

	public Set<Job> getSubmittedJobs() {
		Set<Job> jobs = new HashSet<Job>();
		for (List<JobHandler> handlers : _submittedJobs.values()) {
			for (JobHandler handler : handlers) {
				jobs.add(handler._job);
			}
		}
		return jobs;
	}
}
