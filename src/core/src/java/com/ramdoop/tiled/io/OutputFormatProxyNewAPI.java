/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled.io;

import java.io.IOException;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Progressable;
import org.apache.hadoop.util.ReflectionUtils;

/**
 * New API.
 * 
 * @author sertel
 * 
 * @param <K>
 * @param <V>
 */
public class OutputFormatProxyNewAPI<K, V> extends FileOutputFormat<K, V> {

	@SuppressWarnings("hiding")
	class RecordWriterProxy<K, V> extends RecordWriter<K, V> {
		RecordWriter<K, V> _writer = null;
		Partitioner<K, V> _partitioner = null;
		int _numReduceTasks = -1;
		FileSystem _ignored = null;
		String _name = null;
		Progressable _progress = null;

		RecordWriterProxy(Partitioner<K, V> partitioner) {
			_partitioner = partitioner;
		}

		// This assumes that all keys that hit this writer are actually
		// belonging to one single partition. This is true because there is a
		// writer spawned for every partition in the reduce task.
		@SuppressWarnings("unchecked")
		@Override
		public void write(K key, V value) throws IOException,
				InterruptedException {
			System.out.println("First key:" + key);
			System.out.println("Found partition:"
					+ _partitioner.getPartition(key, value, _numReduceTasks));

			if (_writer == null) {
				System.out.println("First key:" + key);
				_partition = _partitioner.getPartition(key, value,
						_numReduceTasks);
				System.out.println("Found partition:" + _partition);
				// register the partition at the job's configuration so it can
				// be used when the spills are moved from the temporary
				// directory to the final output dir.
				String folder = "partition-" + _partition;
				Path p = new Path(FileOutputFormat.getOutputPath(_context),
						folder);
				System.out.println("New output dir:" + p.toString());
				System.out.println("Original output format: "
						+ FileOutputFormat.getOutputPath(_context));
				setOutputName(_context, new Path(folder,
						getOutputName(_context)).toString() + "-t" + _tileID);
				_writer = (RecordWriter<K, V>) _outputFormat
						.getRecordWriter(_context);
			}
			_writer.write(key, value);
		}

		@Override
		public void close(TaskAttemptContext context) throws IOException,
				InterruptedException {
			if (_writer != null) {
				_writer.close(context);
			} else {
				System.out.println("Reducer received no keys!");
			}
		}
	}

	private int _partition = -1;
	private long _tileID = -1;
	private TaskAttemptContext _context = null;
	private FileOutputFormat<K, V> _outputFormat = null;

	public static String TILE_ID_PARAM = "ramdoop.tile.id";

	@SuppressWarnings("unchecked")
	@Override
	public RecordWriter<K, V> getRecordWriter(TaskAttemptContext job)
			throws IOException, InterruptedException {
		_context = job;
		_tileID = job.getConfiguration().getLong(TILE_ID_PARAM, -1);
		if (_tileID < 0) {
			throw new RuntimeException("Missing tile ID!");
		}
		System.out.println("Num reducers: " + _context.getNumReduceTasks());
		createOutputFormat();
		try {
			return new RecordWriterProxy<K, V>(
					(Partitioner<K, V>) ReflectionUtils.newInstance(
							_context.getPartitionerClass(),
							_context.getConfiguration()));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	private void createOutputFormat() {
		Class<?> cls = _context.getConfiguration().getClass(
				"ramdoop.reduce.outputformat", null);
		_outputFormat = (FileOutputFormat<K, V>) ReflectionUtils.newInstance(
				cls, _context.getConfiguration());
	}

}
