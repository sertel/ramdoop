/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled.io;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.DataInputBuffer;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.serializer.Deserializer;
import org.apache.hadoop.io.serializer.SerializationFactory;
import org.apache.hadoop.mapred.Task;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import redis.clients.jedis.Jedis;

import com.ramdoop.tiled.io.RedisInputFormat.RedisInputSplit;
import com.ramdoop.tiled.io.RedisInputFormat.Ref;
import com.ramdoop.tiled.io.RedisInputFormat.Strategy;

public class RedisReader<KEYIN, VALUEIN> extends RecordReader<KEYIN, VALUEIN> {

	public static String BATCH_SIZE = "ramdoop.tile.reader.batch.size";
	public static String SORT = "ramdoop.tile.reader.sort";

	public static final Log LOG = LogFactory.getLog(Task.class);

	private Jedis _redis = null;

	private KEYIN _currentKey = null;
	private VALUEIN _currentValues = null;
	private long _bytesRead = 0;
	private long _totalRecordCount = 0;
	private long _totalKeyCount = 0;
	private int _maxValuesPerKey = 0;

	// taken from IFile.Reader and Task.ValuesIterator
	private DataInputBuffer dataIn = new DataInputBuffer();
	private SerializationFactory _serializationFactory = null;
	private Deserializer<KEYIN> _keyDeserializer = null;
	private Deserializer<?> _valueDeserializer = null;

	private String _luaScriptID = null;
	private int _batchSize = 100;
	private long _start = 0;
	private long _stop = -1;
	private long _count = 0;
	@SuppressWarnings("rawtypes")
	private List _buffer = new ArrayList();
	
	private Strategy _strategy = null;
		
	private class TextDeserializer implements Deserializer<Text>{

		@Override
		public void close() throws IOException {
			// nothing
		}

		@Override
		public Text deserialize(Text arg0) throws IOException {
			return new Text(dataIn.getData());
		}

		@Override
		public void open(InputStream arg0) throws IOException {
			// nothing
		}
		
	}
	
	protected static int getBatchSize(Configuration conf){
		return conf.getInt(BATCH_SIZE, 100);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void initialize(InputSplit split, TaskAttemptContext context)
			throws IOException, InterruptedException {

		String[] locations = ((RedisInputSplit) split).getRedisLocations();
		// there is only one location specified in the RedisInputFormat!
		_redis = RedisWriter.retrieveRedisConnection(locations[0]);

		_start = ((RedisInputSplit) split).getStart();
		_stop = ((RedisInputSplit) split).getStop();
		_luaScriptID = ((RedisInputSplit) split).getScriptID();

		_serializationFactory = new SerializationFactory(
				context.getConfiguration());
		SerializationFactory serializationFactory = new SerializationFactory(
				context.getConfiguration());
		if(context.getOutputKeyClass().equals(Text.class))
			_keyDeserializer = (Deserializer<KEYIN>) new TextDeserializer();
		else
			_keyDeserializer = (Deserializer<KEYIN>) serializationFactory
				.getDeserializer(context.getOutputKeyClass());
		_keyDeserializer.open(dataIn);
		_valueDeserializer = serializationFactory.getDeserializer(context
				.getOutputValueClass());
		_valueDeserializer.open(dataIn);
		_batchSize = getBatchSize(context.getConfiguration());
		_strategy = RedisInputFormat.getStrategy(context.getConfiguration());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public boolean nextKeyValue() throws IOException, InterruptedException {
		if (_buffer.isEmpty()) {
			if (_count >= (_stop - _start)) {
				// done
				return false;
			} else {
				// query the DB -> more parallelism possible
//				long start = System.currentTimeMillis();
//				long stop = _start + _count + _batchSize - 1;
//				stop = stop > _stop ? _stop : stop;
//				_buffer = (List) _redis.evalsha(_luaScriptID.getBytes(), 0,
//						Long.toString(_start + _count).getBytes(), Long
//								.toString(stop).getBytes());
				long count = _strategy.read(_redis, _luaScriptID, _start + _count, _batchSize, _stop, new Ref() {
					@Override
					public void update(List buffer) {_buffer = buffer;}
					});
//				LOG.info("Retrieved value count from redis: " + _buffer.size()
//						+ " in " + (System.currentTimeMillis() - start) + " ms");
//				_count += _buffer.size();
				_count += count;
			}
		}
		// can be done in parallel!
		List keyValues = (List) _buffer.remove(0);
		deserialize((byte[]) keyValues.get(0), (List<byte[]>) keyValues.get(1));
		return true;
	}

	// TODO optimize this
	@SuppressWarnings("unchecked")
	private void deserialize(byte[] key, List<byte[]> values)
			throws IOException {
		_bytesRead = _bytesRead + key.length;
		// Deserializer<KEYIN> keyDeserializer = create(values.remove(0));
		// Deserializer<?> valueDeserializer = create(values.remove(0));

		Deserializer<KEYIN> keyDeserializer = _keyDeserializer;
		// keyDeserializer.open(dataIn);
		Deserializer<?> valueDeserializer = _valueDeserializer;
		// valueDeserializer.open(dataIn);

		// deserialize the key
		_currentKey = deserialize(keyDeserializer, key);
		_totalKeyCount++;
		_maxValuesPerKey = (int) Math.max(_maxValuesPerKey, values.size());
		
		// deserialize the values
		@SuppressWarnings("rawtypes")
		List deserializedValues = new ArrayList();
		for (byte[] value : values) {
			_bytesRead = _bytesRead + value.length;
			_totalRecordCount++;
			deserializedValues.add(deserialize(valueDeserializer, value));
		}
		// Note: the VALUEIN for the mapper which is actually a reducer must be
		// a List of some type.
		_currentValues = (VALUEIN) deserializedValues;
	}

	private <T> T deserialize(Deserializer<T> deserializer, byte[] value)
			throws IOException {
		dataIn.reset(value, value.length);
		T deserializedValue = deserializer.deserialize(null);// always create
																// new instance
		return deserializedValue;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	private <T> Deserializer<T> create(byte[] serializedClassName)
			throws IOException {
		try {
			Class<T> clz = (Class<T>) Class.forName(new String(
					serializedClassName));
			Deserializer<T> deserializer = _serializationFactory
					.getDeserializer(clz);
			deserializer.open(dataIn);
			return deserializer;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new IOException(e);
		}
	}

	@Override
	public KEYIN getCurrentKey() throws IOException, InterruptedException {
		return _currentKey;
	}

	@Override
	public VALUEIN getCurrentValue() throws IOException, InterruptedException {
		return _currentValues;
	}

	// report progress
	@Override
	public float getProgress() throws IOException, InterruptedException {
		// we need explicit casting here because otherwise we use integer division
		return ((float)_count / (float)(_stop - _start));
	}

	@Override
	public void close() throws IOException {
		System.out.println("Bytes read: " + _bytesRead);
		System.out.println("Total key count: " + _totalKeyCount);
		System.out.println("Total record count: " + _totalRecordCount);
		System.out.println("Max values per key: " + _maxValuesPerKey);
		_redis.disconnect();
	}
}
