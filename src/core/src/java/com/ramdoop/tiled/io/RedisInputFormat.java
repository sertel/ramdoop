/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled.io;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.InputFormat;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import redis.clients.jedis.Jedis;

public class RedisInputFormat<K, V> extends InputFormat<K, V> {

	// this is the full path to a redis instance
	public static String INTERMEDIATE_TILE_LOCATION = "ramdoop.tile.intermediate";
	public static String STRATEGY = "ramdoop.tile.intermediate.strategy";

	public interface Ref {
		public void update(@SuppressWarnings("rawtypes") List buffer);
	}

	public enum Strategy {
		TILE_KEYS(Script.TILE_KEY_PREPARE, Script.TILE_KEY_READ), 
		DB_MOVE(Script.DB_MOVE_PREPARE, Script.DB_MOVE_READ);

		private Script _prepareScript = null;
		private Script _readScript = null;

		Strategy(Script prepare, Script read) {
			_prepareScript = prepare;
			_readScript = read;
		}

		public Script getPrepareScript() {
			return _prepareScript;
		}

		public Script getReadScript() {
			return _readScript;
		}

		public int read(Jedis redis, String scriptID, long start,
				int batchSize, long stop, Ref buffer) {
			return _readScript.execute(redis, scriptID, start, batchSize, stop,
					buffer);
		}
	}

	private enum Script {
		TILE_KEY_PREPARE {
			@Override
			public int execute(Jedis redis, String scriptID, long start,
					int batchSize, long stop, Ref buffer) {
				// TODO Auto-generated method stub
				return 0;
			}
		},
		TILE_KEY_READ {
			@Override
			public int execute(Jedis redis, String scriptID, long start,
					int batchSize, long stop, Ref buffer) {
				long stopNow = start + batchSize - 1;
				// Redis includes the last element into the retrieval
				stopNow = stopNow > (stop-1) ? (stop-1) : stopNow;
				List<?> result = (List<?>) redis.evalsha(scriptID.getBytes(),
						0, Long.toString(start).getBytes(),
						Long.toString(stopNow).getBytes());
				buffer.update(result);
				return result.size();
			}
		},
		SCAN {
			@Override
			public int execute(Jedis redis, String scriptID, long start,
					int batchSize, long stop, Ref buffer) {
				// TODO Auto-generated method stub
				return 0;
			}
		},
		DB_MOVE_PREPARE {
			@Override
			public int execute(Jedis redis, String scriptID, long start,
					int batchSize, long stop, Ref buffer) {
				// TODO Auto-generated method stub
				return 0;
			}
		},
		DB_MOVE_READ {
			@Override
			public int execute(Jedis redis, String scriptID, long start,
					int batchSize, long stop, Ref buffer) {
				List<?> result = (List<?>) redis.evalsha(scriptID.getBytes(),
						0, Long.toString(start).getBytes());
				buffer.update(result);
				return 1;
			}
		};

		abstract public int execute(Jedis redis, String sciptID, long start,
				int batchSize, long stop, Ref buffer);
	}

	/**
	 * A split contains references to the keys that belong to this split and the
	 * node locations.
	 * 
	 * @author sertel
	 * 
	 */
	protected static class RedisInputSplit extends InputSplit implements
			Writable {

		private String[] _locations = null;
		private long _start = -1;
		private long _stop = -1;
		private String _scriptID = null;

		public RedisInputSplit() {
			// default constructor for deserialization
		}

		public RedisInputSplit(String[] locations, long start, long stop,
				String scriptID) {
			_locations = locations;
			_start = start;
			_stop = stop;
			_scriptID = scriptID;
		}

		@Override
		public long getLength() throws IOException, InterruptedException {
			return _stop - _start;
		}

		/**
		 * In order to again achieve task locality, we have to make sure that
		 * these strings actually match the URLs returned from the file formats.
		 */
		@Override
		public String[] getLocations() throws IOException, InterruptedException {
			// this is problematic because the Hadoop framework tries to perform
			// some fancy stuff. therefore, we gain again task locality via the
			// TiledTaskSelector.
			String[] hosts = new String[_locations.length];
			for (int i = 0; i < _locations.length; i++) {
				hosts[i] = new URL(_locations[i]).getHost();
			}
			return hosts;
		}

		protected String getScriptID() {
			return _scriptID;
		}

		protected String[] getRedisLocations() {
			return _locations;
		}

		public long getStart() {
			return _start;
		}

		public long getStop() {
			return _stop;
		}

		@Override
		public void write(DataOutput arg0) throws IOException {
			arg0.writeLong(_start);
			arg0.writeLong(_stop);
			arg0.writeInt(_locations.length);
			for (String location : _locations) {
				byte[] locBytes = location.getBytes();
				arg0.writeInt(locBytes.length);
				arg0.write(locBytes);
			}
			byte[] locBytes = _scriptID.getBytes();
			arg0.writeInt(locBytes.length);
			arg0.write(locBytes);
		}

		@Override
		public void readFields(DataInput arg0) throws IOException {
			_start = arg0.readLong();
			_stop = arg0.readLong();
			int locCount = arg0.readInt();
			_locations = new String[locCount];
			for (int i = 0; i < locCount; i++) {
				// TODO use real buffer
				byte[] buf = new byte[arg0.readInt()];
				arg0.readFully(buf);
				_locations[i] = new String(buf);
			}
			byte[] buf = new byte[arg0.readInt()];
			arg0.readFully(buf);
			_scriptID = new String(buf);
		}

		public String toString() {
			return "Redis Split: " + Arrays.deepToString(_locations) + ":"
					+ _start + ":" + _stop;
		}
	}

	protected static Strategy getStrategy(Configuration conf) {
		return conf.getEnum(STRATEGY, Strategy.TILE_KEYS);
	}

	/**
	 * Note, this function is called on the client-side to understand where to
	 * schedule the according tasks!
	 */
	@Override
	public List<InputSplit> getSplits(JobContext context) throws IOException,
			InterruptedException {
		List<InputSplit> splits = new ArrayList<InputSplit>();

		// TODO create splits by taking the reference to the according DB to be
		// queried from the job context. afterwards the splits are created by
		// looking into the db and finding the partitions.
		String location = context.getConfiguration().get(
				INTERMEDIATE_TILE_LOCATION);
		Jedis redis = RedisWriter.retrieveRedisConnection(location);

		// Creating multiple splits allows parallel execution.
		// However, one split per key is too fine-grained! -> key range for
		// input split

		int concurrencyLevel = context.getConfiguration().getInt(
				"mapred.tasktracker.map.tasks.maximum", 2);

		// range query support -> returns key count
		Map<Script, String> scripts = registerLUAScript(redis);

		Strategy strategy = getStrategy(context.getConfiguration());
		System.out.println("Selected reading strategy: " + strategy.name());
		
		int batchSize = RedisReader.getBatchSize(context.getConfiguration());
		long start = System.currentTimeMillis();

		long keyCount = (long) redis.evalsha(
				scripts.get(strategy.getPrepareScript()), 0,
				Integer.toString(batchSize));
		System.out.println("Creating tile keys list took "
				+ (System.currentTimeMillis() - start) + " ms.");
		long length = keyCount > 1 ? keyCount / concurrencyLevel : 0;
		System.out.println("Key count: " + keyCount + " concurrency level: "
				+ concurrencyLevel + " length: " + length);
		for (int i = 0; i < concurrencyLevel - 1 && length > 0; i++) {
			splits.add(new RedisInputSplit(new String[] { location }, i
					* length, ((i + 1) * length), scripts.get(strategy
					.getReadScript())));
		}
		// get the rest
		splits.add(new RedisInputSplit(new String[] { location },
				(concurrencyLevel - 1) * length, keyCount, scripts.get(strategy
						.getReadScript())));
		redis.disconnect();

		return splits;
	}

	/**
	 * Note, this is being called at the "node-site" when the tasks are actually
	 * being executed.
	 */
	@Override
	public RecordReader<K, V> createRecordReader(InputSplit split,
			TaskAttemptContext context) throws IOException,
			InterruptedException {
		RedisReader<K, V> reader = new RedisReader<K, V>();
		reader.initialize(split, context);
		return reader;
	}

	protected static Map<Script, String> registerLUAScript(Jedis redis) {
		redis.scriptFlush();

		Map<Script, String> scripts = new HashMap<>();

		String keyScript = "redis.call('DEL', 'tile_keys') "
				+ "for k,v in pairs(redis.call('KEYS', '*')) do "
				+ "redis.call('RPUSH', 'tile_keys', v) " + "end "
				+ "return redis.call('LLEN', 'tile_keys')";
		scripts.put(Script.TILE_KEY_PREPARE, redis.scriptLoad(keyScript));

		// this script is meant to perform a shared scan over the keys. the scan
		// is shared across the requests.
		String scanScript = "local db = ARGV[1] or 11 "
				+ "local count = ARGV[2] or 100 " + "local s=0 "
				+ "redis.call('SELECT', '1') "
				+ "if redis.call('EXISTS', 'scan_cursor') == 1 then "
				+ "s=redis.call('GET', 'scan_cursor') " + "end "
				+ "redis.call('SET', 'scan_cursor', s+count) "
				+ "redis.call('SELECT', db) "
				+ "if s+count > redis.call('DBSIZE') then " + "return nil "
				+ "else "
				+ "local result = redis.call('SCAN', s, 'COUNT', count) "
				+ "return redis.call('MGET', unpack(result[2])) " + "end";
		scripts.put(Script.SCAN, redis.scriptLoad(scanScript));

		String tileKeyScan = "local start = ARGV[1] "
				+ "local stop = ARGV[2] "
				+ "local keyz = redis.call('LRANGE', 'tile_keys', start, stop) "
				+ "local result = {} " + "for k,v in ipairs(keyz) do "
				+ "local valuez = redis.call('LRANGE', v, 0, -1) "
				+ "result[k] = {v, valuez} " + "end " + "return result";
		scripts.put(Script.TILE_KEY_READ, redis.scriptLoad(tileKeyScan));

		String dbTiling = "local batchSize = tonumber(ARGV[1]) "
				+ "redis.call('SELECT', 0) "
				// this sorts the keys
				+ "local keyz = redis.call('KEYS', '*') "
				// leave the first batch on this db -> this turns out to be super costly! need to resemble to pcall
//				+ "for i = 0, (batchSize-1) do "
//				+ "table.remove(keyz, 1) "
//				+ "end "
				+ "local count = 0 "
				+ "local targetDB = 1 " 
				+ "for i = batchSize+1, #keyz do "
//				+ "for k,v in ipairs(keyz) do "
//				+ "redis.call('MOVE', v, targetDB) "
				+ "redis.call('MOVE', keyz[i], targetDB) "
				+ "count = count + 1 "
				+ "if count == batchSize then " 
				+ "targetDB = targetDB + 1 "
				+ "count = 0 " + "end " 
				+ "end " 
				+ "return targetDB";
		scripts.put(Script.DB_MOVE_PREPARE, redis.scriptLoad(dbTiling));

		String dbGetAll = "local targetDB = ARGV[1] "
				+ "redis.call('SELECT', targetDB) "
				// this sorts the keys
				+ "local keyz = redis.call('KEYS', '*') "
				+ "local result = {} " + "for k,v in ipairs(keyz) do "
				+ "local valuez = redis.call('LRANGE', v, 0, -1) "
				+ "result[k] = {v, valuez} " + "end " + "return result";
		scripts.put(Script.DB_MOVE_READ, redis.scriptLoad(dbGetAll));
		return scripts;
	}

}
