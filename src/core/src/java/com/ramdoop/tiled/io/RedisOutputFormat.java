/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled.io;

import java.io.IOException;

import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.OutputCommitter;
import org.apache.hadoop.mapreduce.OutputFormat;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.output.FileOutputCommitter;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.ReflectionUtils;

public class RedisOutputFormat<K, V> extends OutputFormat<K, V> {

	/**
	 * This will be a writer for a specific partition of a job. Since all mini
	 * jobs use the same partition function, we have to derive the target redis
	 * instance from the partition function and the partition in which the keys
	 * fall into.
	 */
	@Override
	public RecordWriter<K, V> getRecordWriter(TaskAttemptContext context)
			throws IOException, InterruptedException {
		Partitioner<K, V> partitioner = retrievePartitioner(context);
		return new RedisWriter<K, V>(context, partitioner, context.getNumReduceTasks());
	}

	@SuppressWarnings("unchecked")
	protected Partitioner<K, V> retrievePartitioner(TaskAttemptContext context)
			throws IOException {
		try {
			return (Partitioner<K, V>) ReflectionUtils.newInstance(
					context.getPartitionerClass(), context.getConfiguration());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new IOException(e);
		}
	}

	// Same implementation that DBOutputFormat uses
	@Override
	public void checkOutputSpecs(JobContext context) throws IOException,
			InterruptedException {
		// nothing
	}

	// Same implementation that DBOutputFormat uses
	@Override
	public OutputCommitter getOutputCommitter(TaskAttemptContext context)
			throws IOException, InterruptedException {
		return new FileOutputCommitter(FileOutputFormat.getOutputPath(context),
				context);
	}

}
