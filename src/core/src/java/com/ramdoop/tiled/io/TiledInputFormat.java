package com.ramdoop.tiled.io;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.InputFormat;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFilter;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.util.ReflectionUtils;

public class TiledInputFormat<K, V> extends FileInputFormat<K, V> {

	public static String ORIGINAL_INPUT_FORMAT = "ramdoop.original.input.format";
	public static String FILTERED_INPUTS = "ramdoop.input.filtered";
	public static String CURRENT_SPLIT = "ramdoop.current.split";

	public static class FilteredFileSplit extends InputSplit implements Writable {

		private FileSplit _fSplit = null;

		public FilteredFileSplit() {
		}

		public FilteredFileSplit(Path file, long start, long length, String[] hosts) {
			_fSplit = new FileSplit(file, start, length, hosts);
		}

		@Override
		public void readFields(DataInput in) throws IOException {
			// the values will be overridden (corrected) by the below call
			_fSplit = new FileSplit(null, 0, 0, null);
			_fSplit.readFields(in);
		}

		@Override
		public void write(DataOutput out) throws IOException {
			_fSplit.write(out);
		}

		@Override
		public long getLength() throws IOException, InterruptedException {
			return _fSplit.getLength();
		}

		@Override
		public String[] getLocations() throws IOException, InterruptedException {
			return _fSplit.getLocations();
		}
	}

	/**
	 * This is called job tracker-site.
	 */
	@Override
	public List<InputSplit> getSplits(JobContext context) throws IOException {
		String blocks = context.getConfiguration().get("mapred.input.dir", "");
		// the job has been assigned to a location where it is desirable to
		// execute this tile. it will be evaluated by TiledTaskSelector.
		String[] filters = findFilters(context.getConfiguration());
		Set<String> filtered = new HashSet<String>();
		Collections.addAll(filtered, filters);
		return deserializeSplits(blocks, filtered);
	}

	/**
	 * This is called worker-site. <br>
	 * Note that the split locations are only used by the scheduler; worker-site
	 * the split location is not used at all because the task was already
	 * assigned. From that point on the call through HDFS ends hopefully up at a
	 * local copy. Hence, it is essentially this call that decides which replica
	 * to use! Conclusion: the order of the host is completely irrelevant.
	 */
	@Override
	public RecordReader<K, V> createRecordReader(InputSplit split, TaskAttemptContext context)
			throws IOException, InterruptedException {
		if (split instanceof FilteredFileSplit) {
			if (!(getOriginalInputFormat(context) instanceof SequenceFileInputFormat)) {
				throw new IOException("Incompatible input format for filtered input! Format :"
						+ getOriginalInputFormat(context).getClass().getName());
			} else {
				SequenceFileInputFilter<K, V> filter = new SequenceFileInputFilter<>();
				System.out.println(context.getConfiguration().get(SequenceFileInputFilter.FILTER_CLASS));
				context.getConfiguration().set(CURRENT_SPLIT,
						((FilteredFileSplit) split)._fSplit.getPath().getName());
				return filter.createRecordReader(((FilteredFileSplit) split)._fSplit, context);
			}
		} else {
			return getOriginalInputFormat(context).createRecordReader(split, context);
		}
	}

	private String[] findFilters(Configuration conf) {
		return conf.get(FILTERED_INPUTS, "").split("\\|");
	}

	@SuppressWarnings("unchecked")
	private InputFormat<K, V> getOriginalInputFormat(TaskAttemptContext context) {
		System.out.println("Original input format: "
				+ context.getConfiguration().get(ORIGINAL_INPUT_FORMAT));
		return (InputFormat<K, V>) ReflectionUtils.newInstance(
				context.getConfiguration().getClass(ORIGINAL_INPUT_FORMAT, TextInputFormat.class),
				context.getConfiguration());
	}

	public static String serializeSplits(List<InputSplit> splits) {
		StringBuffer serialized = new StringBuffer();
		for (InputSplit split : splits) {
			try {
				serialized.append(Arrays.deepToString(split.getLocations()));
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			serialized.append(split.toString());
			serialized.append("|");
		}
		serialized.deleteCharAt(serialized.length() - 1);
		return serialized.toString();
	}

	public static List<InputSplit> deserializeSplits(String serializedSplits) {
		return deserializeSplits(serializedSplits, Collections.<String> emptySet());
	}

	public static List<InputSplit> deserializeSplits(String serializedSplits, Set<String> filtered) {
		// System.out.println("Serialized splits: " + serializedSplits);
		List<InputSplit> inputSplits = new ArrayList<InputSplit>();
		String[] splits = serializedSplits.split("\\|");
		for (String split : splits) {
			// host list is given via [host1, host2, host3, etc] see
			// Arrays.deepToString()
			int hostsEnd = split.indexOf("]");
			String[] hosts = split.substring(1, hostsEnd).split(", ");
			String blockInfo = split.substring(hostsEnd + 1);

			// see FileSplit.toString()
			int s = blockInfo.lastIndexOf(":");
			int t = blockInfo.lastIndexOf("+");
			String file = blockInfo.substring(0, s);
			long start = Long.parseLong(blockInfo.substring(s + 1, t));
			long length = Long.parseLong(blockInfo.substring(t + 1));

			if (filtered.contains(file)) {
				inputSplits.add(new FilteredFileSplit(new Path(file), start, length, hosts));
			} else {
				// if we give more than one option then we can not make sure
				// that the scheduler executes the tasks of this job at the same
				// node. on the other hand, how do we then deal with stragglers
				// and
				// crashed nodes? -> we have to provide all nodes and make sure
				// the
				// scheduler always takes the same for a mini job!
				inputSplits.add(new FileSplit(new Path(file), start, length, hosts));
			}
		}
		return inputSplits;
	}

}
