package com.ramdoop.tiled.io;

import java.io.IOException;

import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

public class PartitionedRedisOutputFormat<K, V> extends RedisOutputFormat<K, V> {

	public static String PARTITION_COUNT = "ramdoop.partition.count";

	@Override
	public RecordWriter<K, V> getRecordWriter(TaskAttemptContext context)
			throws IOException, InterruptedException {
		Partitioner<K, V> partitioner = retrievePartitioner(context);
		// we can't use getNumReduceTasks() because they will be 0 for the first tile phase
		return new PartitionRedisWriter<K, V>(context, partitioner, context
				.getConfiguration().getInt(PARTITION_COUNT, 1));
	}

}
