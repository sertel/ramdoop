/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled.io;

import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.DataOutputBuffer;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.serializer.SerializationFactory;
import org.apache.hadoop.io.serializer.Serializer;
import org.apache.hadoop.mapred.Task;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Protocol;

public class RedisWriter<K, V> extends RecordWriter<K, V> {

	protected static final Log LOG = LogFactory.getLog(Task.class);

	public static String OUTPUT_PREFIX = "ramdoop.output.prefix";
//	public static String ROOT_JOB_ID = "ramdoop.root.job.id";
	public static String PARTITION_DIFFERENCE = "ramdoop.partition.diff";
	public static String PARTITION_HOLES = "ramdoop.partition.holes";

	// Identifier to store all keys that belong to this partition.
	protected Partitioner<K, V> _partitioner = null;
	private int _partitionCount = -1;

	private Jedis _redis = null;

	// taken from IFile.Writer
	private DataOutputBuffer keyBuffer = new DataOutputBuffer();
	private DataOutputBuffer valueBuffer = new DataOutputBuffer();
	private Serializer<K> keySerializer;
	private Serializer<V> valueSerializer;

	@SuppressWarnings("unused")
	private Class<K> _outputKeyClass = null;
	@SuppressWarnings("unused")
	private Class<V> _outputValueClass = null;

	private Configuration _config = null;

	private final int _maxBatchSize;
	private int _currentBatchSize = 0;
	private long _bytesWritten = 0;

	private long _recordCounter = 0;
	private Pipeline _pipeline = null;
	
//	private class LongSerializer implements Serializer<LongWritable>{
//
//		@Override
//		public void close() throws IOException {
//			// nothing
//		}
//
//		@Override
//		public void open(OutputStream arg0) throws IOException {
//			// nothing
//		}
//
//		@SuppressWarnings("unchecked")
//		@Override
//		public void serialize(LongWritable arg0) throws IOException {
//			((TextDataOutputBuffer) keyBuffer).buf = Long.toString(arg0.get()).getBytes();
//			((TextDataOutputBuffer) keyBuffer).length = ((TextDataOutputBuffer) keyBuffer).buf.length;
//		}
//		
//	}

	private class TextSerializer implements Serializer<Text> {

		@Override
		public void close() throws IOException {
			// nothing
		}

		@Override
		public void open(OutputStream arg0) throws IOException {
			// nothing
		}

		@SuppressWarnings("unchecked")
		@Override
		public void serialize(Text txt) throws IOException {
			((TextDataOutputBuffer) keyBuffer).buf = txt.getBytes();
			((TextDataOutputBuffer) keyBuffer).length = txt.getLength();
		}
	}

	private class TextDataOutputBuffer extends DataOutputBuffer {
		byte[] buf = null;
		int length = -1;

		public int getLength() {
			return length;
		}

		public byte[] getData() {
			return buf;
		}
	}

	@SuppressWarnings("unchecked")
	public RedisWriter(TaskAttemptContext context,
			Partitioner<K, V> partitioner, int partitionCount) {
		_config = context.getConfiguration();
		// the location is derived from the partitioner and the key which
		// identifies the Redis instance where these results have to go.
		_partitioner = partitioner;
		_partitionCount = partitionCount;

		// store the type information of the key and the values
		SerializationFactory serializationFactory = new SerializationFactory(
				context.getConfiguration());
		if (context.getOutputKeyClass().equals(Text.class)) {
			keySerializer = (Serializer<K>) new TextSerializer();
			keyBuffer = new TextDataOutputBuffer();
//		}else if(context.getOutputKeyClass().equals(LongWritable.class)){
//			keySerializer = (Serializer<K>) new LongSerializer();
//			keyBuffer = new TextDataOutputBuffer();			
		}else {
			keySerializer = serializationFactory
					.getSerializer((Class<K>) context.getOutputKeyClass());
		}
		_outputKeyClass = (Class<K>) context.getOutputKeyClass();
		valueSerializer = serializationFactory.getSerializer((Class<V>) context
				.getOutputValueClass());
		_outputValueClass = (Class<V>) context.getOutputValueClass();
		try {
			keySerializer.open(keyBuffer);
			valueSerializer.open(valueBuffer);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		_maxBatchSize = context.getConfiguration().getInt(
				"io.file.buffer.size", 4096);
		_currentBatchSize = 0;

		// TODO store type info once

		// TODO compression. see SequenceFileOutputFormat.
	}

	/**
	 * Here we write all to the same Redis it seems. Check on that again! ->
	 * This is ok, because MR creates one writer for one partition in a Reduce
	 * task. However, we identify the partition and therewith the Redis instance
	 * only when we receive the first key.
	 * 
	 * (non-Javadoc)
	 * 
	 * @see org.apache.hadoop.mapreduce.RecordWriter#write(java.lang.Object,
	 *      java.lang.Object)
	 */
	@Override
	public void write(K key, V value) throws IOException, InterruptedException {
		if (_redis == null) {
			_redis = findRedisInstance(key);
			_pipeline = _redis.pipelined();
		}

		write(key, value, _pipeline, _currentBatchSize);
	}

	private void executeBatch(Pipeline pipe) {
		pipe.sync();
//		List<Object> responses = pipe.syncAndReturnAll();
//		for(Object response : responses){
//			
//		}
		pipe.cleanResponses();
	}

	protected final int write(K key, V value, Pipeline pipeline, int currentBatchSize)
			throws IOException, InterruptedException {
//		System.out.println("Writing key: " + key.toString() + " value: " + value.toString());

		// TODO serialize only once! -> can't do that because every key only
		// arrives once! -> not true if this is a reducer-less job!
		keySerializer.serialize(key);
		byte[] serializedKey = keyBuffer.getData();
		int length = keyBuffer.getLength();
		// TODO not needed for Text
		serializedKey = Arrays.copyOfRange(serializedKey, 0, length);
		// if (!_redis.exists(serializedKey)) {
		// // place the type info first
		// _redis.rpush(serializedKey, _outputKeyClass.getName().getBytes(),
		// _outputValueClass.getName().getBytes());
		// }
		valueSerializer.serialize(value);
		byte[] serializedValue = valueBuffer.getData();
		serializedValue = Arrays.copyOfRange(serializedValue, 0,
				valueBuffer.getLength());
		_bytesWritten = _bytesWritten + serializedKey.length + serializedValue.length;
		pipeline.rpush(serializedKey, serializedValue);

		keyBuffer.reset();
		valueBuffer.reset();

		_recordCounter++;
//		if (_recordCounter % 1000 == 0) {
//			logRecordCount();
//		}

		int size = serializedKey.length + serializedKey.length;		
		if ((currentBatchSize + size) > _maxBatchSize) {
			executeBatch(pipeline);
			return 0;
		}else{
			return currentBatchSize + size;
		}
	}
	
	protected void logRecordCount(){
		System.out.println("Output record count: " + _recordCounter);
		LOG.info("Output record count: " + _recordCounter);
	}

	protected int getPartition(K key) {
		return _partitioner.getPartition(key, null, _partitionCount);
	}

	/**
	 * Deterministic mapping from partition to Redis instance.
	 * 
	 * @param key
	 * @return
	 */
	private Jedis findRedisInstance(K key) {
		// Most partitioner are working solely on a key basis. I would like to
		// know which partitioner doesn't. (That's why the null pointer.)
		int partition = _partitioner.getPartition(key, null, _partitionCount);
		return findRedisInstance(partition);
	}

	protected Jedis findRedisInstance(int partition) {
		String url = constructTileLocationReference(_config, partition);
		Jedis redis = retrieveRedisConnection(url);
		// create and connect to a DB right for this job
//		redis.select(_rootJobID);
		return redis;
	}

	public static Jedis retrieveRedisConnection(String url) {
		URL redisURL = null;
		try {
			redisURL = new URL(url);
		} catch (MalformedURLException e) {
			System.out.println("Redis URL is no good!!!");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		Jedis redis = redisURL.getPort() == -1 ? new Jedis(redisURL.getHost(),
				Protocol.DEFAULT_PORT, 1000 * Protocol.DEFAULT_TIMEOUT)
				: new Jedis(redisURL.getHost(), redisURL.getPort(),
						1000 * Protocol.DEFAULT_TIMEOUT);
//		LOG.info("Redis URL: " + url + " db: " + 0);
		redis.setInputBufferSize(200000);
		redis.setOutputBufferSize(200000);
		redis.connect();
		// db 0 is always our default now!
		redis.select(0);
		return redis;
	}

	@Override
	public void close(TaskAttemptContext context) throws IOException,
			InterruptedException {
		closeRedis(_redis, _pipeline);
		System.out.println("Bytes written: " + _bytesWritten);
		keySerializer.close();
		valueSerializer.close();
	}

	protected void closeRedis(Jedis redis, Pipeline pipeline) {
//		System.out.println("Closing redis connection now!");
		if (redis == null) {
//			System.out.println("Nothing was written to this writer!");
		} else {
			executeBatch(pipeline);
			redis.disconnect();
			logRecordCount();
		}
	}

	public static String constructTileLocationReference(Configuration conf,
			int partitionNum) {
		String outputPrefix = conf.get(RedisWriter.OUTPUT_PREFIX);
		int start = conf.getInt(PARTITION_DIFFERENCE, 0);
		String holes = conf.get(PARTITION_HOLES);
		int partitionRef = start + partitionNum;
		if(holes != null){
			String[] holesSplit = holes.split("\\|");
//			System.out.println("Detected range holes: " + Arrays.deepToString(holesSplit));
			int[] iHoles = new int[holesSplit.length];
			for(int i=0;i<holesSplit.length;i++)
				iHoles[i] = Integer.parseInt(holesSplit[i]);
			
			for(Integer hole : iHoles)
				if( start <= hole && hole <= partitionRef) 
					partitionRef++;
		}
		String reference = outputPrefix + partitionRef;
//		System.out.println("Calculated node for partition " + partitionNum + " is " + reference);
		return reference;
	}
}
