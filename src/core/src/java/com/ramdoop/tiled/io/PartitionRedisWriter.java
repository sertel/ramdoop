package com.ramdoop.tiled.io;

import java.io.IOException;
import java.util.Arrays;

import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

public class PartitionRedisWriter<K, V> extends RedisWriter<K, V> {

	private Jedis[] _clients = null;
	private Pipeline[] _pipelines = null;
	
	private int[] _recordCounters = null;
	private long[] _maxLatency = null;
	private long[] _accLatency = null;
	private int[] _currentBatchSize = null;
	
	public PartitionRedisWriter(TaskAttemptContext context,
			Partitioner<K, V> partitioner, int partitionCount) {
		super(context, partitioner, partitionCount);

		_clients = new Jedis[partitionCount];
		_pipelines = new Pipeline[partitionCount];
		
		// setup all pipelines here already
		for (int i = 0; i < partitionCount; i++) {
			Jedis redis = super.findRedisInstance(i);
			_clients[i] = redis;
			_pipelines[i] = redis.pipelined();
		}
		_recordCounters = new int[partitionCount];
		_maxLatency = new long[partitionCount];
		_accLatency = new long[partitionCount];
		_currentBatchSize = new int[partitionCount];
		Arrays.fill(_maxLatency, 0);
		Arrays.fill(_accLatency, 0);
		Arrays.fill(_recordCounters, 0);
		Arrays.fill(_currentBatchSize, 0);
	}

	@Override
	public void write(K key, V value) throws IOException, InterruptedException {
		int partition = super.getPartition(key);
		long start = System.currentTimeMillis();
		_currentBatchSize[partition] = super.write(key, value, _pipelines[partition], _currentBatchSize[partition]);
		long stop = System.currentTimeMillis();
		long length = stop - start;
		_recordCounters[partition] = _recordCounters[partition] + 1;
		_maxLatency[partition] = Math.max(_maxLatency[partition], length);
		_accLatency[partition] = _accLatency[partition] + length;
	}
		
	protected void logRecordCount(){
		super.logRecordCount();
		String logOutput = createLogOutput();
		LOG.info(logOutput);
		Arrays.fill(_maxLatency, 0);
		Arrays.fill(_accLatency, 0);
	}

	private String createLogOutput(){
		StringBuilder s = new StringBuilder();
		for(int i=0; i<_accLatency.length; i++){
			s.append(" Partition: ");
			s.append(i);
			s.append(" count: ");
			s.append(_recordCounters[i]);
			s.append(" max latency: ");
			s.append(_maxLatency[i]);
			s.append(" total I/O time: ");
			s.append(_accLatency[i]);
		}
		return s.toString();
	}
	
	protected void closeRedis(Jedis redis, Pipeline pipeline){
		for (int i = 0; i < _clients.length; i++) {
			super.closeRedis(_clients[i], _pipelines[i]);
		}
		System.out.println(createLogOutput());
	}
}
