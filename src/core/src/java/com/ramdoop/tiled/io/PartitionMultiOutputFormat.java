/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled.io;

import java.io.IOException;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.Partitioner;
import org.apache.hadoop.mapred.RecordWriter;
import org.apache.hadoop.mapred.lib.MultipleSequenceFileOutputFormat;
import org.apache.hadoop.util.Progressable;
import org.apache.hadoop.util.ReflectionUtils;

/**
 * Again only old API possible.
 * 
 * @author sertel
 * 
 * @param <K>
 * @param <V>
 */
public class PartitionMultiOutputFormat<K, V> extends
		MultipleSequenceFileOutputFormat<K, V> {

	private Partitioner<K, V> _partitioner = null;
	private int _numReducers = -1;

	@SuppressWarnings("unchecked")
	@Override
	protected RecordWriter<K, V> getBaseRecordWriter(FileSystem fs,
			JobConf job, String name, Progressable arg3) throws IOException {
		_numReducers = job.getNumReduceTasks();
		_partitioner = (Partitioner<K, V>) ReflectionUtils.newInstance(
				job.getPartitionerClass(), job);
		return super.getBaseRecordWriter(fs, job, name, arg3);
	}

	@Override
	protected String generateFileNameForKeyValue(K key, V value, String leaf) {
		return new Path("parition-"
				+ _partitioner.getPartition(key, value, _numReducers), leaf)
				.toString();
	}
}
