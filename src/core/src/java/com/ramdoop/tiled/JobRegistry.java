/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled;

import java.util.Collection;
import java.util.LinkedHashMap;

import org.apache.hadoop.mapreduce.Job;

/**
 * A simple registry for job configurations.
 * 
 * @author sertel
 * 
 */
public class JobRegistry {

	// use a linked hash map in order to preserve the registration order of the
	// jobs which is important for execution.
	private LinkedHashMap<String, Job> _registry = new LinkedHashMap<>();

	protected void register(Job conf) {
		String jobName = conf.getJobName();
		if (_registry.containsKey(jobName)) {
			throw new RuntimeException("Duplicate registry entry detected for job '" + jobName
					+ "'!");
		}
		_registry.put(jobName, conf);
	}

	protected Collection<Job> getAllRegisteredJobs() {
		return _registry.values();
	}

}
