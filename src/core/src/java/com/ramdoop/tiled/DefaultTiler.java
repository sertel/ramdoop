package com.ramdoop.tiled;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.mapreduce.InputSplit;

import com.ramdoop.tiled.api.Tiler;

/**
 * This tiling implementation assumes there are no restriction on the tiling
 * algorithm and therefore optimizes for locality.<br>
 * Examples: Sort, Word count
 * 
 * @author sertel
 * 
 */
public class DefaultTiler extends Tiler {

	@Override
	public Map<String, List<List<InputSplit>>> tile(List<InputSplit> splits, long tileSize) throws IOException, InterruptedException {
		Map<String, List<List<InputSplit>>> result = new HashMap<>();
		Map<String, List<InputSplit>> grpedSplits = super.grpByLocation(splits);
		for(Map.Entry<String, List<InputSplit>> entry : grpedSplits.entrySet()){
			result.put(entry.getKey(), tileOnLocation(entry.getValue(), tileSize));
		}
		verify(splits.size(), getNumSplits(result.values()));
		return result;
	}

	private List<List<InputSplit>> tileOnLocation(List<InputSplit> splits, long tileSize) throws IOException, InterruptedException{
		List<List<InputSplit>> tiles = new ArrayList<>();
		long currentTileSize = 0;
		int next = 0;
		for (int i = 0; i < splits.size(); i++) {
			InputSplit split = splits.get(i);
			currentTileSize += split.getLength();
			if (currentTileSize > tileSize) {
				tiles.add(splits.subList(next, i + 1));
				next = i + 1;
				currentTileSize = 0;
			}
		}

		if (currentTileSize > 0) tiles.add(splits.subList(next, splits.size()));

		return tiles;
	}

}
