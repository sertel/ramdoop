/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.tiled;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.DataInputBuffer;
import org.apache.hadoop.io.RawComparator;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.RawKeyValueIterator;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.ManualReducer;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.OutputCommitter;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.StatusReporter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.util.Progress;
import org.apache.hadoop.util.ReflectionUtils;

/**
 * This is a mapper that actually runs a provided reducer. This allows us to
 * implement a reduce-only job without hacking into Hadoop. Note though that
 * this mapper assumes to receive each key only once and therefore must be used
 * with the according reader that fulfills this requirement.
 * 
 * @author sertel
 * 
 * @param <KEYIN>
 * @param <VALUEIN>
 * @param <KEYOUT>
 * @param <VALUEOUT>
 */
public class TiledReducer<KEYIN, VALUEIN, KEYOUT, VALUEOUT> extends
		Mapper<KEYIN, VALUEIN, KEYOUT, VALUEOUT> {

	private Reducer<KEYIN, VALUEIN, KEYOUT, VALUEOUT> _reducer = null;
	private Reducer<KEYIN, VALUEIN, KEYOUT, VALUEOUT>.Context _reduceContext = null;

	@SuppressWarnings({"rawtypes", "unchecked"})
	private class TiledReduceContext extends Reducer<KEYIN, VALUEIN, KEYOUT, VALUEOUT>.Context {

		private Mapper.Context _mapperContext = null;
		
		public TiledReduceContext(Mapper.Context mapperContext, Reducer reducer, Configuration conf, TaskAttemptID taskid,
				RawKeyValueIterator input, Counter inputKeyCounter, Counter inputValueCounter,
				RecordWriter output, OutputCommitter committer, StatusReporter reporter,
				RawComparator comparator, Class keyClass, Class valueClass) throws IOException,
				InterruptedException {
			reducer.super(conf, taskid, input, inputKeyCounter, inputValueCounter, output,
					committer, reporter, comparator, keyClass, valueClass);
			_mapperContext = mapperContext;
//			RedisReader.LOG.info("TiledReduceContext created.");
		}
		
		@Override
		public Counter getCounter(Enum<?> counterName) {
//			RedisReader.LOG.info("Counter requested: " + counterName + " value: " + _mapperContext.getCounter(counterName));
			return _mapperContext.getCounter(counterName);
		}

		public Counter getCounter(String groupName, String counterName) {
//			RedisReader.LOG.info("Counter requested: ");
			return _mapperContext.getCounter(groupName, counterName);
		}

		@Override
		public void progress() {
			_mapperContext.progress();
		}

		public float getProgress() {
			return _mapperContext.getProgress();
		}

		@Override
		public void setStatus(String status) {
			_mapperContext.setStatus(status);
		}

		public OutputCommitter getOutputCommitter() {
			return _mapperContext.getOutputCommitter();
		}
	}

	@SuppressWarnings("unchecked")
	protected void setup(Context context) throws IOException, InterruptedException {
		super.setup(context);
		try {
			_reducer = (Reducer<KEYIN, VALUEIN, KEYOUT, VALUEOUT>) ReflectionUtils.newInstance(
					context.getReducerClass(), context.getConfiguration());
			_reduceContext = createReduceContext(context);
			ManualReducer.setup(_reducer, _reduceContext);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private org.apache.hadoop.mapreduce.Reducer.Context createReduceContext(
			org.apache.hadoop.mapreduce.Mapper.Context context) throws IOException,
			InterruptedException {
		final Mapper.Context mapperContext = context;
		final RecordWriter<KEYOUT, VALUEOUT> recordWriter = new RecordWriter<KEYOUT, VALUEOUT>() {

			@Override
			public void write(KEYOUT key, VALUEOUT value) throws IOException, InterruptedException {
				mapperContext.write(key, value);
			}

			@Override
			public void close(TaskAttemptContext context) throws IOException, InterruptedException {
				// nothing
			}
		};
		org.apache.hadoop.mapreduce.Reducer.Context rContext = new TiledReduceContext(mapperContext, _reducer,
				context.getConfiguration(), context.getTaskAttemptID(), new RawKeyValueIterator() {

					@Override
					public boolean next() throws IOException {
						// only this function is needed to make the outer
						// construction of the context pass
						return true;
					}

					@Override
					public DataInputBuffer getValue() throws IOException {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public Progress getProgress() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public DataInputBuffer getKey() throws IOException {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public void close() throws IOException {
						// TODO Auto-generated method stub

					}
				},
				// the RecordWriter is the only thing that gets triggered during
				// our execution because all we need is the write() call.
				null, null, recordWriter, null, null, null,
				// the below two arguments are not important but must be set in
				// order to avoid an NPE
				(Class<KEYIN>) Text.class, (Class<VALUEIN>) Text.class);
		return rContext;
	}

	/**
	 * Note, by design of the reducer interface the value list must fit into
	 * memory. Otherwise, even the original implementation can not perform the
	 * reduce.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void map(KEYIN key, VALUEIN value, Context context) throws IOException,
			InterruptedException {
		// convert the type of the values to be an Iterable
		final List lValues = (List) value;
//		RedisReader.LOG.info("Calling reducer: " + _reducer.getClass().getName() + " context: " + _reduceContext.getClass().getName());
		ManualReducer.reduce(_reducer, key, new Iterable() {
			@Override
			public Iterator iterator() {
				return lValues.iterator();
			}
		}, _reduceContext);
	}

	protected void cleanup(Context context) throws IOException, InterruptedException {
		super.cleanup(context);
		ManualReducer.cleanup(_reducer, _reduceContext);
	}
}
