package com.ramdoop.tiled;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.TiledTaskSelector;
import org.apache.hadoop.mapreduce.InputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.JobAccess;
import org.apache.hadoop.mapreduce.JobID;
import org.apache.hadoop.mapreduce.OutputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import redis.clients.jedis.Jedis;

import com.ramdoop.tiled.io.PartitionedRedisOutputFormat;
import com.ramdoop.tiled.io.RedisInputFormat;
import com.ramdoop.tiled.io.RedisWriter;
import com.ramdoop.tiled.io.TiledInputFormat;

public class TiledExecution {

	protected static String FIRST_PHASE_REDUCER = "ramdoop.tiled.first.phase.reduce";
	private static String MINI_JOBS_COMBINER_USE = "mini.jobs.use.combiner";

	private Job _config = null;
	private Set<Job> _submittedTileJobs = new HashSet<Job>();
	private String _jobFamily = null;

	/*
	 * We need to create this here, because if we always use the same then our
	 * jvm reuse concept inside Hadoop, will never spawn new VMs. However, we
	 * need it to do that when we run a completely new job such that the new
	 * jars get put onto the class path of the JVM responsible for executing the
	 * task.
	 */
	private int _rootID = Math.abs(new Random(System.currentTimeMillis()).nextInt());

	@SuppressWarnings("unused")
	private TiledExecution() {
		System.out.println("Generated root ID: " + _rootID);
	}

	protected TiledExecution(Job rootJob) {
		_jobFamily = rootJob.getJobName();
		setRootJob(rootJob);
	}

	public void setRootJob(Job rootJob) {
		_config = rootJob;
		prepareJob();
	}

	/**
	 * A single computation with two tiling phases.
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	protected void runRAMdoopComputation() throws IOException, InterruptedException {

		// prepare everything (RAM) for execution
		prepareExecution();

		// run a tiled computation on the job
		runTiledComputation();

		// run second tile round
		runFinalTiledReduceJobs();
	}

	protected void prepareExecution() {
		// sweep intermediate tile locations
		sweepIntermediateStorage();
	}

	private void prepareJob() {
		// create a job family
		_config.getConfiguration().set("mapred.job.reuse.jvm.familyID",
				constructJobFamily().toString());
	}

	private JobID constructJobFamily() {
		// beware: the JobID does not allow for "_" in the string identifier!
		String jobName = _jobFamily.replace("_", "-");
		return new JobID(jobName + "-tiled", _rootID);
	}

	private void sweepIntermediateStorage() {
		long start = System.currentTimeMillis();

		for (int i = 0; i < _config.getNumReduceTasks(); i++) {
			String location = RedisWriter.constructTileLocationReference(
					_config.getConfiguration(), i);
			Jedis redis = RedisWriter.retrieveRedisConnection(location);
			// redis.select(rootID);
			redis.flushAll();
			redis.disconnect();
		}
		System.out.println("Sweeping intermediate storage took "
				+ (System.currentTimeMillis() - start) + " ms.");
	}

	protected void runTiledComputation() throws IOException, InterruptedException {
		// create the configuration for the tiles
		Job tileConf = new Job(_config.getConfiguration());

		configureTilePhaseNature(tileConf);

		// special input format ...
		tileConf.getConfiguration().setClass("mapreduce.inputformat.class", TiledInputFormat.class,
				InputFormat.class);

		// ... but save the specified input format
		tileConf.getConfiguration().set(TiledInputFormat.ORIGINAL_INPUT_FORMAT,
				_config.getConfiguration().get(JobAccess.inputFormat()));

		// remove the output directory. otherwise it will be created already and
		// then the final tile job run will complain.
		tileConf.getConfiguration().set("mapred.output.dir", "");

		// tile the input of the job based on the original input format
		TilingManager tilingMngr = new TilingManager(_config.getConfiguration());

		System.out.println("Job registered. Executing ...");
		long start = System.currentTimeMillis();
		JobSubmitter submitter = new JobSubmitter();
		int tileCount = 1;
		for (Map.Entry<String, List<String>> entry : tilingMngr.tile(_config).entrySet()) {
			for (String tile : entry.getValue()) {
				startTileJob(submitter, Job.getInstance(tileConf.getConfiguration()), tileCount++,
						entry.getKey(), tile);
			}
		}
		submitter.awaitAllJobsDone();
		_submittedTileJobs.addAll(submitter.getSubmittedJobs());
		System.out.println("Tile phase execution took: " + (System.currentTimeMillis() - start)
				+ " ms.");
	}

	private void startTileJob(JobSubmitter submitter, Job tileJob, int tileCount,
			String tilePrimary, String tile) {
		tileJob.setJobName(tileJob.getJobName() + "-first-tile-" + tileCount);
		// specify a primary for the locality decision in the
		// TiledTaskSelector
		tileJob.getConfiguration().set(TiledTaskSelector.TILE_PRIMARY, tilePrimary);

		// the serialized tile references are passed as input
		tileJob.getConfiguration().set("mapred.input.dir", tile);

		// enable a slight slow start for these jobs to speed up loading
		tileJob.getConfiguration().set("mapred.reduce.slowstart.completed.maps", "0.8");

		submitter.executeJob(tile, tileJob);
	}

	private void configureTilePhaseNature(Job tileConf) {
		String combinerClass = _config.getConfiguration().get(FIRST_PHASE_REDUCER, null);
		if (combinerClass != null) {
			// adjust the number of reducers to available degree of parallelism on a single node
			int concurrencyLevel = tileConf.getConfiguration().getInt(
					"mapred.tasktracker.reduce.tasks.maximum", 2);
			tileConf.setNumReduceTasks(concurrencyLevel);
			
			// the combiner becomes also the reducer
			tileConf.getConfiguration().set(JobAccess.reduceClass(), combinerClass);
			
			// the combiner becomes optional (to be evaluated)
			if(!_config.getConfiguration().getBoolean(MINI_JOBS_COMBINER_USE, true))
				tileConf.getConfiguration().set(JobAccess.combineClass(), null);
			
			// special output format
			tileConf.getConfiguration().setInt(PartitionedRedisOutputFormat.PARTITION_COUNT,
					_config.getNumReduceTasks());
			tileConf.getConfiguration().setClass(JobAccess.outputFormat(),
					PartitionedRedisOutputFormat.class, OutputFormat.class);
			
			// the correctness of the result does not depend on this parameter anymore
			tileConf.setNumReduceTasks(concurrencyLevel);
			
			// the below accounts only when we split up a single job! therefore,
			// we use the more general PartitionedRedisOutputFormat.
			// tileConf.getConfiguration().setClass(JobAccess.outputFormat(),
			// RedisOutputFormat.class,
			// OutputFormat.class);
		} else {
			// TODO allow to specify a no-op reducer to nevertheless have the
			// ordering and partitioning done by the MR framework

			// don't specify a reducer ...
			tileConf.setNumReduceTasks(0);

			// ... but now we need an output format that does the partitioning
			tileConf.getConfiguration().setInt(PartitionedRedisOutputFormat.PARTITION_COUNT,
					_config.getNumReduceTasks());
			tileConf.getConfiguration().setClass(JobAccess.outputFormat(),
					PartitionedRedisOutputFormat.class, OutputFormat.class);
		}
	}

	/**
	 * We start a job for each intermediate tile. (Later we will make this more
	 * flexible.)
	 * 
	 * @throws IOException
	 */
	protected void runFinalTiledReduceJobs() throws IOException {

		// prepare the configuration for the tile jobs
		Job conf = new Job(_config.getConfiguration());

		try {
			// no map tasks needed. this is just about a final reduce step
			conf.setMapperClass(TiledReducer.class);

			// we leave the reducer class as specified. the TiledReducer will
			// pick it up and instantiate it.

			// set the redis input format
			conf.setInputFormatClass(RedisInputFormat.class);
		} catch (IllegalStateException e1) {
			e1.printStackTrace();
			throw new RuntimeException("Should never happen!", e1);
		}

		// reducing is done in the mapper
		conf.setNumReduceTasks(0);

		// generate the intermediate tile locations from the bucket ids of the
		// reducers
		List<String> intermediateTileLocations = new ArrayList<String>();
		for (int i = 0; i < _config.getNumReduceTasks(); i++) {
			String location = RedisWriter.constructTileLocationReference(
					_config.getConfiguration(), i);
			System.out.println("Constructed location: " + location);
			intermediateTileLocations.add(location);
		}

		// submit the jobs
		System.out.println("Job registered. Executing ...");
		long start = System.currentTimeMillis();
		JobSubmitter submitter = new JobSubmitter();
		int tileID = 0;
		// TODO it is not clear why we need tiling here because tasks are
		// independent anyways?! -> because then we can start consecutive jobs
		// for certain tiles earlier.
		LinkedHashMap<String, Job> jobs = new LinkedHashMap<>();
		for (String tileLocation : intermediateTileLocations) {
			Job tileJob = Job.getInstance(conf.getConfiguration());
			tileJob.setJobName(tileJob.getJobName() + "-second-tile-" + tileID);

			// TODO this should be just
			// tileJob.getConfiguration().set("mapred.input.dir", tile);

			// specify a primary for the locality decision in the
			// TiledTaskSelector
			URL url = new URL(tileLocation);
			tileJob.getConfiguration().set(TiledTaskSelector.TILE_PRIMARY, url.getHost());

			// this is for the redis input format to retrieve the splits for the
			// job
			tileJob.getConfiguration().set(RedisInputFormat.INTERMEDIATE_TILE_LOCATION,
					tileLocation);

			// can't have multiple jobs with the same output dir
			tileJob.getConfiguration().set("mapred.output.dir",
					tileJob.getConfiguration().get("mapred.output.dir") + "/tile-" + tileID++);

			// assert !jobs.containsKey(url.getHost());
			// // NOTE: Here we are actually swallowing jobs (when multiple tile
			// // locations is on the same box)! This is ok for now because it
			// // might only happen during testing.
			// jobs.put(url.getHost(), tileJob);
			jobs.put(tileLocation, tileJob);
		}

		// prepare atomic tile gang submission
		String sentinel = jobs.keySet().iterator().next();
		Job sentinelJob = jobs.remove(sentinel);
		Set<String> gang = jobs.keySet();
		String[] a = gang.toArray(new String[gang.size()]);
		sentinelJob.getConfiguration().set(TiledTaskSelector.SENTINEL, Arrays.deepToString(a));
		submitter.executeJob(sentinel, sentinelJob);

		for (Map.Entry<String, Job> preparedJob : jobs.entrySet()) {
			submitter.executeJob(preparedJob.getKey(), preparedJob.getValue());
		}

		submitter.awaitAllJobsDone();
		_submittedTileJobs.addAll(submitter.getSubmittedJobs());
		System.out.println("Final tile phase execution took: "
				+ (System.currentTimeMillis() - start) + " ms.");
	}

	/**
	 * In order to avoid the defaults here, we must ask directly if the number
	 * of reducers had been set explicitly.</p> First attempt: This is tricky
	 * because this is also the default in the default config files!</p> The
	 * solution:<br>
	 * Normally, the correct configuration is retrieved from specific
	 * configuration files. these can be added at the comamnd line via
	 * <ul>
	 * <li>"-conf <configuration file>"
	 * <li>"-D <property=value>".
	 * </ul>
	 * Hence, we always explicitly set the number of reduce tasks to -1. If the
	 * value differs when reaching this code then we know the desired final
	 * result granularity.
	 * 
	 * @throws IOException
	 */
	protected void finalizeJob() throws IOException {
		logJobIDs();
		returnJobIDs();
	}

	private void returnJobIDs() throws IOException {
		File f = new File(_config.getConfiguration().get("ramdoop.result.file"));
		FileWriter writer = new FileWriter(f);
		StringBuffer buffer = new StringBuffer();
		for (Job subJob : _submittedTileJobs) {
			// for deserialization use JobID.forName(jobid)
			buffer.append(subJob.getJobID());
			buffer.append("|");
		}
		buffer.deleteCharAt(buffer.length() - 1);
		writer.write(buffer.toString());
		writer.close();
	}

	private void logJobIDs() throws IOException {
		String logPath = _config.getConfiguration().get("ramdoop.job.log");
		if (logPath == null || logPath.isEmpty())
			return;
		String jobName = _config.getJobName();
		jobName = jobName.replaceAll(" ", "_");
		String jobLogName = jobName + "_jobSize"
				+ _config.getConfiguration().get("ramdoop.input.job.size");
		Path p = new Path(logPath, jobLogName);
		FSDataOutputStream outStream = p.getFileSystem(_config.getConfiguration()).create(p, true);
		PrintWriter writer = new PrintWriter(outStream);
		for (Job subJob : _submittedTileJobs) {
			writer.println(FileOutputFormat.getOutputPath(subJob));
		}
		writer.close();
		outStream.close();
	}
}
