/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.continuous;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;

import com.ramdoop.input.DataSource.DataTransfer;
import com.ramdoop.memmgnt.IRAMFSStatistics;
import com.ramdoop.tiled.JobSubmitter;
import com.ramdoop.tiled.JobSubmitter.IJobDoneListener;

// FIXME this component becomes interesting again when we bring continuous data in
/**
 * The memory manager is responsible of transferring the data from the Java heap
 * into HDFS (disk only for the time being).
 * 
 * @author sertel
 * 
 */
public class InputDataManager implements IJobDoneListener {

	private class SourceInfo {
		Path[] _fh = null;
		long _numBytesWritten = 0;
		Path _outputDir = null;
		String _outputDirectoryName = null;
		DataTransfer _transfer = null;
	}

	/**
	 * The HDFS (disk) directory for storing the inputs and outputs of the jobs.
	 */
	private String _hdfsJobRoot = null;
	private String _jobOutputRoot = null;

	/**
	 * The local directory to store the intermediate results of mappers.
	 */
//	private String _mapResultRoot = null; // is currently defined in the
//											// mapred-site.xml
//
//	private int _maxParallelJobs = 0;
	private long _memoryPerJob = 10000000; // default ~10MB

	private long _jobCounter = 0;

	private Map<String, SourceInfo> _sources = new HashMap<String, SourceInfo>();

	private JobSubmitter _jobSubmitter = null;

	private FileSystem _hdfs = null;
	private IRAMFSStatistics _stats = null;

	private AtomicLong _occupiedRAM = new AtomicLong(0);
	private ReentrantLock _lock = new ReentrantLock();
	private Condition _ramAvailableCondition = _lock.newCondition();

	public InputDataManager(JobSubmitter jobSubmitter, IRAMFSStatistics stats,
			String hdfsJobRoot, String mapResultRoot) {
		_jobSubmitter = jobSubmitter;
		_stats = stats;
		_hdfsJobRoot = hdfsJobRoot;
		_jobOutputRoot = _hdfsJobRoot;
		_jobSubmitter.addListener(this);
	}

//	public void setMaxParallelJobs(int maxParallelJobs) {
//		_maxParallelJobs = maxParallelJobs;
//	}

	public void setJobInputSize(long size) {
		_memoryPerJob = size;
	}

	/**
	 * Initialize the internal file structure.<br>
	 * 
	 * @throws IOException
	 */
	public void initialize() throws IOException {
		// FIXME Instead intializing this one its own it show receive the
		// current configuration because it might have parameters that got
		// configured via the command line!
		Configuration conf = new Configuration();
//		conf.addResource(new Path("file://" + RAMdoopClusterNode.HADOOP_HOME
//				+ "/conf/core-site.xml"));
//		conf.addResource(new Path("file://" + RAMdoopClusterNode.HADOOP_HOME
//				+ "/conf/hdfs-site.xml"));
//		conf.addResource(new Path("file://" + RAMdoopClusterNode.HADOOP_HOME
//				+ "/conf/mapred-site.xml"));
		_hdfs = FileSystem.get(conf);
	}

	/**
	 * 
	 * @return
	 */
	public String getNewSourceID(DataTransfer transfer) {
		String sourceID = "src-ID-" + Math.abs(new Random().nextInt());
		SourceInfo info = new SourceInfo();
		info._transfer = transfer;
		_sources.put(sourceID, info);
		return sourceID;
	}

	/**
	 * Stores a chunk of data. This call will block if there is no free space
	 * available
	 * 
	 * @param data
	 */
	// TODO implement blocking when no more space is available! -> Hadoop blocks
	// already by itself. This can be specified in the configuration.
	public void store(String sourceID, Object data) throws IOException {
		SourceInfo info = _sources.get(sourceID);
		if (info._numBytesWritten == 0) {
			createNewFile(info, sourceID);
		}

		info._numBytesWritten += info._transfer.write(data);

		if (info._numBytesWritten >= _memoryPerJob) {
			System.out.println("Bytes gathered: " + info._numBytesWritten);
			info._transfer.close();
			submitNewJob(sourceID, info);
			info._numBytesWritten = 0;
		}
	}

	private void submitNewJob(String sourceID, SourceInfo info)
			throws IOException {
		waitForAvailableRAM();
//		String fsName = _hdfs.getConf().get("fs.default.name");
//		_jobSubmitter.notifyReadyJob(sourceID, info._fh, new Path(fsName
//				+ info._outputDirectoryName));
		_jobSubmitter.notifyReadyJob(sourceID, info._fh, new Path(info._outputDirectoryName));
	}

	public void flush(String sourceID) throws IOException {
		SourceInfo info = _sources.get(sourceID);
		info._transfer.close();
		if (info._numBytesWritten > 0)
			submitNewJob(sourceID, info);
	}

	private void waitForAvailableRAM() {
		// FIXME insert correct path to directory
		if (_occupiedRAM.get() + _memoryPerJob > _stats.getSize(null)) {
			_lock.lock();
			try {
				while (_occupiedRAM.get() + _memoryPerJob > _stats
						.getSize(null)) {
					_ramAvailableCondition.awaitUninterruptibly();
				}
			} finally {
				_lock.unlock();
			}
		}
		_occupiedRAM.addAndGet(_memoryPerJob);
	}

	@Override
	public void done(Job job, Exception error) {
		long before = _occupiedRAM.getAndAdd(-_memoryPerJob);
		// FIXME insert correct path for directory
		if (before > _stats.getSize(null) - _memoryPerJob) {
			_lock.lock();
			try {
				_ramAvailableCondition.signalAll();
			} finally {
				_lock.unlock();
			}
		}
	}

	/**
	 * Creates a new input file for a job and specifies the according output
	 * directory.
	 */
	private void createNewFile(SourceInfo info, String sourceID)
			throws IOException {
		long batchJobCounter = _jobCounter++;
		String inputFileName = _hdfsJobRoot + "/inputs/source-" + sourceID
				+ "/batch-job-" + batchJobCounter;
		info._outputDirectoryName = getContinuousJobOutputDir(sourceID)
				+ "/batch-job-" + batchJobCounter;

		info._fh = info._transfer.recycle(_hdfs, inputFileName);
		info._numBytesWritten = 0;

		// sweep the output dir
		info._outputDir = new Path(info._outputDirectoryName);
		if (_hdfs.exists(info._outputDir))
			_hdfs.delete(info._outputDir, true);

		// don't create the output directory, hadoop will do that for you!
		// _hdfs.mkdirs(info._outputDir);
		System.out.println("New input file set: " + Arrays.toString(info._fh));
		System.out.println("New output directory: " + info._outputDir);
	}

	protected Path getContinuousJobOutputDir(String sourceID) {
		return new Path(_jobOutputRoot + "/outputs/source-" + sourceID);
	}

	public void setJobOutputDirectory(String jobOutputDir) {
		_jobOutputRoot = jobOutputDir;
	}

}
