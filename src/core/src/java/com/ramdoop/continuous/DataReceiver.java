/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.continuous;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import com.ramdoop.input.DataSource;

//FIXME this component becomes interesting again when we bring continuous data in
/**
 * The purpose of this component is to:<br>
 * <ol>
 * <li>drain the input sources given to it,
 * <li>put the drained data into the ram fs,
 * <li>notify the job submitter when there is a new job ready.
 * </ol>
 * 
 * @author sertel
 * 
 */
public class DataReceiver implements Runnable {

	private class Source {
		DataSource _input = null;
		String _id = null;
	}

	private AtomicBoolean _done = new AtomicBoolean(false);
	private List<Source> _sources = new ArrayList<Source>();
	private Map<String, DataSource> _mSources = new HashMap<String, DataSource>();
	
	// FIXME rather than giving this to the memory manager, the receiver should
	// be given a path where it is supposed to store the input. this can be
	// either a location on local disk or in the hdfs.

	// Another thought would be actually to have a data receiver and memory
	// manager on every cluster node. that way, job submission could be
	// performed from every node inside the cluster! Mappers would then be
	// located directly on this one cluster node preferably. This happens
	// already automatically in map/reduce!
	// Problem: How do tasks get spread across the cluster? -> Data is always
	// loaded to the HDFS! Mappers are then assigned accordingly. We do not
	// stear this distribution. Hence we should always store received data into
	// the HDFS (this might be real disk-based). Only intermediate results are
	// stored in RAMfs.

	// Maybe we can actually support both flavors! -> Always store in HDFS!
	private InputDataManager _memoryManager = null;

	protected DataReceiver(InputDataManager memoryManager) {
		_memoryManager = memoryManager;
	}

	/**
	 * Currently data sources must be registered before starting the receiver
	 * but changing this to enable concurrent addition of data source can be
	 * trivially added later.
	 * 
	 * @param source
	 */
	protected void addDataSource(DataSource source) {
		Source src = new Source();
		src._id = _memoryManager.getNewSourceID(source.createDataTransfer());
		source.assignId(src._id);
		src._input = source;
		_sources.add(src);
		DataSource s = _mSources.put(src._id, src._input);
		if (s != null) {
			_sources.remove(s);
			s.close();
		}
	}

	public void run() {
		System.out.println("Data Receiver started ...");
		while (!_done.get() && !_sources.isEmpty()) {
			// TODO build in a queue that blocks and waits for new sources
			Source source = _sources.remove(0);
			try {
				if (receiveData(source)) {
					_sources.add(source);
				} else {
					_mSources.remove(source._id);
					_sources.remove(source);
				}
			} catch (IOException e) {
				// input failed but we can still perform work on the other
				// inputs
				e.printStackTrace();
			}
		}
		System.out.println("Data Receiver done.");
		// TODO send a flush to the memory manager so it knows to wrap up pending input
	}

	private boolean receiveData(Source source) throws IOException {
		Object data = source._input.getData();
		try {
			if (data != null) {
				_memoryManager.store(source._id, data);
				return true;
			} else {
				_memoryManager.flush(source._id);
				System.out.println("Data Receiver received null.");
				source._input.close();
				return false;
			}
		} catch (IOException ioe) {
			// the memory manager has a problem
			throw new RuntimeException(ioe);
		}
	}

	public void stop() {
		_done.set(true);
	}

	public boolean exists(String sourceID) {
		return _mSources.containsKey(sourceID);
	}
}
