/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ramdoop.deploy;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.Shell;
import org.apache.hadoop.util.Shell.ShellCommandExecutor;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class RedisDeploy {

	private static class RedisTool implements Tool {

		private Configuration _conf = null;

		@Override
		public Configuration getConf() {
			return _conf;
		}

		@Override
		public void setConf(Configuration arg0) {
			_conf = arg0;
		}

		/**
		 * This only has the remaining arguments. <br>
		 * We have to start redis straight from here otherwise it does not get
		 * killed.
		 * 
		 * @param arg0
		 *            the reference to where redis is to be installed and
		 *            located.
		 */
		@Override
		public int run(String[] arg0) throws Exception {
			String redisRoot = arg0[0];
			// check whether redis is installed and if not then install it
			Shell.execCommand("./bin/ramdoop/redis/redis-install.sh", redisRoot);

			// start redis -> synchronous connection
			final ShellCommandExecutor executor = new ShellCommandExecutor(
					new String[] { redisRoot + "/bin/redis-server",
							redisRoot + "/conf/redis.conf" });
			// the Hadoop framework kills the JVM process so we have to make
			// sure it also shuts down the script that we started.
			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
				@Override
				public void run() {
					executor.getProcess().destroy();
				}
			}));
			executor.execute();
			String output = executor.getOutput();
			System.out.println(output);
			return 0;
		}
	}

	public static void main(String argv[]) throws Exception {
		ToolRunner.run(new RedisTool(), argv);
	}
}
