#!/bin/bash

mkdir hadoop
mv hadoop-1.2.1.tar.gz hadoop/
cd hadoop

# download tools and dependencies
git clone git@bitbucket.org:sertel/hadoopextensions.git
git clone git@bitbucket.org:sertel/ramdoop.git
git clone https://github.com/intel-hadoop/HiBench.git

# prepare hadoop distro
tar -xf hadoop-1.2.1.tar.gz
rm -rf hadoop-1.2.1/src
cp -rf hadoopextensions/src hadoop-1.2.1/src
cd hadoop-1.2.1/

# rebuild the necessary jars
ant jar -Dversion=1.2.1
cp build/hadoop-core-1.2.1.jar hadoop-core-1.2.1.jar

ant examples -Dversion=1.2.1
cp build/hadoop-examples-1.2.1.jar hadoop-examples-1.2.1.jar

cd src/contrib/fairscheduler
ant jar -Dversion=1.2.1
cd ../../../
cp build/contrib/fairscheduler/hadoop-fairscheduler-*.jar lib/hadoop-fairscheduler-1.2.1.jar

# deploy RAMdoop
cd ../ramdoop
ant deploy -DHADOOP_INSTALL_ROOT=../hadoop-1.2.1

echo "Next steps:"
echo "-> Deploy the config files: ant deploy-hadoop-config -DHADOOP_INSTALL_ROOT=hadoop/hadoop-1.2.1 -Dhadoop.setup=single|cluster"
echo "-> Adjust the configuration files depending on your setup (JAVA_HOME, data directory etc.)"
echo "-> Export HADOOP_HOME to allow HiBench to run properly"
echo "-> After deployment of the configuration, alter hadoop-1.2.1/bin/ramdoop/redis/start-redis.sh to point to hadoop/redis"
echo "-> Finally don't forget to format your namenode! (bin/hadoop namenode -format)"