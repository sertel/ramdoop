#!/bin/sh

HADOOP_ROOT=$1
SANDBOX=$2

$HADOOP_ROOT/bin/stop-all.sh
rm -rf $HADOOP_ROOT/logs/*
rm -rf $SANDBOX/test/local/*
rm -rf $SANDBOX/hdfs/*
$HADOOP_ROOT/bin/hadoop namenode -format
$HADOOP_ROOT/bin/start-all.sh
