#!/bin/bash

cd /Users/sebastianertel/sandbox/sandbox_hadoop/experiments/hadoop

python ../../RAMdoop_ws/RAMdoop/src/python/experiments/combined_subjob.py wc_run_50GB/spec_on/hibench.report wc_run_50GB/spec_on/subjob_logs/ wc_run_50GB/spec_off/hibench.report wc_run_50GB/spec_off/subjob_logs/
python ../../RAMdoop_ws/RAMdoop/src/python/experiments/combined_map_reduce.py wc_run_50GB/spec_on/hibench.report wc_run_50GB/spec_on/subjob_logs/ wc_run_50GB/spec_off/hibench.report wc_run_50GB/spec_off/subjob_logs/
python ../../RAMdoop_ws/RAMdoop/src/python/experiments/output_records.py wc_run_50GB/spec_off/subjob_logs/
python ../../RAMdoop_ws/RAMdoop/src/python/experiments/dir_monitor.py ram_monitoring_sort_new/ram_monitoring_orig.out ram_monitoring_sort_new/ram_monitoring_1.out ram_monitoring_sort_new/ram_monitoring_2.out ram_monitoring_sort_new/ram_monitoring_3.out ram_monitoring_wordcount_new/ram_monitoring_orig.out ram_monitoring_wordcount_new/ram_monitoring_1.out ram_monitoring_wordcount_new/ram_monitoring_2.out ram_monitoring_wordcount_new/ram_monitoring_3.out
python ../../RAMdoop_ws/RAMdoop/src/python/experiments/reducer_selection.py reducer_count.report reducer_count_2.report reducer_count_8.report
python ../../RAMdoop_ws/RAMdoop/src/python/experiments/fast_sort.py fasrt_sort.report
python ../../RAMdoop_ws/RAMdoop/src/python/experiments/job_progress_monitor.py progress_monitoring/wc_400/ramdoop_logs/ progress_monitoring/sort_n_400/ramdoop_logs/
python ../../RAMdoop_ws/RAMdoop/src/python/experiments/ram_based_runs.py ram_based_run/wc_no_ram_orig.report ram_based_run/wc_ram_orig.report ram_based_run/sort_no_ram_orig.report ram_based_run/sort_ram_orig.report