#!/bin/sh

HADOOP_HOME=$1
LOG=$2

echo $HADOOP_HOME
echo $LOG

JOB_LOG_DIR=$LOG"_sub_job_logs" 
echo $JOB_LOG_DIR
mkdir -p "$JOB_LOG_DIR"

count=1
while read JOB_OUTPUT_DIR
do
	echo "Getting summary for job: $JOB_OUTPUT_DIR"
	# get the log of the job from hadoop
	$HADOOP_HOME/bin/hadoop job -history all $JOB_OUTPUT_DIR > $JOB_LOG_DIR/job_$count.log 
	
	let count++
		
done < "$LOG"
