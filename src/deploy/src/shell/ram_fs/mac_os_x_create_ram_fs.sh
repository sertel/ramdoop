#!/bin/bash

#
# This script is taken from
# http://spargelkohl.livejournal.com/61558.html
#

# The volume name will be based on the string "RAMFS" and the number
# of days since the epoch.
#DAY=$(($(date +%s)/86400))
#VOLNAME="RAMFS-${DAY}"
VOLNAME=$@

# The size of the RAMFS disk (in 512K blocks)
NUMBLOCKS=256000

# Use hdid to create the raw RAMDISK
myrdev=`hdid -nomount ram://${NUMBLOCKS}`

# Use diskutil to partition the RAMDISK and create an HFS+ filesystem
# on the only partition.  (Noisy output)
diskutil partitionDisk $(basename ${myrdev}) 1 HFS+ "${VOLNAME}" "100%"

# Use hdiutil to mount (attach) the partition on the RAMDISK
mydev=`echo $myrdev | sed -e 's#/rdisk#/disk#g;'`
hdiutil attach $mydev
echo My device is $mydev

# Make the mounted filesytem private.  (Any other ACLs to munge?)
# (Can we get the mount point some reliable way?)
mountpoint=/Volumes/${VOLNAME}
chmod 0700 ${mountpoint}

echo "run \"hdiutil detach ${mydev}\" when done or use Finder to \"eject\" ${VOLNAME}"
