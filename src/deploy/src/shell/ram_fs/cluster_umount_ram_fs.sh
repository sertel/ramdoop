#!/bin/bash

for N in $@
do
	echo "Running:"
	echo "ssh $N 'mkdir -p /opt/sebastian/ramdoop/sandbox/storage/ram && /opt/sebastian/hadoop/hadoop-1.1.1/lib/ramdoop/linux_delete_ram_fs.sh /opt/sebastian/ramdoop/sandbox/storage/ram'"
	ssh $N 'mkdir -p /opt/sebastian/ramdoop/sandbox/storage/ram && /opt/sebastian/hadoop/hadoop-1.1.1/lib/ramdoop/linux_delete_ram_fs.sh /opt/sebastian/ramdoop/sandbox/storage/ram && mount'
done 