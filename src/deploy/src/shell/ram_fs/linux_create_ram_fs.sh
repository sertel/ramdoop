#!/bin/bash

PATH_TO_RAM_DIR=$@

#sudo mount -t ramfs ramfs $PATH_TO_RAM_DIR
sudo mount -t tmpfs -o size=400M,mode=0777 tmpfs $PATH_TO_RAM_DIR 
sudo chown sebastian $PATH_TO_RAM_DIR
sudo chgrp sebastian $PATH_TO_RAM_DIR

echo unmout with \'umount $PATH_TO_RAM_DIR\'