#!/bin/sh

START=$1
STOP=$2

for (( c=$START; c<=$STOP; c++ ))
do
	ssh stream$c 'rm -rf /opt/sebastian/hadoop/hadoop-1.2.1/logs/* && rm -rf /opt/sebastian/hadoop/sandbox/hdfs/data/* && rm -rf /opt/sebastian/hadoop/sandbox/hdfs/name/*'
done

/opt/sebastian/hadoop/hadoop-1.2.1/bin/hadoop namenode -format