import sys
import muddi

excludes =[]

def set_excludes(list):
    global excludes
    excludes = list

def configure(start, stop, cmd, upload=None, download=None):
    global excludes
    hosts = ["stream%i" % i for i in range(start,stop+1) if not i in excludes]
    # (host, cmd, term, kill)
    procs = [(h, cmd, None, None) for i, h in enumerate(hosts)]
    if upload:
        up = [(h, upload[0], upload[1]) for h in hosts]
    else:
        up = []

    if download:
        down = [(h, download[0], download[1]) for h in hosts]
    else:
        down = []
    return (procs, up, down)


def execute(run_id, procs, up, down, **kwargs):
    cfg = muddi.conf(procs, up, down, logdir="logs-" + run_id)
    if len(up) > 0:
        status = muddi.upload(cfg, stdout=None, stderr=None)
#        status == 0
    status = muddi.execute(cfg, stdout=sys.stdout, stderr=sys.stderr, **kwargs)
#    assert status == 0
    if len(down) > 0:
        status = muddi.download(cfg, stdout=None, stderr=None)
#        assert status == 0
    return cfg


def do(run_id, start, stop, cmd, download=None, upload=None, **kwargs):
    print("Executing the command:", cmd, "on servers", start, "-", stop)
    procs, up, down = configure(start, stop, cmd, upload=upload, download=download)
    cfg = execute(run_id, procs, up, down, **kwargs)
    print("Done.\n")
