import sys
import os

# put the parent dir on the path
p = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')
sys.path.insert(0, p)

from subprocess import call, STDOUT

from muddi_cluster_support import do

RAMdoop_HOME = "/opt/sebastian/ramdoop/project/ramdoop"
Hadoop_HOME = "/opt/sebastian/hadoop"
Hadoop_DIST = "hadoop-1.1.1" 

if __name__ == '__main__':
    start = int(sys.argv[1])
    stop = int(sys.argv[2])

    # prepare RAMdoop
    call(["ant", "deploy", "-DHADOOP_INSTALL_ROOT=" + Hadoop_HOME + "/" + Hadoop_DIST], cwd=RAMdoop_HOME, stderr=STDOUT)
    call(["ant", "deploy-hadoop-config", "-DHADOOP_INSTALL_ROOT=" + Hadoop_HOME + "/" + Hadoop_DIST, "-Dhadoop.setup=cluster"], cwd=RAMdoop_HOME, stderr=STDOUT)
    # make sure the scripts are executable
    call(["find", Hadoop_HOME + "/" + Hadoop_DIST + "/lib/ramdoop", "-name", "*.sh", "-print0", "|", "xargs", "-0", "-I", "{}", "chmod", "u+x", "{}"])

    # cleanup
    do("full-cleanup", start, stop, "rm -rf " + Hadoop_HOME)
    
    # create the hadoop dir
    do("create-hadoop-dir", start, stop, "mkdir -p " + Hadoop_HOME)

    # create a hadoop distro from the current installation
    call(["tar", "-czf", Hadoop_HOME + "/hadoop-dist.tar.gz", Hadoop_DIST], cwd=Hadoop_HOME, stderr=STDOUT)

    # distribute Hadoop
    cmds = ["cd " + Hadoop_HOME, "tar xf hadoop-dist.tar.gz"]
    do("distribute-hadoop", start, stop, " && ".join(cmds), upload=(Hadoop_HOME + "/hadoop-dist.tar.gz", Hadoop_HOME))    
