import matplotlib.pyplot as plt
import numpy as np
import matplotlib

import sys
import os
import re
import math

p = os.path.join(os.path.dirname(os.path.realpath(__file__)), '.')
sys.path.insert(0, p)

import formatting
import tasks
import log_parser

if __name__ == '__main__':
    
    formatting.perform_pure_formatting()
    tasks.perform_additional_formatting()
    matplotlib.rcParams['figure.subplot.bottom'] = 0.35
    matplotlib.rcParams['figure.subplot.top'] = 0.96
    matplotlib.rcParams['figure.subplot.right'] = 0.96
    matplotlib.rcParams['figure.subplot.left'] = 0.19

    rows = []
    runs = []
    for i in range(1,len(sys.argv),2): 
        report = sys.argv[i]
        sub_dir_logs = sys.argv[i+1]
        rows.append(log_parser.read_hibench_data(report))
        run = tasks.sort_runs(log_parser.read_hadoop_history(sub_dir_logs))
        runs.append(run)

    ax = tasks.prepare_chart()
    rects = []    
    ind = np.arange(len(rows[0]))
    colors = [0.4,0.8]
    for color, row, run in zip(colors, rows, runs):
        data = [sum(r['map-task-count']) for r in run]
        rects.append(tasks.plot_chart(ind+0.35, ax, data, color))
        ind = ind+0.35
    tasks.finalize_chart(ax, rows[0], runs[0], rects[0], ind)
    ax.legend(rects, ("spec: on", "spec: off"), ncol=2)
    plt.savefig("combined_map.pdf")
    plt.show()
