'''
 A script to plot the task counts for the different jobs.
'''

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib
import numpy as np

import sys
import os
import re
import math

p = os.path.join(os.path.dirname(os.path.realpath(__file__)), '.')
sys.path.insert(0, p)

import log_parser
import formatting

def plot_bar_chart(runs):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('Sub Job Input Size (MB)')
    ax.set_ylabel('Job I/O (MB)')
    ax.set_xlim([-0.5, len(runs)])
    ax.set_yscale('log')
    ax.set_ylim([math.pow(10,1),math.pow(10,7)])

    ind = np.arange(len(runs))

    runs = sorted(runs, key=lambda run:run['defined-size'])
    runs = [runs[-1]] + runs[:-1]
    
    xlabels = [int(math.ceil(max(run['input-size']))) for run in runs]
    
    data = [sum(run["input-size"]) for run in runs]
    rects0 = ax.bar(ind, data, 0.15, color=plt.get_cmap('gray')(0.01), align='center')

    data = [sum(run["map-bytes-read"]) / math.pow(2, 20) for run in runs]
    rects1 = ax.bar(0.15 + ind, data, 0.15, color=plt.get_cmap('gray')(0.2), align='center')

    data = [sum(run["map-bytes-written"]) / math.pow(2, 20) for run in runs]
    rects2 = ax.bar(0.3 + ind, data, 0.15, color=plt.get_cmap('gray')(0.4), align='center')

    data = [sum(run["map-output-bytes"]) / math.pow(2, 20) for run in runs]
    rects3 = ax.bar(0.45 + ind, data, 0.15, color=plt.get_cmap('gray')(0.6), align='center')
    
    data = [sum(run["map-output-materialized-bytes"]) / math.pow(2, 20) for run in runs]
    rects4 = ax.bar(0.6 + ind, data, 0.15, color=plt.get_cmap('gray')(0.8), align='center')

    xaxis = ax.get_xaxis()
    plt.xticks(ind, xlabels, horizontalalignment="center", rotation= -45)
    plt.setp(ax.get_xticklines(), visible=False)
    
    ax.legend( (rects0[0], rects1[0], rects2[0], rects3[0], rects4[0]), ('HDFS\_BYTES\_READ', 'FILE\_BYTES\_READ', 'FILE\_BYTES\_WRITTEN', 'Map output bytes', 'Map output materialized bytes'), ncol=3)

def perform_additional_formatting():
    matplotlib.rcParams['figure.subplot.bottom'] = 0.25

if __name__ == '__main__':
    
    sub_dir_logs = sys.argv[1]
    runs = log_parser.read_hadoop_history(sub_dir_logs)
    formatting.perform_pure_formatting()
    perform_additional_formatting()
    plot_bar_chart(runs)
    plt.show()
