import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib
import numpy as np

import sys
import os
import re
import math

p = os.path.join(os.path.dirname(os.path.realpath(__file__)), '.')
sys.path.insert(0, p)

import log_parser
import formatting

job_log_file_pattern = re.compile("job_[0-9]+_([0-9]+)\.log")
def parse_sub_job_logs(job_dir):
    sub_job_data = []
    for root, dirs, filenames in os.walk(job_dir):
        for file in filenames:
            data = None
            with open(os.path.join(root, file), 'r') as f:
                import json
                data = json.load(f)
            head, tail = os.path.split(file)
            match = job_log_file_pattern.match(tail)
            assert match, root
            sub_job_data.append({'job-id': int(match.group(1)), 'data':data})
    return sub_job_data

def plot_chart(ax_map, ax_reduce, data, start, degree):
    x = [ (long(d['time'])-start)/1000 for d in data]
    map = [float(d['map']) for d in data]
    reduce = [float(d['reduce']) for d in data]
    ax_map.plot(x,map,color=plt.get_cmap('winter')(degree))
    ax_reduce.plot(x,reduce,color=plt.get_cmap('winter')(degree))

def plot_errorbar(ax, jobs):
    y = []
    start_map = []
    stop_map = []
    start_reduce = []
    stop_reduce = []
    start = jobs[0]['data'][0]['time']
    for job in jobs:
        job_start = None
        map_done = None
        reduce_start = None
        for d in job['data']:
            map = float(d['map'])
            reduce = float(d['reduce'])
            if map > 0.00001 and not job_start:
                job_start = (long(d['time'])-start)/1000
            if map > 0.99 and not map_done:
                map_done = (long(d['time'])-start)/1000
            if reduce > 0.00001 and not reduce_start:
                reduce_start = (long(d['time'])-start)/1000
        assert job_start
        assert map_done, job['job-id']
        assert reduce_start
        job_end = (long(job['data'][-1]['time'])-start)/1000
        start_map.append(job_start)
        stop_map.append(map_done)
        start_reduce.append(reduce_start)
        stop_reduce.append(job_end)
        y.append(job['job-id']-jobs[0]['job-id']+1)
    e1 = ax.errorbar(start_map, y, xerr=([0 for d in start_map],[e-s for s,e in zip(start_map,stop_map)]), fmt='|',color=plt.get_cmap('gray')(0.3), label="eins")
    e2 = ax.errorbar(start_reduce, y, xerr=([0 for d in start_reduce],[e-s for s,e in zip(start_reduce,stop_reduce)]), fmt='|',color=plt.get_cmap('gray')(0.6), label="zwei")

#     ax.legend((e1[0],e2[0]), ("Map", "Reduce"), loc="upper left", numpoints=1)    
    return e1, e2

def plot_charts(data):
    fig = plt.figure(1)
    ax_map = fig.add_subplot(212)
    ax_map.set_xlabel('Time (s)')
    ax_map.set_ylabel('Map\n Progress', multialignment='center')    
    
    ax_reduce = fig.add_subplot(211)
    ax_reduce.set_xlabel('Time')
    ax_reduce.set_ylabel('Reduce\n Progress', multialignment='center')

    data = sorted(data, key=lambda d: d['job-id'])
    color = range(0, len(data))
    for c,d in zip(color,data):
        plot_chart(ax_map,ax_reduce, d['data'], long(data[0]['data'][0]['time']), c/float(len(data)))

    plt.setp(ax_reduce.get_xticklabels(), visible=False)
    ax_reduce.xaxis.get_label().set_visible(False)

    ax_map.get_yticklabels()[-1].set_visible(False)
    ax_reduce.get_yticklabels()[0].set_visible(False)

def prepare_plot(data, loc=111, prefix=''):
    fig = plt.figure(1)
    ax = fig.add_subplot(loc)
    ax.set_xlabel('Time (s)')
    ax.set_xlim([0,400])
    ax.set_ylabel(prefix + 'Mini Jobs', multialignment='center')    
    
    data = sorted(data, key=lambda d: d['data'][0]['time'])
    return ax, data


def perform_additional_formatting():
    matplotlib.rcParams['figure.figsize'][0] = matplotlib.rcParams['figure.figsize'][0]+1
    matplotlib.rcParams['figure.figsize'][1] = matplotlib.rcParams['figure.figsize'][1]+1
    matplotlib.rcParams['figure.subplot.bottom'] = 0.15
    matplotlib.rcParams['figure.subplot.top'] = 0.85
    matplotlib.rcParams['figure.subplot.right'] = 0.96
    matplotlib.rcParams['figure.subplot.left'] = 0.16
    matplotlib.rcParams['figure.subplot.hspace'] = 0.05
    matplotlib.rcParams['axes.labelsize'] = 10
    matplotlib.rcParams['font.size'] = 10
    matplotlib.rcParams['xtick.labelsize'] = 10
    matplotlib.rcParams['ytick.labelsize'] = 10
    matplotlib.rcParams['legend.fontsize'] = 10
    
if __name__ == '__main__':
    
    job_dir_wc = sys.argv[1]
    sub_job_data_wc = parse_sub_job_logs(job_dir_wc)
    job_dir_sort = sys.argv[2]
    sub_job_data_sort = parse_sub_job_logs(job_dir_sort)
    formatting.perform_pure_formatting()
    perform_additional_formatting()
    
    ax_wc, sub_job_data_wc = prepare_plot(sub_job_data_wc,loc=211, prefix='Word Count\n')
    e1,e2 = plot_errorbar(ax_wc, sub_job_data_wc)
    
    ax_sort, sub_job_data_sort = prepare_plot(sub_job_data_sort,loc=212, prefix='Sort\n')
    plot_errorbar(ax_sort, sub_job_data_sort)
    
    plt.setp(ax_wc.get_xticklabels(), visible=False)
    ax_wc.xaxis.get_label().set_visible(False)
    ax_sort.get_yticklabels()[-1].set_visible(False)
    ax_wc.get_yticklabels()[0].set_visible(False)

    # get handles
    handles, labels = ax_wc.get_legend_handles_labels()
    handles = [(h[0],h[2]) for h in handles]
    ax_wc.legend(handles, ("Map", "Reduce"), bbox_to_anchor=(0.25, 1.02, 1., .102), loc=3, ncol=2, borderaxespad=0.5)
    
    plt.savefig("job_progress.pdf")
    plt.show()
