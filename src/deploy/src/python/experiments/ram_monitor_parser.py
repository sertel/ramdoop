import sys
import os
import re
import math

'''
Get the data by executing:
watch 'echo Timestamp `date +%s` | tee -a ../ram_monitoring.out && du -b --max-depth=1 . | tee -a ../ram_monitoring.out'
'''

timestamp_pattern = re.compile("Timestamp:* ([0-9]+)")
file_pattern = re.compile("([0-9]+)[\s\t]*([\.a-zA-Z0-9_\/]+)")
def parse_log(log):
    data = []
    with open(log, 'r') as f:
#         count = 0
        for line in f:
#             print line
            match = timestamp_pattern.match(line)
            if match:
                record = {}
                record['Timestamp'] = long(match.group(1))
                record['dirs'] = []
                data.append(record)
            else:
                match = file_pattern.match(line)
                assert match, f
                dir = {}
                dir['size'] = long(match.group(1))
                dir['name'] = match.group(2)
#                 print dir['name']
#                 if count == 4: assert false
#                 count += 1
                if dir['name'] != ".": data[-1]['dirs'].append(dir)
    return data

filename_pattern = re.compile("jobSize([0-9]+)_ram_monitor.out")
def parse_logs(log_dir):
    runs = []
    for root, dirs, filenames in os.walk(log_dir):
        for f in filenames:
            run = {}
            run['data'] = parse_log(os.path.join(root, f))
            match = filename_pattern.match(f)
            assert match
            run['defined-size'] = long(match.group(1))
            runs.append(run)
    return runs
