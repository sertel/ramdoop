import sys
import os
import re
import math
import pprint

'''
Example line:
(FileSystemCounters)(FileSystemCounters)[(FILE_BYTES_READ)(FILE_BYTES_READ)(566408)][(HDFS_BYTES_READ)(HDFS_BYTES_READ)(67110307)][(FILE_BYTES_WRITTEN)(FILE_BYTES_WRITTEN)(613417)]}{(org\.apache\.hadoop\.mapreduce\.lib\.input\.FileInputFormat$Counter)(File Input Format Counters )[(BYTES_READ)(Bytes Read)(67109894)]}{(org\.apache\.hadoop\.mapred\.Task$Counter)(Map-Reduce Framework)[(MAP_OUTPUT_MATERIALIZED_BYTES)(Map output materialized bytes)(16661)][(COMBINE_OUTPUT_RECORDS)(Combine output records)(22000)][(MAP_INPUT_RECORDS)(Map input records)(99929)][(PHYSICAL_MEMORY_BYTES)(Physical memory \\(bytes\\) snapshot)(240615424)][(SPILLED_RECORDS)(Spilled Records)(35000)][(MAP_OUTPUT_BYTES)(Map output bytes)(79665731)][(CPU_MILLISECONDS)(CPU time spent \\(ms\\))(24280)][(COMMITTED_HEAP_BYTES)(Total committed heap usage \\(bytes\\))(224395264)][(VIRTUAL_MEMORY_BYTES)(Virtual memory \\(bytes\\) snapshot)(653889536)][(COMBINE_INPUT_RECORDS)(Combine input records)(5456648)][(MAP_OUTPUT_RECORDS)(Map output records)(5435648)][(SPLIT_RAW_BYTES)(SPLIT_RAW_BYTES)(121)]"
'''
map_counter_pattern = re.compile(".*\(FILE_BYTES_READ\)\(([0-9]+)\).*\(HDFS_BYTES_READ\)\(([0-9]+)\).*\(FILE_BYTES_WRITTEN\)\(([0-9]+)\).*\(Map output materialized bytes\)\(([0-9]+)\).*\(Map output bytes\)\(([0-9]+)\).*")
def parse_map_counters(counter_line):
    counters = {}
    match = map_counter_pattern.match(counter_line)
    assert match
    counters['FILE_BYTES_READ'] = long(match.group(1))
    counters['HDFS_BYTES_READ'] = long(match.group(2))
    counters['FILE_BYTES_WRITTEN'] = long(match.group(3))
    counters['map-materialized-output-bytes'] = long(match.group(4))
    counters['map-output-bytes'] = long(match.group(5))
    return counters

'''
Example line:
{(org\.apache\.hadoop\.mapreduce\.lib\.output\.FileOutputFormat$Counter)(File Output Format Counters )[(BYTES_WRITTEN)(Bytes Written)(22960)]}{(FileSystemCounters)(FileSystemCounters)[(FILE_BYTES_READ)(FILE_BYTES_READ)(4896576)][(FILE_BYTES_WRITTEN)(FILE_BYTES_WRITTEN)(4926891)][(HDFS_BYTES_WRITTEN)(HDFS_BYTES_WRITTEN)(22960)]}{(org\.apache\.hadoop\.mapred\.Task$Counter)(Map-Reduce Framework)[(REDUCE_INPUT_GROUPS)(Reduce input groups)(1000)][(COMBINE_OUTPUT_RECORDS)(Combine output records)(0)][(REDUCE_SHUFFLE_BYTES)(Reduce shuffle bytes)(4898334)][(PHYSICAL_MEMORY_BYTES)(Physical memory \\(bytes\\) snapshot)(127369216)][(REDUCE_OUTPUT_RECORDS)(Reduce output records)(1000)][(SPILLED_RECORDS)(Spilled Records)(294000)][(CPU_MILLISECONDS)(CPU time spent \\(ms\\))(9050)][(COMMITTED_HEAP_BYTES)(Total committed heap usage \\(bytes\\))(116654080)][(VIRTUAL_MEMORY_BYTES)(Virtual memory \\(bytes\\) snapshot)(666918912)][(COMBINE_INPUT_RECORDS)(Combine input records)(0)][(REDUCE_INPUT_RECORDS)(Reduce input records)(294000)]}
'''
reduce_counter_pattern = re.compile(".*\(Bytes Written\)\(([0-9]+)\).*\(FILE_BYTES_READ\)\(([0-9]+)\).*\(FILE_BYTES_WRITTEN\)\(([0-9]+)\).*\(HDFS_BYTES_WRITTEN\)\(([0-9]+)\).*")
def parse_reduce_counters(counter_line):
    counters = {}
    match = reduce_counter_pattern.match(counter_line)
    assert match
    counters['BYTES_WRITTEN'] = long(match.group(1))
    counters['FILE_BYTES_READ'] = long(match.group(2))
    counters['FILE_BYTES_WRITTEN'] = long(match.group(3))
    counters['HDFS_BYTES_WRITTEN'] = long(match.group(4))
    return counters

'''
Example lines:
MapAttempt TASK_TYPE="MAP" TASKID="task_201305131019_0625_m_000003" TASK_ATTEMPT_ID="attempt_201305131019_0625_m_000003_0" START_TIME="1368438483626" TRACKER_NAME="tracker_stream42:localhost/127\.0\.0\.1:33848" HTTP_PORT="50060" .
MapAttempt TASK_TYPE="MAP" TASKID="task_201305131019_0625_m_000289" TASK_ATTEMPT_ID="attempt_201305131019_0625_m_000289_0" TASK_STATUS="SUCCESS" FINISH_TIME="1368438629636" HOSTNAME="/default-rack/stream40" STATE_STRING="" COUNTERS="{(FileSystemCounters)(FileSystemCounters)[(FILE_BYTES_READ)(FILE_BYTES_READ)(566408)][(HDFS_BYTES_READ)(HDFS_BYTES_READ)(67110307)][(FILE_BYTES_WRITTEN)(FILE_BYTES_WRITTEN)(613417)]}{(org\.apache\.hadoop\.mapreduce\.lib\.input\.FileInputFormat$Counter)(File Input Format Counters )[(BYTES_READ)(Bytes Read)(67109894)]}{(org\.apache\.hadoop\.mapred\.Task$Counter)(Map-Reduce Framework)[(MAP_OUTPUT_MATERIALIZED_BYTES)(Map output materialized bytes)(16661)][(COMBINE_OUTPUT_RECORDS)(Combine output records)(22000)][(MAP_INPUT_RECORDS)(Map input records)(99929)][(PHYSICAL_MEMORY_BYTES)(Physical memory \\(bytes\\) snapshot)(240615424)][(SPILLED_RECORDS)(Spilled Records)(35000)][(MAP_OUTPUT_BYTES)(Map output bytes)(79665731)][(CPU_MILLISECONDS)(CPU time spent \\(ms\\))(24280)][(COMMITTED_HEAP_BYTES)(Total committed heap usage \\(bytes\\))(224395264)][(VIRTUAL_MEMORY_BYTES)(Virtual memory \\(bytes\\) snapshot)(653889536)][(COMBINE_INPUT_RECORDS)(Combine input records)(5456648)][(MAP_OUTPUT_RECORDS)(Map output records)(5435648)][(SPLIT_RAW_BYTES)(SPLIT_RAW_BYTES)(121)]}" .
'''
map_task_start_pattern = re.compile("MapAttempt TASK_TYPE=MAP TASKID=([a-z0-9_]+).*START_TIME=([0-9]+).*")
map_task_finish_pattern = re.compile("MapAttempt TASK_TYPE=MAP TASKID=([a-z0-9_]+).*TASK_STATUS=([A-Z]+).*FINISH_TIME=([0-9]+).*COUNTERS=\{(.*)\}.*")
def parse_map_task(line):
    line = line.replace("\"", "")
    match = map_task_start_pattern.match(line)
    if match:
        task = {}
        task['type'] = "MAP"
        task['id'] = match.group(1)
        task['start-time'] = long(match.group(2))
#         print "Found task start", task['id']
        return task
    else:
        match = map_task_finish_pattern.match(line)
        if match:
            task = {}
            task['id'] = match.group(1)
            task['status'] = match.group(2)
            task['finish-time'] = long(match.group(3))
            task['counters'] = parse_map_counters(match.group(4))
#             print "Found task stop", task['id']
            return task
        else:
            return None

'''
Example lines:
ReduceAttempt TASK_TYPE="REDUCE" TASKID="task_201305131019_0625_r_000000" TASK_ATTEMPT_ID="attempt_201305131019_0625_r_000000_0" START_TIME="1368438502910" TRACKER_NAME="tracker_stream39:localhost/127\.0\.0\.1:59263" HTTP_PORT="50060" .
ReduceAttempt TASK_TYPE="REDUCE" TASKID="task_201305131019_0625_r_000000" TASK_ATTEMPT_ID="attempt_201305131019_0625_r_000000_0" TASK_STATUS="SUCCESS" SHUFFLE_FINISHED="1368438635480" SORT_FINISHED="1368438638513" FINISH_TIME="1368438639944" HOSTNAME="/default-rack/stream39" STATE_STRING="reduce > reduce" COUNTERS="{(org\.apache\.hadoop\.mapreduce\.lib\.output\.FileOutputFormat$Counter)(File Output Format Counters )[(BYTES_WRITTEN)(Bytes Written)(22960)]}{(FileSystemCounters)(FileSystemCounters)[(FILE_BYTES_READ)(FILE_BYTES_READ)(4896576)][(FILE_BYTES_WRITTEN)(FILE_BYTES_WRITTEN)(4926891)][(HDFS_BYTES_WRITTEN)(HDFS_BYTES_WRITTEN)(22960)]}{(org\.apache\.hadoop\.mapred\.Task$Counter)(Map-Reduce Framework)[(REDUCE_INPUT_GROUPS)(Reduce input groups)(1000)][(COMBINE_OUTPUT_RECORDS)(Combine output records)(0)][(REDUCE_SHUFFLE_BYTES)(Reduce shuffle bytes)(4898334)][(PHYSICAL_MEMORY_BYTES)(Physical memory \\(bytes\\) snapshot)(127369216)][(REDUCE_OUTPUT_RECORDS)(Reduce output records)(1000)][(SPILLED_RECORDS)(Spilled Records)(294000)][(CPU_MILLISECONDS)(CPU time spent \\(ms\\))(9050)][(COMMITTED_HEAP_BYTES)(Total committed heap usage \\(bytes\\))(116654080)][(VIRTUAL_MEMORY_BYTES)(Virtual memory \\(bytes\\) snapshot)(666918912)][(COMBINE_INPUT_RECORDS)(Combine input records)(0)][(REDUCE_INPUT_RECORDS)(Reduce input records)(294000)]}" .
'''
reduce_task_start_pattern = re.compile("ReduceAttempt TASK_TYPE=REDUCE TASKID=([a-z0-9_]+).*START_TIME=([0-9]+).*")
reduce_task_finish_pattern = re.compile("ReduceAttempt TASK_TYPE=REDUCE TASKID=([a-z0-9_]+).*TASK_STATUS=([A-Z]+).*FINISH_TIME=([0-9]+).*COUNTERS=\{(.*)\}.*")
def parse_reduce_task(line):
    line = line.replace("\"", "")
    match = reduce_task_start_pattern.match(line)
    if match:
        task = {}
        task['type'] = "REDUCE"
        task['id'] = match.group(1)
        task['start-time'] = long(match.group(2))
        return task
    else:
        match = reduce_task_finish_pattern.match(line)
        if match:
            task = {}
            task['id'] = match.group(1)
            task['status'] = match.group(2)
            task['finish-time'] = long(match.group(3))
            task['counters'] = parse_reduce_counters(match.group(4))
            return task
        else:
            return None

def parse_line(line):
    task = parse_map_task(line)
    if not task:
        task = parse_reduce_task(line)
    return task
    
def read_job_log(job_log):
    tasks = {}
    with open(job_log, 'r') as f:
        for line in f:
            task = parse_line(line)
            if task:
                if task['id'] in tasks:
#                     print "Before:", tasks[task['id']].keys()
                    tasks[task['id']].update(task)
#                     print "After:", tasks[task['id']].keys()
                else:
                    tasks[task['id']] = task 
    return tasks

if __name__ == '__main__':
    
#      print parse_line("MapAttempt TASK_TYPE=\"MAP\" TASKID=\"task_201305131019_0625_m_000003\" TASK_ATTEMPT_ID=\"attempt_201305131019_0625_m_000003_0\" START_TIME=\"1368438483626\" TRACKER_NAME=\"tracker_stream42:localhost/127\.0\.0\.1:33848\" HTTP_PORT=\"50060\" .")
#      print parse_line("MapAttempt TASK_TYPE=\"MAP\" TASKID=\"task_201305131019_0625_m_000289\" TASK_ATTEMPT_ID=\"attempt_201305131019_0625_m_000289_0\" TASK_STATUS=\"SUCCESS\" FINISH_TIME=\"1368438629636\" HOSTNAME=\"/default-rack/stream40\" STATE_STRING=\"\" COUNTERS=\"{(FileSystemCounters)(FileSystemCounters)[(FILE_BYTES_READ)(FILE_BYTES_READ)(566408)][(HDFS_BYTES_READ)(HDFS_BYTES_READ)(67110307)][(FILE_BYTES_WRITTEN)(FILE_BYTES_WRITTEN)(613417)]}{(org\.apache\.hadoop\.mapreduce\.lib\.input\.FileInputFormat$Counter)(File Input Format Counters )[(BYTES_READ)(Bytes Read)(67109894)]}{(org\.apache\.hadoop\.mapred\.Task$Counter)(Map-Reduce Framework)[(MAP_OUTPUT_MATERIALIZED_BYTES)(Map output materialized bytes)(16661)][(COMBINE_OUTPUT_RECORDS)(Combine output records)(22000)][(MAP_INPUT_RECORDS)(Map input records)(99929)][(PHYSICAL_MEMORY_BYTES)(Physical memory \\(bytes\\) snapshot)(240615424)][(SPILLED_RECORDS)(Spilled Records)(35000)][(MAP_OUTPUT_BYTES)(Map output bytes)(79665731)][(CPU_MILLISECONDS)(CPU time spent \\(ms\\))(24280)][(COMMITTED_HEAP_BYTES)(Total committed heap usage \\(bytes\\))(224395264)][(VIRTUAL_MEMORY_BYTES)(Virtual memory \\(bytes\\) snapshot)(653889536)][(COMBINE_INPUT_RECORDS)(Combine input records)(5456648)][(MAP_OUTPUT_RECORDS)(Map output records)(5435648)][(SPLIT_RAW_BYTES)(SPLIT_RAW_BYTES)(121)]}\" .")
    print parse_line("MapAttempt TASK_TYPE=\"MAP\" TASKID=\"task_201305131019_0625_m_000003\" TASK_ATTEMPT_ID=\"attempt_201305131019_0625_m_000003_0\" TASK_STATUS=\"SUCCESS\" FINISH_TIME=\"1368438502152\" HOSTNAME=\"/default-rack/stream42\" STATE_STRING=\"\" COUNTERS=\"{(FileSystemCounters)(FileSystemCounters)[(FILE_BYTES_READ)(FILE_BYTES_READ)(599724)][(HDFS_BYTES_READ)(HDFS_BYTES_READ)(71095554)][(FILE_BYTES_WRITTEN)(FILE_BYTES_WRITTEN)(646731)]}{(org\.apache\.hadoop\.mapreduce\.lib\.input\.FileInputFormat$Counter)(File Input Format Counters )[(BYTES_READ)(Bytes Read)(71095433)]}{(org\.apache\.hadoop\.mapred\.Task$Counter)(Map-Reduce Framework)[(MAP_OUTPUT_MATERIALIZED_BYTES)(Map output materialized bytes)(16661)][(COMBINE_OUTPUT_RECORDS)(Combine output records)(23000)][(MAP_INPUT_RECORDS)(Map input records)(105741)][(PHYSICAL_MEMORY_BYTES)(Physical memory \\(bytes\\) snapshot)(216686592)][(SPILLED_RECORDS)(Spilled Records)(37000)][(MAP_OUTPUT_BYTES)(Map output bytes)(84394522)][(CPU_MILLISECONDS)(CPU time spent \\(ms\\))(24450)][(COMMITTED_HEAP_BYTES)(Total committed heap usage \\(bytes\\))(224722944)][(VIRTUAL_MEMORY_BYTES)(Virtual memory \\(bytes\\) snapshot)(649703424)][(COMBINE_INPUT_RECORDS)(Combine input records)(5781362)][(MAP_OUTPUT_RECORDS)(Map output records)(5759362)][(SPLIT_RAW_BYTES)(SPLIT_RAW_BYTES)(121)]}\" .")
#     print parse_reduce_task("ReduceAttempt TASK_TYPE=\"REDUCE\" TASKID=\"task_201305131019_0625_r_000000\" TASK_ATTEMPT_ID=\"attempt_201305131019_0625_r_000000_0\" START_TIME=\"1368438502910\" TRACKER_NAME=\"tracker_stream39:localhost/127\.0\.0\.1:59263\" HTTP_PORT=\"50060\" .")
#     print parse_reduce_task("ReduceAttempt TASK_TYPE=\"REDUCE\" TASKID=\"task_201305131019_0625_r_000000\" TASK_ATTEMPT_ID=\"attempt_201305131019_0625_r_000000_0\" TASK_STATUS=\"SUCCESS\" SHUFFLE_FINISHED=\"1368438635480\" SORT_FINISHED=\"1368438638513\" FINISH_TIME=\"1368438639944\" HOSTNAME=\"/default-rack/stream39\" STATE_STRING=\"reduce > reduce\" COUNTERS=\"{(org\.apache\.hadoop\.mapreduce\.lib\.output\.FileOutputFormat$Counter)(File Output Format Counters )[(BYTES_WRITTEN)(Bytes Written)(22960)]}{(FileSystemCounters)(FileSystemCounters)[(FILE_BYTES_READ)(FILE_BYTES_READ)(4896576)][(FILE_BYTES_WRITTEN)(FILE_BYTES_WRITTEN)(4926891)][(HDFS_BYTES_WRITTEN)(HDFS_BYTES_WRITTEN)(22960)]}{(org\.apache\.hadoop\.mapred\.Task$Counter)(Map-Reduce Framework)[(REDUCE_INPUT_GROUPS)(Reduce input groups)(1000)][(COMBINE_OUTPUT_RECORDS)(Combine output records)(0)][(REDUCE_SHUFFLE_BYTES)(Reduce shuffle bytes)(4898334)][(PHYSICAL_MEMORY_BYTES)(Physical memory \\(bytes\\) snapshot)(127369216)][(REDUCE_OUTPUT_RECORDS)(Reduce output records)(1000)][(SPILLED_RECORDS)(Spilled Records)(294000)][(CPU_MILLISECONDS)(CPU time spent \\(ms\\))(9050)][(COMMITTED_HEAP_BYTES)(Total committed heap usage \\(bytes\\))(116654080)][(VIRTUAL_MEMORY_BYTES)(Virtual memory \\(bytes\\) snapshot)(666918912)][(COMBINE_INPUT_RECORDS)(Combine input records)(0)][(REDUCE_INPUT_RECORDS)(Reduce input records)(294000)]}\" .")
#     tasks = read_job_log(sys.argv[1])
    