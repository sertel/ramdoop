'''
 A script to plot the execution time for different jobs.
'''

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib
import numpy as np

import sys
import os
import re
import math

p = os.path.join(os.path.dirname(os.path.realpath(__file__)), '.')
sys.path.insert(0, p)

import log_parser
import formatting

def prepare_chart(loc=111):
    fig = plt.figure(1)
    ax = fig.add_subplot(loc)
    ax.set_xlabel('(Mini) Job Input Size')
    ax.set_ylabel(log_parser.columns[4])
    ax.set_ylim([100,350])
    return ax

def plot_chart(ind, ax, rows, color):    
#     ind = np.arange(len(rows))
    data = [float(row[log_parser.columns[4]]) for row in rows]
    rects = ax.bar(ind, data, 0.35, color=plt.get_cmap('gray')(color), align='center')
    return rects
    
def finalize_chart(ax, rows, rects, ind):
#     ind = np.arange(len(rows))
    ax.set_xlim([-0.5, len(rows)-0.5])
#     xlabels = (int(52600125378/math.pow(2,20)), int(200000000/math.pow(2,20)), int(400000000/math.pow(2,20)), int(600000000/math.pow(2,20)), int(1000000000/math.pow(2,20)), int(1400000000/math.pow(2,20)), int(1800000000/math.pow(2,20)))
    xlabels = ("50 GB", "322 MB", "483 MB", "644 MB", "966 MB", "1.4 GB", "1.7 GB")
    #     print xlabels
    
    xaxis = ax.get_xaxis()
    plt.xticks(ind, xlabels, horizontalalignment="center", rotation=-45)
    plt.setp(ax.get_xticklines(), visible=False)

def plot_bar_chart(rows):
    ax = prepare_chart()
    ind = np.arange(len(rows))
    rects = plot_chart(ind, ax, rows, 0.8)
    finalize_chart(ax, rows, rects, ind)

def perform_additional_formatting():
    matplotlib.rcParams['figure.figsize'][1] = matplotlib.rcParams['figure.figsize'][1]+0.5
    matplotlib.rcParams['figure.subplot.bottom'] = 0.35
    matplotlib.rcParams['figure.subplot.top'] = 0.96
    matplotlib.rcParams['figure.subplot.right'] = 0.96
    matplotlib.rcParams['figure.subplot.left'] = 0.17

if __name__ == '__main__':
    
    report = sys.argv[1]
    rows = log_parser.read_hibench_data(report)
    formatting.perform_pure_formatting()
    perform_additional_formatting()
    plot_bar_chart(rows)
    plt.savefig("fast_sort.pdf")
    plt.show()
    
