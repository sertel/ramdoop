import matplotlib.pyplot as plt
import numpy as np
import matplotlib

import sys
import os
import re
import math

p = os.path.join(os.path.dirname(os.path.realpath(__file__)), '.')
sys.path.insert(0, p)

import formatting
import runtime
import log_parser

if __name__ == '__main__':
        
    formatting.perform_pure_formatting()
    runtime.perform_additional_formatting()
    matplotlib.rcParams['figure.figsize'][1] = matplotlib.rcParams['figure.figsize'][1]+0.6
    matplotlib.rcParams['figure.subplot.bottom'] = 0.44
    matplotlib.rcParams['figure.subplot.top'] = 0.98
    matplotlib.rcParams['figure.subplot.right'] = 0.97
    matplotlib.rcParams['figure.subplot.left'] = 0.17

    rows = []
    runs = []
    for i in range(1,len(sys.argv),2): 
        report = sys.argv[i]
        sub_dir_logs = sys.argv[i+1]
        rows.append(log_parser.read_hibench_data(report))
        runs.append(log_parser.read_hadoop_history(sub_dir_logs))

    run = sorted(runs[0], key=lambda run:run['defined-size'])
    run = [run[-1]] + [run[-1]] + run[:-1]

    # filter some uninteresting values
    del run[3]
    del rows[0][3]
    del rows[1][3]
    del run[-1]
    del rows[0][-1]
    del rows[1][-1]
    
    ax = runtime.prepare_chart()
    rects = []    
    ind = np.arange(len(rows[0]))
    colors = [0.4,0.8]
    for color, row in zip(colors, rows):
        rects.append(runtime.plot_chart(ind+0.35, ax, row, color))
        ind = ind+0.35
    runtime.finalize_chart(ax, rows[0], run, rects[0],ind)
#     ax.legend(rects, ("spec: on", "spec: off"), ncol=2)
    for tick in ax.get_yticklabels()[1::2]:
        tick.set_visible(False)
    
    plt.savefig("combined_runtime.pdf")
    plt.show()
