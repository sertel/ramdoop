import matplotlib.pyplot as plt
import numpy as np
import matplotlib

import sys
import os
import re
import math

p = os.path.join(os.path.dirname(os.path.realpath(__file__)), '.')
sys.path.insert(0, p)

import formatting
import tasks
import runtime
import log_parser

if __name__ == '__main__':
    
    formatting.perform_pure_formatting()
    tasks.perform_additional_formatting()
    matplotlib.rcParams['figure.figsize'][0] = (matplotlib.rcParams['figure.figsize'][0]*2)-0.4
    matplotlib.rcParams['figure.figsize'][1] = matplotlib.rcParams['figure.figsize'][1]+0.6
    matplotlib.rcParams['figure.subplot.bottom'] = 0.44
    matplotlib.rcParams['figure.subplot.top'] = 0.98
    matplotlib.rcParams['figure.subplot.right'] = 0.98
    matplotlib.rcParams['figure.subplot.left'] = 0.10
    matplotlib.rcParams['figure.subplot.wspace'] = 0.19

    rows = []
    runs = []
    for i in range(1,len(sys.argv),2): 
        report = sys.argv[i]
        sub_dir_logs = sys.argv[i+1]
        rows.append(log_parser.read_hibench_data(report))
        run = tasks.sort_runs(log_parser.read_hadoop_history(sub_dir_logs))
        runs.append(run)
        
#     #runtime
#     ax = runtime.prepare_chart(loc=131)
#     rects = []    
#     ind = np.arange(len(rows[0]))
#     colors = [0.4,0.8]
#     for color, row, run in zip(colors, rows, runs):
#         rects.append(runtime.plot_chart(ind+0.35, ax, row, color))
#         ind = ind+0.35
#     runtime.finalize_chart(ax, rows[0], runs[0], rects[0], ind)

    # filter some uninteresting values
    for row in rows:
        del row[2]
        del row[-1]
        del row[0]

    for run in runs:
        del run[2]
        del run[-1]

    #map
    ax = tasks.prepare_chart(loc=121)
    rects = []    
    ind = np.arange(len(rows[0]))
    colors = [0.4,0.8]
    for color, row, run in zip(colors, rows, runs):
        data = [sum(r['map-task-count']) for r in run]
        rects.append(tasks.plot_chart(ind+0.35, ax, data, color))
        ind = ind+0.35
    tasks.finalize_chart(ax, rows[0], runs[0], rects[0], ind)
#     ax.legend(rects, ("spec: on", "spec: off"), ncol=2)
#     plt.show()
    for tick in ax.get_yticklabels()[0::2]:
        tick.set_visible(False)

    #reduce
    ax = tasks.prepare_chart(0, 200, 122)
    rects = []    
    ind = np.arange(len(rows[0]))
    colors = [0.4,0.8]
    for color, row, run in zip(colors, rows, runs):
        data = [sum(r['reduce-task-count']) for r in run]
        rects.append(tasks.plot_chart(ind+0.35, ax, data, color))
        ind = ind+0.35
    tasks.finalize_chart(ax, rows[0], runs[0], rects[0], ind)
    for tick in ax.get_yticklabels()[0::2]:
        tick.set_visible(False)

#     plt.setp(ax.get_yticklabels(), visible=False)
    ax.yaxis.get_label().set_visible(False)
    
#     print dir(ax.yaxis.get_label())
    ax.legend(rects, ("speculation: on", "speculation: off"), bbox_to_anchor=(-1.28, -0.85, 1., .102), loc=3, ncol=2, borderaxespad=0.5)
        
    plt.savefig("combined.pdf")
    plt.show()
