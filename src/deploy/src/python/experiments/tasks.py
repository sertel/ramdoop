'''
 A script to plot the task counts for the different jobs.
'''

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib
import numpy as np

import sys
import os
import re
import math

p = os.path.join(os.path.dirname(os.path.realpath(__file__)), '.')
sys.path.insert(0, p)

import log_parser
import formatting

def prepare_chart(ymin=600,ymax=1400, loc=111):
    fig = plt.figure(1)
    ax = fig.add_subplot(loc)
    ax.set_xlabel('(Mini) Job Input Size')
    ax.set_ylabel('Task Count')
    ax.set_ylim([ymin,ymax])
    return ax

def sort_runs(runs):
    runs = sorted(runs, key=lambda run:run['defined-size'])
    runs = [runs[-1]] + runs[:-1]    
    return runs
    
def plot_chart(ind, ax, data, color): 
    return ax.bar(ind, data, 0.35, color=plt.get_cmap('gray')(color), align='center')

def finalize_chart(ax, rows, runs, rects, ind):
#     xlabels = [int(long(rows[0][log_parser.columns[3]]) / math.pow(2, 20))] + [int(math.ceil(max(run['input-size']))) for run in runs]
#     xlabels = [int(math.ceil(max(run['input-size']))) for run in runs]
    xlabels = []
    for run in runs:
        mbytes = int(math.ceil(max(run['input-size'])))
        if mbytes > 1024:
            xlabels.append("%.1f" % (float(mbytes)/1024) + " GB")
        else:
            xlabels.append(str(mbytes) + " MB")
    xlabels[0] = "50 GB"

    ax.set_xlim([-0.5, len(rows)+0.35])
    xaxis = ax.get_xaxis()
    plt.xticks(ind, xlabels, horizontalalignment="center", rotation= -45)
    plt.setp(ax.get_xticklines(), visible=False)
    
def plot_bar_chart(rows, runs):
    ax = prepare_chart()
    ind = np.arange(len(rows))
    runs = sort_runs(runs)
    rects = []
    data = [sum(run['map-task-count']) for run in runs]
    rects.append(plot_chart(ind,ax,data,0.4))
    data = [sum(run['reduce-task-count']) for run in runs]
    rects.append(plot_chart(0.35+ind,ax,data,0.8))
    finalize_chart(ax,rows,runs,rects,ind)
    ax.legend( rects, ('Map', 'Reduce'))

def perform_additional_formatting():
    matplotlib.rcParams['figure.subplot.bottom'] = 0.25

if __name__ == '__main__':
    
#     print log_parser.parse_map_task_count("|Job Counters                  |Launched map tasks            |0         |0         |168")
    report = sys.argv[1]
    sub_dir_logs = sys.argv[2]
    rows = log_parser.read_hibench_data(report)
    runs = log_parser.read_hadoop_history(sub_dir_logs)
    formatting.perform_pure_formatting()
    perform_additional_formatting()
    plot_bar_chart(rows, runs)
    plt.show()
