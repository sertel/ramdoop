'''
 A script to plot the task counts for the different jobs.
'''

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib
import numpy as np

import sys
import os
import re
import math

p = os.path.join(os.path.dirname(os.path.realpath(__file__)), '.')
sys.path.insert(0, p)

import job_log_parser
import formatting

def plot_bar_chart(jobs):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('Compression Codec')
    ax.set_ylabel('Task I/O (KB)')
    ax.set_xlim([-0.5, len(jobs)])
    ax.set_yscale('log')
#     ax.set_ylim([0,300])

    ind = np.arange(len(jobs))

    xlabels = ["Disabled", "Default"]#, "GZip", "B2Zip"]
    assert len(xlabels) == len(jobs)
    
    # second task is representative, I think
    map_tasks = [sorted(filter(lambda task: task['type'] == 'MAP', tasks.values()), key=lambda task:task['start-time'])[5] for tasks in jobs]
    
    data = [map_task['counters']['HDFS_BYTES_READ']/ math.pow(2, 10) for map_task in map_tasks]
    print data
    rects0 = ax.bar(ind, data, 0.15, color=plt.get_cmap('gray')(0.01), align='center')

    data = [map_task['counters']['FILE_BYTES_READ']/ math.pow(2, 10) for map_task in map_tasks]
    print data
    rects1 = ax.bar(0.15 + ind, data, 0.15, color=plt.get_cmap('gray')(0.2), align='center')

    data = [map_task['counters']['FILE_BYTES_WRITTEN']/ math.pow(2, 10) for map_task in map_tasks]
    print data
    rects2 = ax.bar(0.3 + ind, data, 0.15, color=plt.get_cmap('gray')(0.4), align='center')

    data = [map_task['counters']['map-output-bytes']/ math.pow(2, 10) for map_task in map_tasks]
    print data
    rects3 = ax.bar(0.45 + ind, data, 0.15, color=plt.get_cmap('gray')(0.6), align='center')
    
    data = [map_task['counters']['map-materialized-output-bytes']/ math.pow(2, 10) for map_task in map_tasks]
    print data
    rects4 = ax.bar(0.6 + ind, data, 0.15, color=plt.get_cmap('gray')(0.8), align='center')

    xaxis = ax.get_xaxis()
    plt.xticks(0.3 + ind, xlabels, horizontalalignment="center")
    plt.setp(ax.get_xticklines(), visible=False)
    
    ax.legend( (rects0[0], rects1[0], rects2[0], rects3[0], rects4[0]), ('HDFS\_BYTES\_READ', 'FILE\_BYTES\_READ', 'FILE\_BYTES\_WRITTEN', 'Map output bytes', 'Map output materialized bytes'), ncol=3)

def perform_additional_formatting():
    matplotlib.rcParams['figure.subplot.bottom'] = 0.25


if __name__ == '__main__':
    
    jobs = []
    for codec_job_log in sys.argv[1:]:
        tasks = job_log_parser.read_job_log(codec_job_log)
        jobs.append(tasks)
    formatting.perform_pure_formatting()
#     perform_additional_formatting()
    plot_bar_chart(jobs)
    plt.show()
