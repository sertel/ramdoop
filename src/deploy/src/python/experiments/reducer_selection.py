import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib
import numpy as np

import sys
import os
import re
import math

p = os.path.join(os.path.dirname(os.path.realpath(__file__)), '.')
sys.path.insert(0, p)

import log_parser
import formatting

def plot_chart(ax, data, start, degree):
    x = [ long(d['time'])-start for d in data]
    map = [float(d['map']) for d in data]
    reduce = [float(d['reduce']) for d in data]
    ax.plot(x,map,color=plt.get_cmap('RdBu')(degree))
    ax.plot(x,reduce,color=plt.get_cmap('RdBu')(degree))

def plot_charts(runs):
    fig = plt.figure(1)
    ax = fig.add_subplot(111)
    ax.set_xlabel('\# of Reducers')
    ax.set_ylabel('Duration (s)')
#     ax.set_yticks(range(0,800,200))
    
    labels = []
    plots = []
    for d in runs:
        ydata = [f['Duration(s)'] for f in d]
        m = min(ydata)
        plots.append(ax.plot(range(10,101,10),[f['Duration(s)'] for f in d])[0])
        labels.append(long(d[0]['Input_data_size'])/math.pow(2,30))
#         plot_chart(ax, d['data'], long(data[0]['data'][0]['time']), c/float(len(data)))
#         plots.append(plot_chart(ax, data))
    return ax,plots

def perform_additional_formatting():
    matplotlib.rcParams['figure.figsize'][1] = matplotlib.rcParams['figure.figsize'][1]+0.5
    matplotlib.rcParams['figure.subplot.bottom'] = 0.35
    matplotlib.rcParams['figure.subplot.top'] = 0.96
    matplotlib.rcParams['figure.subplot.right'] = 0.96
    matplotlib.rcParams['figure.subplot.left'] = 0.17
    matplotlib.rcParams['axes.labelsize'] = 11
    matplotlib.rcParams['font.size'] = 11
    matplotlib.rcParams['xtick.labelsize'] = 11
    matplotlib.rcParams['ytick.labelsize'] = 11
    matplotlib.rcParams['legend.fontsize'] = 11

    
if __name__ == '__main__':
    
    runs = []
    for report in sys.argv[1:]:
        runs.append(log_parser.read_hibench_data(report))
    formatting.perform_pure_formatting()
    perform_additional_formatting()
    ax,plots = plot_charts(runs)
    formatting.setFigLinesBW(plt.figure(1))
    ax.legend(plots, ("50 GB", "25 GB", "5 GB"), bbox_to_anchor=(-0.1, -0.5, 1., .102), loc=3,
       ncol=3, borderaxespad=0.4)
    for tick in ax.get_yticklabels()[1::2]:
        tick.set_visible(False)
    plt.savefig("reducer_selection.pdf")
    plt.show()
