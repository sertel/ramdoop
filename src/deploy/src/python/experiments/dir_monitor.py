'''
 A script to plot the task counts for the different jobs.
'''

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
from matplotlib.pyplot import *

import sys
import os
import re
import math

p = os.path.join(os.path.dirname(os.path.realpath(__file__)), '.')
sys.path.insert(0, p)

import ram_monitor_parser
import formatting

def plot_setup(loc=111):
    fig = plt.figure(1)
    ax = fig.add_subplot(loc)
    ax.set_xlabel('Time (s)')
    ax.set_ylabel('Size (MB)')

    ax.set_xlim([0,270])
#     ax.set_yscale('log')
    return ax

def plot_chart(ax, data):

    x = [ d['Timestamp']-data[0]['Timestamp'] for d in data]
#     ax.set_xlim(x[0], x[-1])

#     labels = [d['name'].replace(".", "").replace("/", "").replace("_", "\_") for d in data[0]['dirs']]
#     plots = []
#     y = [0 for d in data]
    i = [d['name'] for d in data[0]['dirs']].index("./taskTracker")
    return ax.plot(x, [d['dirs'][i]['size']/math.pow(2,20) for d in data])[0]
#     for i in range(0,len(data[0]['dirs'])):
#         # this does not work because of the log scale!
# #         y = [p + d['dirs'][i]['size'] for p,d in zip(y,data)]
# #         y = [sum([e['size'] for e in d['dirs'][:i+1]]) for d in data]
# #         plots.append(ax.plot(x,y)[0])
#         plots.append(ax.plot(x, [d['dirs'][i]['size'] for d in data])[0])
#     ax.legend(plots, labels, ncol=2)

def perform_additional_formatting():
    matplotlib.rcParams['figure.figsize'][0] = matplotlib.rcParams['figure.figsize'][0]*2
    matplotlib.rcParams['figure.figsize'][1] = matplotlib.rcParams['figure.figsize'][1]+0.5
    matplotlib.rcParams['figure.subplot.bottom'] = 0.35
    matplotlib.rcParams['figure.subplot.top'] = 0.96
    matplotlib.rcParams['figure.subplot.right'] = 0.99
    matplotlib.rcParams['figure.subplot.left'] = 0.1


def plot_diagram(loc, files):
    ax = plot_setup(loc)
    loc += loc
    plots = []
    for log in files:
        _,logFile = os.path.split(log)
#         match = log_file_pattern.match(logFile)
#         assert match
#         labels.append(int(long(match.group(1))/math.pow(2,20)))
        data = ram_monitor_parser.parse_log(log)
        plots.append(plot_chart(ax, data))
    return ax,plots

log_file_pattern = re.compile("jobSize([0-9]+).*")
if __name__ == '__main__':
    
    formatting.perform_pure_formatting()
    perform_additional_formatting()
    ax1,_ = plot_diagram(121, sys.argv[1:5])
    ax,plots = plot_diagram(122, sys.argv[5:])
    ax.yaxis.get_label().set_visible(False)
    for tick in ax1.get_yticklabels()[1::2]:
        tick.set_visible(False)

    formatting.setFigLinesBW(plt.figure(1))
#     labels = ("50 GB", str(int(200000000/math.pow(2,20))) + " MB", str(int(1400000000/math.pow(2,20))) + " MB", str(int(2200000000/math.pow(2,20))) + " MB")
    labels = ("50 GB", "322 MB", "1.4 GB", "2.2 GB")
    ax.legend(plots, labels,bbox_to_anchor=(-1., -0.55, 1., .102), loc=3,
       ncol=4, borderaxespad=0.4)
    
    plt.savefig("dir_monitor.pdf")
    plt.show()
