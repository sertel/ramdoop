import matplotlib.pyplot as plt
import numpy as np
import matplotlib

import sys
import os
import re
import math

p = os.path.join(os.path.dirname(os.path.realpath(__file__)), '.')
sys.path.insert(0, p)

import formatting
import tasks
import runtime
import log_parser

def finalize_chart(ax, rows, rects, ind):
#     ind = np.arange(len(rows))
    ax.set_xlim([-0.5, len(rows)+0.35])
    
    xlabels = (5, 10, 15, 20)
    #     print xlabels
    
    xaxis = ax.get_xaxis()
    plt.xticks(ind, xlabels, horizontalalignment="center")
    plt.setp(ax.get_xticklines(), visible=False)

if __name__ == '__main__':
    
    formatting.perform_pure_formatting()
    tasks.perform_additional_formatting()
    matplotlib.rcParams['figure.figsize'][0] = (matplotlib.rcParams['figure.figsize'][0]*2)-0.4
    matplotlib.rcParams['figure.figsize'][1] = matplotlib.rcParams['figure.figsize'][1]+0.6
    matplotlib.rcParams['figure.subplot.bottom'] = 0.17
    matplotlib.rcParams['figure.subplot.top'] = 0.83
    matplotlib.rcParams['figure.subplot.right'] = 0.98
    matplotlib.rcParams['figure.subplot.left'] = 0.10
    matplotlib.rcParams['figure.subplot.wspace'] = 0.03

    rows_no_ram = []
    rows_ram = []
    for i in range(1,len(sys.argv),2): 
        report_no_ram = sys.argv[i]
        report_ram = sys.argv[i+1]
        rows_no_ram.append(log_parser.read_hibench_data(report_no_ram))
        rows_ram.append(log_parser.read_hibench_data(report_ram))

    # word count
    ax = runtime.prepare_chart(loc=121)
    ax.set_ylim([0,80])
    rects = []    
    ind = np.arange(len(rows_no_ram[0]))
    colors = [0.4,0.8]
    for color, row in zip(colors, (rows_no_ram[0], rows_ram[0])):
        rects.append(runtime.plot_chart(ind+0.35, ax, row, color))
        ind = ind+0.35
    finalize_chart(ax, rows_no_ram[0], rects[0], ind)
    ax.set_xlabel("Job Input Size (GB)")
    for tick in ax.get_yticklabels()[1::2]:
        tick.set_visible(False)

    #sort
    ax = runtime.prepare_chart(122)
    ax.set_ylim([0,80])
    rects = []
    ind = np.arange(len(rows_no_ram[1]))
    colors = [0.4,0.8]
    for color, row in zip(colors, (rows_no_ram[1], rows_ram[1])):
        rects.append(runtime.plot_chart(ind+0.35, ax, row, color))
        ind = ind+0.35
    finalize_chart(ax, rows_no_ram[1], rects[0], ind)
    plt.setp(ax.get_yticklabels(), visible=False)
    ax.yaxis.get_label().set_visible(False)
    ax.set_xlabel("Job Input Size (GB)")
    ax.legend(rects, ("disk based", "ram based"), bbox_to_anchor=(-0.5, 1.02, 1., .102), loc=3, ncol=2, borderaxespad=0.5)
        
    plt.savefig("ram_based_runs.pdf")
    plt.show()
