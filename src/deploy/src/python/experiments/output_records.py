'''
 A script to plot the task counts for the different jobs.
'''

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib
import numpy as np

import sys
import os
import re
import math

p = os.path.join(os.path.dirname(os.path.realpath(__file__)), '.')
sys.path.insert(0, p)

import log_parser
import formatting

def plot_bar_chart(runs):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('(Mini) Job Input Size')
    ax.set_ylabel('Record Count')
    ax.set_xlim([-0.5, len(runs)])
    ax.set_yscale('log')
    ax.set_ylim([math.pow(10,2),math.pow(10,9)])
    
    ind = np.arange(len(runs))

#     xlabels = [int(math.ceil(max(run['input-size']))) for run in runs]
    xlabels = []
    for run in runs:
        mbytes = int(math.ceil(max(run['input-size'])))
        if mbytes > 1024:
            xlabels.append("%.1f" % (float(mbytes)/1024) + " GB")
        else:
            xlabels.append(str(mbytes) + " MB")
    xlabels[0] = "50 GB"
    
    data = [sum(run['combine-output-records']) for run in runs]
    rects1 = ax.bar(ind, data, 0.35, color=plt.get_cmap('gray')(0.4), align='center')
    
    data = [sum(run['reduce-output-records']) for run in runs]
    rects2 = ax.bar(0.35 + ind, data, 0.35, color=plt.get_cmap('gray')(0.8), align='center')

    xaxis = ax.get_xaxis()
    plt.xticks(ind+0.35, xlabels, horizontalalignment="center", rotation= -45)
    plt.setp(ax.get_xticklines(), visible=False)
    
    ax.legend( (rects1[0], rects2[0]), ('Combine', 'Reduce'), bbox_to_anchor=(0, -0.85, 1., .102), loc=3,
       mode="expand", ncol=2, borderaxespad=0.5)
    
    return ax

def perform_additional_formatting():
    matplotlib.rcParams['figure.figsize'][1] = matplotlib.rcParams['figure.figsize'][1]+0.6
    matplotlib.rcParams['figure.subplot.bottom'] = 0.44
    matplotlib.rcParams['figure.subplot.top'] = 0.98
    matplotlib.rcParams['figure.subplot.right'] = 0.98
    matplotlib.rcParams['figure.subplot.left'] = 0.17
    matplotlib.rcParams['figure.subplot.wspace'] = 0.03


if __name__ == '__main__':
    
    sub_dir_logs = sys.argv[1]
    runs = log_parser.read_hadoop_history(sub_dir_logs)
    formatting.perform_pure_formatting()
    perform_additional_formatting()
    
    runs = sorted(runs, key=lambda run:run['defined-size'])
    runs = [runs[-1]] + runs[:-1]
    
    # filter some uninteresting values
    del runs[2]
    del runs[-1]

    ax = plot_bar_chart(runs)

    for tick in ax.get_yticklabels()[1::2]:
        tick.set_visible(False)

    plt.savefig("output_records.pdf")
    plt.show()
