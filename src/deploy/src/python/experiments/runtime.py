'''
 A script to plot the execution time for different jobs.
'''

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib
import numpy as np

import sys
import os
import re
import math

p = os.path.join(os.path.dirname(os.path.realpath(__file__)), '.')
sys.path.insert(0, p)

import log_parser
import formatting

def set_top_labels(ax, rects, top_labels):
    for rect, top_label in zip(rects, top_labels):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width(), 1.05 * height, '%d' % int(top_label),
                ha='center', va='bottom')

def prepare_chart(loc=111):
    fig = plt.figure(1)
    ax = fig.add_subplot(loc)
    ax.set_xlabel('(Mini) Job Input Size')
    ax.set_ylabel('Duration (s)')
    ax.set_ylim([250, 600])
    return ax

def plot_chart(ind, ax, rows, color):    
#     ind = np.arange(len(rows))
    data = [float(row[log_parser.columns[4]]) for row in rows]
    rects = ax.bar(ind, data, 0.35, color=plt.get_cmap('gray')(color), align='center')
    return rects
    
def finalize_chart(ax, rows, runs, rects, ind):
#     ind = np.arange(len(rows))
    ax.set_xlim([-0.5, len(rows)+0.35])
    
    xlabels = []
    for run in runs:
        mbytes = int(math.ceil(max(run['input-size'])))
        if mbytes > 1024:
            xlabels.append("%.1f" % (float(mbytes)/1024) + " GB")
        else:
            xlabels.append(str(mbytes) + " MB")
    xlabels[0] = "50 GB\n(JVM reuse)"
    xlabels[1] = "50 GB"
            
#     print runs[0].keys()
#     xlabels = [int(math.ceil(max(run['map-bytes-read'])))/math.pow(2,20) for run in runs]
    #     print xlabels
    
    xaxis = ax.get_xaxis()
    plt.xticks(ind, xlabels, horizontalalignment="center", rotation=-45)
    plt.setp(ax.get_xticklines(), visible=False)

    set_top_labels(ax, rects, [run['job-count'] for run in runs])

    ax.text((rects[1].get_x() + rects[1].get_width() / 2.)+0.4, 1.3 * rects[1].get_height(), 'Sub Job \nCounts:',
            ha='right', va='bottom')

def plot_bar_chart(rows, runs):
    ax = prepare_chart(rows)
    ind = np.arange(len(rows))
    rects = plot_chart(ind, ax, rows, 0.8)
    runs = sorted(runs, key=lambda run:run['defined-size'])
    runs = [runs[-1]] + runs[:-1]
    finalize_chart(ax, rows, runs, rects, ind)

def perform_additional_formatting():
    matplotlib.rcParams['font.size'] = 12
    matplotlib.rcParams['figure.subplot.bottom'] = 0.25

if __name__ == '__main__':
    
    report = sys.argv[1]
    sub_dir_logs = sys.argv[2]
    rows = log_parser.read_hibench_data(report)
    runs = log_parser.read_hadoop_history(sub_dir_logs)
    formatting.perform_pure_formatting()
    perform_additional_formatting()
    plot_bar_chart(rows, runs)
    plt.show()
    
