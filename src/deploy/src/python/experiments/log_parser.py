import sys
import os
import re
import math

# columns = ["Type", "Date", "Time", "Input_data_size", "Duration(s)", "Throughput(bytes/s)", "Throughput/node"]
columns = None
# sample line: WORDCOUNT    2013-05-08 18:07:13 320002924            85.000               3764740              3764740
pattern = re.compile("([A-Z]+)[\s\t]*([0-9\-]+)[\s\t]*([0-9\:]+)[\s\t]*([0-9]+)[\s\t]*([0-9\.]+)[\s\t]*([0-9]+)[\s\t]*([0-9]+)")
def read_hibench_data(report):
    global columns
    columns = None
    rows = []
    with open(report, 'r') as f:
        for line in f:
            global columns
            if not columns:
                columns = line.replace("\n", "").split(" ")
                columns = [column.replace(" ", "") for column in columns if len(column) > 0]
            else:
                match = pattern.match(line)
                assert match, line
                row = {}
                for index, column in enumerate(columns):
                    row[column] = match.group(index + 1)
                rows.append(row)
    return rows

job_log_pattern = re.compile("job_([0-9]+).log")
log_file_pattern = re.compile("word_count_jobSize([0-9]+)_sub_job_logs")
def read_hadoop_history(log_dir):
    runs = []
    for root, dirs, filenames in os.walk(log_dir):
        if len(filenames) > 0:
            run = {}
            run['job-count'] = len(filenames)
            run['job-ids'] = []
            map_count = 0
            reduce_count = 0
            duration = 0
            for f in filenames:
                with open(os.path.join(root, f), 'r') as file:
                    for line in file:
                        parse_history_log(line, run)
                assert map_count < run['map-task-count'], file
                assert reduce_count < run['reduce-task-count'], file
                assert duration < len(run['duration']), file
                duration = len(run['duration'])
                map_count = sum(run['map-task-count'])
                reduce_count = sum(run['reduce-task-count'])
                job_log_match = job_log_pattern.match(f)
                assert job_log_match
                run['job-ids'].append(int(job_log_match.group(1)))
            assert run['job-count'] == len(run['duration']), root + " " + str(run['job-count']) + " " + str(len(run['duration']))
            head, tail = os.path.split(root)
            match = log_file_pattern.match(tail)
            assert match, root
            run['defined-size'] = long(match.group(1))
            runs.append(run)
    return runs

'''
The pattern is: 
Finished At: 13-May-2013 10:55:18 (4mins, 11sec)
Finished At: 13-May-2013 10:51:51 (49sec)
Finished At: 14-May-2013 13:17:17 (1hrs, 34sec)
'''
full_finish_pattern = re.compile("Finished At:[\s\t]*[a-zA-Z0-9\-]+[\s\t]*[0-9\:]+[\s\t]*\((.*)\)")
digit_pattern = re.compile("([0-9]+)[hrs|mins|sec]")
def parse_duration(line):
    match = full_finish_pattern.match(line)
    if match:
        duration = match.group(1).split(", ")
        d = 0
        for t in duration:
            match = digit_pattern.match(t)
            assert match
            if t.endswith("hrs"): d += (int(match.group(1)) * 60 * 60)
            if t.endswith("mins"):d += (int(match.group(1)) * 60)
            if t.endswith("sec"):d += int(match.group(1))        
        return d
    else:
        return None


'''
|FileSystemCounters            |FILE_BYTES_READ               |1,732,540 |49,971    |1,782,511
'''
file_bytes_read_pattern = re.compile("\|FileSystemCounters[\s\t]*\|FILE_BYTES_READ[\s\t]*\|([0-9,]+)[\s\t]*\|([0-9,]+)[\s\t]*\|[0-9,]+")    
def parse_bytes_read(line):
    match = file_bytes_read_pattern.match(line)
    if match:
        map = long(match.group(1).replace(",", ""))
        reduce = long(match.group(2).replace(",", ""))
        return map, reduce
    else:
        return None

'''
|FileSystemCounters            |FILE_BYTES_WRITTEN            |1,856,833 |74,710    |1,931,543
'''
file_bytes_written_pattern = re.compile("\|FileSystemCounters[\s\t]*\|FILE_BYTES_WRITTEN[\s\t]*\|([0-9,]+)[\s\t]*\|([0-9,]+)[\s\t]*\|[0-9,]+")    
def parse_bytes_written(line):
    match = file_bytes_written_pattern.match(line)
    if match:
        map = long(match.group(1).replace(",", ""))
        reduce = long(match.group(2).replace(",", ""))
        return map, reduce
    else:
        return None

'''
|Map-Reduce Framework          |Map output materialized bytes |49,983    |0         |49,983
'''
map_out_materialized_pattern = re.compile("\|Map-Reduce Framework[\s\t]*\|Map output materialized bytes[\s\t]*\|([0-9,]+)[\s\t]*.*")    
def parse_map_output_materialized_bytes(line):
    match = map_out_materialized_pattern.match(line)
    if match:
        return long(match.group(1).replace(",", ""))
    else:
        return None

'''
|Map-Reduce Framework          |Map output bytes              |243,760,393|0         |243,760,393
'''
map_out_pattern = re.compile("\|Map-Reduce Framework[\s\t]*\|Map output bytes[\s\t]*\|([0-9,]+)[\s\t]*.*")    
def parse_map_output_bytes(line):
    match = map_out_pattern.match(line)
    if match:
        return long(match.group(1).replace(",", ""))
    else:
        return None

'''
The pattern is: |FileSystemCounters            |HDFS_BYTES_READ               |1,026,555,427|0         |1,026,555,427
'''
job_size_pattern = re.compile("\|FileSystemCounters[\s\t]*\|HDFS_BYTES_READ[\s\t]*\|([0-9,]+)\|[\s\t]*[0-9,]+[\s\t]*\|[0-9,]+")    
def parse_job_input_size(line):
    match = job_size_pattern.match(line)
    if match:
        s = match.group(1)
        s = s.replace(",", "")
        size = long(s) / math.pow(2, 20)
        return size
    else:
        return None

'''
The pattern is: |Job Counters                  |Launched map tasks            |0         |0         |15
'''  
map_task_count_pattern = re.compile("\|Job Counters[\s\t]*\|Launched map tasks[\s\t]*\|[0-9,]+[\s\t]*\|[0-9,]+[\s\t]*\|([0-9,]+)") 
def parse_map_task_count(line):
    match = map_task_count_pattern.match(line)
    if match:
        return int(match.group(1).replace(",", ""))
    else:
        return None

'''
The pattern is: |Job Counters                  |Launched reduce tasks         |0         |0         |1
'''
reduce_task_count_pattern = re.compile("\|Job Counters[\s\t]*\|Launched reduce tasks[\s\t]*\|[0-9,]+[\s\t]*\|[0-9,]+[\s\t]*\|([0-9,]+)")
def parse_reduce_task_count(line):
    match = reduce_task_count_pattern.match(line)
    if match:
        return int(match.group(1).replace(",", ""))
    else:
        return None

'''
The pattern is: |Map-Reduce Framework          |Combine output records        |3,283,000 |0         |3,283,000
'''
combine_output_records_pattern = re.compile("\|Map-Reduce Framework[\s\t]*\|Combine output records[\s\t]*\|([0-9,]+)[\s\t]*\|[0-9,]+[\s\t]*\|[0-9,]+")
def parse_combine_output_records(line):
    match = combine_output_records_pattern.match(line)
    if match:
        return long(match.group(1).replace(",", ""))
    else:
        return None

'''
The pattern is: |Map-Reduce Framework          |Reduce output records         |0         |1,000     |1,000
'''
reduce_output_records_pattern = re.compile("\|Map-Reduce Framework[\s\t]*\|Reduce output records[\s\t]*\|[0-9,]+[\s\t]*\|([0-9,]+)[\s\t]*\|[0-9,]+")
def parse_reduce_output_records(line):
    match = reduce_output_records_pattern.match(line)
    if match:
        return long(match.group(1).replace(",", ""))
    else:
        return None

def parse_history_log(line, run):
    duration = parse_duration(line)
    if duration:
        if "duration" not in run:
            run['duration'] = []    
        run['duration'].append(duration)
        return
    size = parse_job_input_size(line)
    if size:
        if "input-size" not in run:
            run['input-size'] = []    
        run['input-size'].append(size)
        return
    map_task_count = parse_map_task_count(line)
    if map_task_count:
        if "map-task-count" not in run:
            run['map-task-count'] = []
        run['map-task-count'].append(map_task_count)
        return
    reduce_task_count = parse_reduce_task_count(line)
    if reduce_task_count:
        if "reduce-task-count" not in run:
            run['reduce-task-count'] = []
        run['reduce-task-count'].append(reduce_task_count)
        return
    combine_out_recs = parse_combine_output_records(line)
    if combine_out_recs:
        if "combine-output-records" not in run:
            run['combine-output-records'] = []
        run['combine-output-records'].append(combine_out_recs)
        return
    reduce_out_recs = parse_reduce_output_records(line)
    if reduce_out_recs:
        if "reduce-output-records" not in run:
            run['reduce-output-records'] = []
        run['reduce-output-records'].append(reduce_out_recs)
        return
    bytes_read = parse_bytes_read(line)
    if bytes_read:
        if "map-bytes-read" not in run:
            run['map-bytes-read'] = []
            run['reduce-bytes-read'] = []
        run['map-bytes-read'].append(bytes_read[0])
        run['reduce-bytes-read'].append(bytes_read[0])
        return
    bytes_written = parse_bytes_written(line)
    if bytes_written:
        if "map-bytes-written" not in run:
            run['map-bytes-written'] = []
            run['reduce-bytes-written'] = []
        run['map-bytes-written'].append(bytes_written[0])
        run['reduce-bytes-written'].append(bytes_written[0])
        return
    map_out_bytes = parse_map_output_bytes(line)
    if map_out_bytes:
        if "map-output-bytes" not in run:
            run['map-output-bytes'] = []
        run['map-output-bytes'].append(map_out_bytes)
        return
    map_out_materialized_bytes = parse_map_output_materialized_bytes(line)
    if map_out_materialized_bytes:
        if "map-output-materialized-bytes" not in run:
            run['map-output-materialized-bytes'] = []
        run['map-output-materialized-bytes'].append(map_out_materialized_bytes)
        return
    
if __name__ == '__main__':
    print parse_duration("Finished At: 13-May-2013 10:55:18 (4mins, 11sec)")
    print parse_duration("Finished At: 13-May-2013 10:51:51 (49sec)")
    print parse_duration("Finished At: 14-May-2013 13:17:17 (1hrs, 4mins, 34sec)")
