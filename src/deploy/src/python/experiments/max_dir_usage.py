'''
 A script to plot the task counts for the different jobs.
'''

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib
import numpy as np

import sys
import os
import re
import math

p = os.path.join(os.path.dirname(os.path.realpath(__file__)), '.')
sys.path.insert(0, p)

import ram_monitor_parser
import formatting

def plot_chart(runs):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('Sub Job Input Size (MB)')
    ax.set_ylabel('Size (MB)')
    ind = np.arange(len(runs))
    ax.set_xlim([-1,len(runs)])

    ax.set_ylim([0,250])
#     ax.set_yscale('log')

    runs = sorted(runs, key=lambda run:run['defined-size'])
    runs = [runs[-1]] + runs[:-1]
    
    ttprivate_data = [[filter(lambda d:d['name'] == "./ttprivate", data['dirs'])[0]['size'] for data in run['data'][:-1]] for run in runs]
    taskTracker_data = [[filter(lambda d:d['name'] == "./taskTracker", data['dirs'])[0]['size'] for data in run['data'][:-1]] for run in runs]
    ttprivate = [np.array(d) for d in ttprivate_data]    
    taskTracker = [np.array(filter(lambda s:s!=16384, d)) for d in taskTracker_data]
#     print taskTracker
#     rects0 = ax.bar(ind, [y.mean()/math.pow(2,20) for y in ttprivate], 0.25, color=plt.get_cmap('gray')(0.3), align='center', yerr=[y.std()/math.pow(2,20) for y in ttprivate])
#     rects1 = ax.bar(0.25 + ind, [y.mean()/math.pow(2,20) for y in taskTracker], 0.25, color=plt.get_cmap('gray')(0.6), align='center', yerr=[y.std()/math.pow(2,20) for y in taskTracker])
    rects1 = ax.bar(ind, [y.mean()/math.pow(2,20) for y in taskTracker], 0.25, color=plt.get_cmap('gray')(0.6), align='center', yerr=[y.std()/math.pow(2,20) for y in taskTracker])
    
    xlabels = [62657, 196, 588, 980, 1371, 1763, 2154, 4896, 9595, 19189]
    xaxis = ax.get_xaxis()
    plt.xticks(ind, xlabels, horizontalalignment="center", rotation= -45)
    plt.setp(ax.get_xticklines(), visible=False)

#     ax.legend((rects0,rects1), ("ttprivate", "taskTracker"), ncol=2)

def perform_additional_formatting():
    matplotlib.rcParams['figure.subplot.bottom'] = 0.25
    
if __name__ == '__main__':
    
    log_dir = sys.argv[1]
    runs = ram_monitor_parser.parse_logs(log_dir)
    formatting.perform_pure_formatting()
    perform_additional_formatting()
    plot_chart(runs)
    plt.show()
