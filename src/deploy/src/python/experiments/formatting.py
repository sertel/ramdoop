import matplotlib

"""
Below BW trick is due to:
http://stackoverflow.com/questions/7358118/matplotlib-black-white-colormap-with-dashes-dots-etc
"""

def setAxLinesBW(ax):
    """
    Take each Line2D in the axes, ax, and convert the line style to be 
    suitable for black and white viewing.
    """
    MARKERSIZE = 3

    COLORMAP = {
        'b': {'marker': "|", 'dash': (None,None)},
        'g': {'marker': "x", 'dash': [5,5]},
        'r': {'marker': "^", 'dash': [4,3,1,3]},
        'c': {'marker': "o", 'dash': [1,3]},
        'm': {'marker': "D", 'dash': [5,2,5,2,5,10]},
        'y': {'marker': "H", 'dash': [5,3,1,2,1,10]},
        'k': {'marker': 'x', 'dash': (None,None)} #[1,2,1,10]}
        }

    for line in ax.get_lines():
        origColor = line.get_color()
        line.set_color('black')
        line.set_dashes(COLORMAP[origColor]['dash'])
        line.set_marker(COLORMAP[origColor]['marker'])
        line.set_markersize(MARKERSIZE)
#        line.set_markerfacecolor('white')

def setFigLinesBW(fig):
    """
    Take each axes in the figure, and for each line in the axes, make the
    line viewable in black and white.
    """
    for ax in fig.get_axes():
        setAxLinesBW(ax)
        
def perform_pure_formatting():
    
    fig_width_pt = 240.0  # Get this from LaTeX using \showthe\columnwidth
    inches_per_pt = 1.0/72.27               # Convert pt to inch
    import math
    golden_mean = (math.sqrt(5)-1.0)/2.0         # Aesthetic ratio
    fig_width = fig_width_pt*inches_per_pt  # width in inches
#    print "width: ", fig_width
    fig_height = fig_width*golden_mean      # height in inches
#    print "height: ", fig_height
    fig_size =  [fig_width,fig_height]
    
    # check out this site for more options: http://matplotlib.sourceforge.net/users/customizing.html
    params = {'backend': 'pdf',
             'axes.labelsize': 12,
             'text.fontsize': 12,
             'lines.linewidth' : 0.6,
             'lines.markersize'  : 3,
             'xtick.labelsize': 12,
             #'xtick.major.pad' : 100,
             #'xtick.minor.pad' : 100,
             'ytick.labelsize': 12,
             'legend.fontsize' : 12,
             'legend.borderpad' : 0.25,
             'legend.labelspacing' : 0.7,
             'legend.columnspacing' :0.8,
             'figure.subplot.top' : 0.9,
             'figure.subplot.bottom' : 0.15,
             'figure.subplot.right' : 0.9,
             'figure.subplot.left' : 0.1,
             #'figure.subplot.wspace'  : 0.2,
             #'figure.subplot.hspace'  : 0.2,
             #'savefig.bbox' : 'tight',
             #'savefig.pad_inches' : 1.0,
             'text.usetex': True,
             'figure.figsize': fig_size}
    matplotlib.rcParams.update(params)
    return params

def update_font_size(size):
    params = {'axes.labelsize': size,
             'text.fontsize': size,
             'xtick.labelsize': size,
             'ytick.labelsize': size,
             'legend.fontsize' : size}
    matplotlib.rcParams.update(params)

def update_line_size(size):
    params = {'lines.linewidth' : size}
    matplotlib.rcParams.update(params)
    