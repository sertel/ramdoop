import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.pyplot import *

import sys
import os
import re
import math

p = os.path.join(os.path.dirname(os.path.realpath(__file__)), './../../')
sys.path.insert(0, p)

import formatting
project_root = p + "../../../../../"
target_dir = project_root + "papers/tiled-mr-paper/plotting/"
# data_source = project_root + "experiments/results/camera_ready_results/motivation/slowstart_overview.plotdata"
data_source = project_root + "experiments/results/camera_ready_results/RawData/execution_time/execution_time.plot"

def perform_additional_formatting():
#     matplotlib.rcParams['font.size'] = 12
    matplotlib.rcParams['figure.subplot.bottom'] = 0.25
    matplotlib.rcParams['figure.subplot.left'] = 0.18

if __name__ == '__main__':
    
    formatting.perform_pure_formatting()
    perform_additional_formatting()
    formatting.update_font_size(13)
    formatting.update_line_size(1.4)

    data=np.loadtxt(data_source, skiprows=1, delimiter=" ")
    plt.xlabel("Slow start [\%]");
    plt.ylabel("Execution Time [m]");
    plt.plot([i[0]*100 for i in data], [i[1] for i in data], marker="^", fillstyle="none", color="k", markersize=10);
#     plt.plot([i[0]*100 for i in data], [i[1]/60.0 for i in data], marker="^", fillstyle="none", color="k", markersize=10);
    plt.savefig(target_dir + "slowstart_runtime.pdf")
