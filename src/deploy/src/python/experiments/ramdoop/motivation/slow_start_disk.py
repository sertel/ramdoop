import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.pyplot import *

import sys
import os
import re
import math

p = os.path.join(os.path.dirname(os.path.realpath(__file__)), './../../')
sys.path.insert(0, p)

import formatting
project_root = p + "../../../../../"
target_dir = project_root + "papers/tiled-mr-paper/plotting/"
# data_source = project_root + "experiments/results/camera_ready_results/motivation/slowstart_disk_io.plotdata"
data_source = project_root + "experiments/results/camera_ready_results/RawData/disk_access/disk_access.plot"
# input_data_size = 200 #in GB
input_data_size = 3.2 * 20 #in GB
# input_data_size = 0 #in GB

def perform_additional_formatting():
    matplotlib.rcParams['figure.subplot.bottom'] = 0.25
    matplotlib.rcParams['figure.subplot.left'] = 0.18

if __name__ == '__main__':
    
    formatting.perform_pure_formatting()
    formatting.update_font_size(13)
    formatting.update_line_size(1.4)
    perform_additional_formatting()
    data=np.loadtxt(data_source, skiprows=1, delimiter=" ")
    plt.xlabel("Slow start [\%]");
    plt.ylabel("Disk Reads [GB]");
    print input_data_size
    #plt.plot([i[0]*100 for i in data], [i[1]/60.0 for i in data], marker="^", fillstyle="none", color="k", markersize=10);
    plt.plot([i[0]*100 for i in data], [(((i[1]/1024)/1024)/1024)-input_data_size for i in data], marker="^", fillstyle="none", color="k", markersize=10);
    plt.savefig(target_dir + "slowstart_disk_io.pdf")
