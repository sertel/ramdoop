package HiBench;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.NLineInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import com.ramdoop.tiled.api.DynamicMapFileOutputFormat;

public class PagerankData {

	private static final Log log = LogFactory.getLog(PagerankData.class.getName());

	private DataOptions options;

	private static final String VERTICALS_DIR_NAME = "vertices";
	private static final String EDGES_DIR_NAME = "edges";
	private boolean balance = false; // original PAGERANK_NODE_BALANCE

	private String cdelim = "\t";

	private Dummy dummy;

	PagerankData(DataOptions options) {
		this.options = options;
		parseArgs(options.getRemainArgs());
	}

	private void parseArgs(String[] args) {

		for (int i = 0; i < args.length; i++) {

			if ("-d".equals(args[i])) {
				cdelim = args[++i];
			} else if ("-pbalance".equals(args[i])) {
				balance = true;
			} else {
				DataOptions.printUsage("Unknown pagerank data arguments -- " + args[i] + "!!!");
			}
		}
	}

	public void init() throws IOException {

		log.info("Initializing PageRank data generator...");

		Utils.checkHdfsPath(options.getResultPath(), true);
		Utils.checkHdfsPath(options.getWorkPath(), true);

		Utils.serialLinkZipf(options);

		dummy = new Dummy(options.getWorkPath(), options.getNumMaps());
	}

	private void setPageRankNodesOptions(Configuration job) {
		job.setLong("pages", options.getNumPages());
		job.setLong("slotpages", options.getNumSlotPages());
	}

	private void setPageRankLinksOptions(JobConf job) throws URISyntaxException {
		job.setLong("pages", options.getNumPages());
		job.setLong("slotpages", options.getNumSlotPages());
		job.set("delimiter", cdelim);

		Utils.shareLinkZipfCore(options, job);
	}

	public static class BalancedLinkNodesMapper extends
			org.apache.hadoop.mapreduce.Mapper<LongWritable, Text, LongWritable, NullWritable> {

		@Override
		protected void map(LongWritable key, Text value,
				org.apache.hadoop.mapreduce.Mapper.Context context) throws java.io.IOException,
				java.lang.InterruptedException {
			String delimiter = "[ \t]";
			String[] pair = value.toString().split(delimiter);

			context.write(new LongWritable(Long.parseLong(pair[0])), NullWritable.get());
		}
	}

	public static class BalancedLinkNodesReducer extends
			Reducer<LongWritable, NullWritable, NullWritable, Text> {

		@Override
		protected void reduce(LongWritable key, java.lang.Iterable values,
				org.apache.hadoop.mapreduce.Reducer.Context context) throws java.io.IOException,
				java.lang.InterruptedException {
			context.write(NullWritable.get(), new Text(key.toString()));
		}
	}

	public static class DummyToNodesMapper extends Mapper<LongWritable, Text, LongWritable, Text> {

		private long pages, slotpages;

		private void getOptions(Configuration job) {
			pages = job.getLong("pages", 0);
			slotpages = job.getLong("slotpages", 0);
		}

		@Override
		protected void setup(org.apache.hadoop.mapreduce.Mapper.Context context)
				throws java.io.IOException, java.lang.InterruptedException {
			getOptions(context.getConfiguration());
		}

		@Override
		protected void map(LongWritable key, Text value,
				org.apache.hadoop.mapreduce.Mapper.Context context) throws java.io.IOException,
				java.lang.InterruptedException {

			int slotId = Integer.parseInt(value.toString().trim());
			long[] range = HtmlCore.getPageRange(slotId, pages, slotpages);

			for (long i = range[0]; i < range[1]; i++) {
				key.set(i);
				Text v = new Text(Long.toString(i));
				context.write(key, v);
				context.getCounter(HiBench.Counters.BYTES_DATA_GENERATED).increment(
						8 + v.getLength());
			}
		}
	}

	private void createPageRankNodesDirectly() throws IOException, ClassNotFoundException,
			InterruptedException {

		log.info("Creating PageRank nodes...", null);

		Path fout = new Path(options.getResultPath(), VERTICALS_DIR_NAME);

		Job job = new Job();
		job.setJarByClass(PagerankData.class);
		String jobname = "Create pagerank nodes";

		job.setJobName(jobname);
		setPageRankNodesOptions(job.getConfiguration());

		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(Text.class);

		FileInputFormat.setInputPaths(job, dummy.getPath());
		job.setInputFormatClass(NLineInputFormat.class);

		if (balance) {
			/***
			 * Balance the output order of nodes, to prevent the running of
			 * pagerank bench from potential data skew
			 */
			job.setMapOutputKeyClass(LongWritable.class);
			job.setMapOutputValueClass(NullWritable.class);

			job.setMapperClass(BalancedLinkNodesMapper.class);
			job.setReducerClass(BalancedLinkNodesReducer.class);
			// job.setPartitionerClass(ModulusPartitioner.class);

			if (options.getNumReds() > 0) {
				job.setNumReduceTasks(options.getNumReds());
			} else {
				job.setNumReduceTasks(Utils.getMaxNumReds());
			}
		} else {
			job.setMapOutputKeyClass(Text.class);
			job.setMapperClass(DummyToNodesMapper.class);
			job.setNumReduceTasks(0);
		}

		if (options.isSequenceOut()) {
			job.setOutputFormatClass(SequenceFileOutputFormat.class);
		} else {
			job.setOutputFormatClass(TextOutputFormat.class);
		}

		if (null != options.getCodecClass()) {
			job.getConfiguration().set("mapred.output.compression.type", "BLOCK");
			FileOutputFormat.setCompressOutput(job, true);
			FileOutputFormat.setOutputCompressorClass(job, options.getCodecClass());
		}

		FileOutputFormat.setOutputPath(job, fout);

		log.info("Running Job: " + jobname);
		log.info("Dummy file " + dummy.getPath() + " as input");
		log.info("Vertices file " + fout + " as output");
		job.waitForCompletion(true);
		log.info("Finished Running Job: " + jobname);
	}

	public static class DummyToPageRankLinksMapper extends
			Mapper<LongWritable, Text, LongWritable, Text> {

		private static final Log log = LogFactory
				.getLog(DummyToPageRankLinksMapper.class.getName());
		private HtmlCore html;
		private long pages, slotpages;
		private String delim;

		private void getOptions(Configuration job) {
			pages = job.getLong("pages", 0);
			slotpages = job.getLong("slotpages", 0);
			delim = job.get("delimiter");
			System.out.println("Found pages: " + pages);
		}

		@Override
		protected void setup(org.apache.hadoop.mapreduce.Mapper.Context context)
				throws java.io.IOException, java.lang.InterruptedException {

			try {
				html = new HtmlCore(new JobConf(context.getConfiguration()));

				getOptions(context.getConfiguration());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		protected void map(LongWritable key, Text value,
				org.apache.hadoop.mapreduce.Mapper.Context context) throws java.io.IOException,
				java.lang.InterruptedException {

			int slotId = Integer.parseInt(value.toString().trim());
			html.fireRandom(slotId);

			long[] range = HtmlCore.getPageRange(slotId, pages, slotpages);

			/**
			 * For output collect
			 */
			for (long i = range[0]; i < range[1]; i++) {
				key.set(i);
				String from = key.toString(), to;

				long[] linkids = html.genPureLinkIds();
				for (int j = 0; j < linkids.length; j++) {
					to = Long.toString(linkids[j]);
					Text v = new Text(from + delim + to);
					context.write(key, v);
					context.getCounter(HiBench.Counters.BYTES_DATA_GENERATED).increment(
							8 + v.getLength());
				}

				if (0 == (i % 10000)) {
					log.info("still running: " + (i - range[0]) + " of " + slotpages);
				}
			}
		}
	}

	private void createPageRankLinksDirectly() throws IOException, URISyntaxException, ClassNotFoundException, InterruptedException {

		log.info("Creating PageRank links", null);

		Job job = new Job();
		job.setJarByClass(PagerankData.class);
		String jobname = "Create pagerank links";

		Path fout = new Path(options.getResultPath(), EDGES_DIR_NAME);

		job.setJobName(jobname);
		setPageRankLinksOptions((JobConf)job.getConfiguration());

		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(Text.class);
		// job.setMapOutputKeyClass(LongWritable.class);
		// job.setMapOutputValueClass(Text.class);

		job.setNumReduceTasks(0);

		FileInputFormat.setInputPaths(job, dummy.getPath());
		job.setInputFormatClass(NLineInputFormat.class);

		job.setMapperClass(DummyToPageRankLinksMapper.class);

		if (options.isSequenceOut()) {
			// edited by Sebastian
			job.setOutputFormatClass(DynamicMapFileOutputFormat.class);
			// job.setOutputFormat(MapFileOutputFormat.class);
			// job.setOutputFormat(SequenceFileOutputFormat.class);
		} else {
			job.setOutputFormatClass(TextOutputFormat.class);
		}

		if (null != options.getCodecClass()) {
			job.getConfiguration().set("mapred.output.compression.type", "BLOCK");
			FileOutputFormat.setCompressOutput(job, true);
			FileOutputFormat.setOutputCompressorClass(job, options.getCodecClass());
		}

		FileOutputFormat.setOutputPath(job, fout);

		log.info("Running Job: " + jobname);
		log.info("Dummy file " + dummy.getPath() + " as input");
		log.info("Edges file " + fout + " as output");
		job.waitForCompletion(true);
		log.info("Finished Running Job: " + jobname);
	}

	public void generate() throws IOException, URISyntaxException, ClassNotFoundException, InterruptedException {

		log.info("Generating pageRank data files...");
		init();
		createPageRankNodesDirectly();
		createPageRankLinksDirectly();
		closeGenerator();
	}

	private void closeGenerator() throws IOException {

		log.info("Closing pagerank data generator...");
		Utils.checkHdfsPath(options.getWorkPath(), true);
	}
}
