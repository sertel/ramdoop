time=$(cat ../../1.runtime.log | grep JOB | awk '{print $3}')
time=$(echo $time | sed 's/" "//g')
startTime=${time/" "*/}
stopTime=${time/*" "/}

list_of_files="[";
for nodeStat in $(ls log.stream*.mem)
do
	cat $nodeStat | awk -v startTime=$startTime -v stopTime=$stopTime\
		'$1 >= startTime && $1 <= stopTime {print $1","$4,$8,$11}' > $nodeStat.tempplot
	rm -f $nodeStat.plot
	while read line
	do
		echo -e $(date -d "${line/,*/}" +%s)" ${line/*,/}" >> $nodeStat.plot
	done < $nodeStat.tempplot
	rm -f $nodeStat.tempplot
	list_of_files=$list_of_files"(\"$nodeStat.plot\",\"Memory\"),"
done
list_of_files=$(echo $list_of_files | sed 's/,$//g')"]"

python - <<END
import numpy as np
import matplotlib.pyplot as plt
list_of_files = $list_of_files;
datalist = [ ( np.loadtxt(filename, skiprows=1, delimiter=" "), label ) for filename, label in list_of_files ]
plt.figure(figsize=(12.21,5.10))
for data, plotLabel in datalist:
        minNet=min(data[:,0]);
        maxNet=max(data[:,0]);
        plt.title("Memory usage");
        plt.xlabel("Seconds");
        plt.ylabel("Megabytes");
        plt.grid(True);
        plt.xlim(0,maxNet-minNet);
        plt.ylim((0,8000));
        plt.plot(data[:,0]-minNet,data[:,1]);
        plt.plot(data[:,0]-minNet,data[:,3]);
        plt.savefig("memory.png",bbox_inches="tight",dpi=100);
print "Done!"
END

list_of_files="[";
for nodeStat in $(ls log.stream*.top)
do
        cat $nodeStat | awk -v startTime=$startTime -v stopTime=$stopTime\
	'$1 >= startTime && $1 <= stopTime {print $1","$10,$11}' > $nodeStat.tempplot
        rm -f $nodeStat.plot
        while read line
        do
                echo -e $(date -d "${line/,*/}" +%s)" ${line/*,/}" >> $nodeStat.plot
        done < $nodeStat.tempplot
        rm -f $nodeStat.tempplot
	list_of_files=$list_of_files"(\"$nodeStat.plot\",\"Memory\"),"
done
list_of_files=$(echo $list_of_files | sed 's/,$//g')"]"

python - <<END
import numpy as np
import matplotlib.pyplot as plt
list_of_files = $list_of_files;
datalist = [ ( np.loadtxt(filename, skiprows=1, delimiter=" "), label ) for filename, label in list_of_files ]
plt.figure(figsize=(12.21,5.10))
for data, plotLabel in datalist:
        minNet=min(data[:,0]);
        maxNet=max(data[:,0]);
        plt.title("Redis cpu usage");
        plt.xlabel("Seconds");
        plt.ylabel("Percent");
        plt.grid(True);
        plt.xlim(0,maxNet-minNet);
#        plt.ylim((0,1));
        plt.plot(data[:,0]-minNet,data[:,1]);
        plt.plot(data[:,0]-minNet,data[:,2]);
        plt.savefig("cpu.png",bbox_inches="tight",dpi=100);
print "Done!"
END
