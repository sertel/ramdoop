for i in {30..49}
do
	ssh stream$i 'rm -rf /opt/siavash/hadoop/data' &
	ssh stream$i 'rm -rf /opt/siavash/hadoop/hadoop-1.2.1' &
done
wait
echo "Erased!"

for i in {30..49}
do
	scp -r /opt/siavash/hadoop/hadoop-1.2.1 stream$i:/opt/siavash/hadoop/. &
done

wait
echo "Deployed!"
