mainLogDir=/opt/siavash/LOGS;                            ###   TOOLS MAIN DIRECTORY  ###

cd $mainLogDir;

myIP=$(hostname -I | cut -d' ' -f1);
rm -f $myIP.snd* $myIP.rcv*;

awk '{print $1"."$3"."$5"."$7}' tcpdump.log |\
	awk -v ip=$myIP -F'.'\
		'ip==$3"."$4"."$5"."$6 {print $1" "$8"."$9"."$10"."$11" "$13\
		>> ip".snd"};\
		ip==$8"."$9"."$10"."$11 {print $1" "$3"."$4"."$5"."$6" "$13\
		>> ip".rcv"}';

awk -F' ' '{arr[$1]=arr[$1]+$3} END{for (a in arr) print a, arr[a]}' $myIP.snd |\
	sort > $myIP.snd.red &

awk -F' ' '{arr[$1]=arr[$1]+$3} END{for (a in arr) print a, arr[a]}' $myIP.rcv |\
	sort > $myIP.rcv.red &

wait;
