if [[ $2 != "" ]]; then

	cat $1 | grep "Running job: $line\|Job complete: $line" | grep $2

elif [[ $2 == "" && $1 != "" ]]; then

	lineNumber=$(cat -n $1 | grep "Tile phase execution" | awk '{print $1}');

	firstPhase=$(head -$lineNumber $1 | grep "Job complete" | awk '{print $7}' | sort -n | tail -1);
	allPhases=$(cat $1 | grep "Job complete" | awk '{print $7}' | sort -n | tail -1);

	firstPhase=$(echo ${firstPhase/*_/} | sed 's/^0*//');
	allPhases=$(echo ${allPhases/*_/} | sed 's/^0*//');

	secondPhase=$(( allPhases - firstPhase ));
	
	badJobList=""
	minTime="9999999999"
	rm -f $1.plotinfo
	for line in $(cat $1 | grep "Job complete" | awk '{print $7}' | sort -n)
	do
		result=${line/*_/} 
		if [ $result -eq $(( firstPhase + 1 )) ]; then
			minTime="9999999999";
		fi
		for times in $(cat $1 | grep "Running job: $line\|Job complete: $line" | awk '{print $2}')
		do
			if [[ $times == *"submitted"* ]]; then
				result=$result" "$minTime
				badJobList=$badJobList"\n"$line;
			else
				result=$result" "$(date -d $times +%s)
				if [[ $minTime > $(date -d $times +%s) ]]; then
					minTime=$(date -d $times +%s);
				fi
			fi
		done
		echo $result >> $1.plotinfo
	done

	firstPhaseDuration=$(cat $1.plotinfo | awk -v num=$firstPhase 'NR==2 {min=$2; max=$3}; NR>2 && NR<=num && min>$2 {min=$2};\
						NR>2 && NR<=num && max<$3 {max=$3}; END{print max-min}');

	secondPhaseDuration=$(cat $1.plotinfo | awk -v num=$firstPhase 'NR==num+2 {min=$2; max=$3}; NR>2 && NR>num+1 && min>$2 {min=$2};\
						NR>2 && NR>num+1 && max<$3 {max=$3}; END{print max-min}');

	totalPhaseDuration=$(( $(cat $1.plotinfo | awk -v num=$firstPhase 'NR==num+2 {max=$3}; NR>2 && NR>num+1 && max<$3 {max=$3};\
			END{print max}') - $(cat $1.plotinfo | awk -v num=$firstPhase 'NR==2 {min=$2};\
			NR>2 && NR<=num && min>$2 {min=$2}; END{print min}') ));


	TotalDuration=$(( $(date -d "$(cat 1.runtime.log | grep "JOB COMPLETED" | awk '{print $3}')" +%s) - $(date -d "$(cat 1.runtime.log | grep "JOB SUBMITTED" | awk '{print $3}')" +%s) ));


	echo ""
	echo "***************************************************"
	grep -Zhr "ramdoop.input.job.size\|test.randomtextwrite.word_count\|DATASIZE=" |\
	grep -v "#" | sed 's/[ ]*//' | sed 's/ \\//g' | sed 's/-D //'
	echo ""
	echo "---------------------------------------------------"
	echo ""
	echo "$firstPhaseDuration $secondPhaseDuration $totalPhaseDuration $TotalDuration" |\
		awk '{printf("--Total job duration:           %d sec \t(%0.2f min)\
			    \n  |--Total minijob duration:    %d sec \t(%0.2f min)\
			    \n  |----First phase duration:    %d sec \t(%0.2f min)\
			    \n  |----GAP between phases:      %d sec \t\t(%0.2f min)\
			    \n  |----Second phase duration:   %d sec \t\t(%0.2f min)\
			    \n  |--Other calculations:        %d sec \t\t(%0.2f min)\n"\
			    ,$4,$4/60.0,$3,$3/60.0,$1,$1/60.0,$3-($1+$2),($3-($1+$2))/60.0,$2,$2/60.0,$4-$3,($4-$3)/60.0)}';
	echo "***************************************************"
	echo ""
	printf "Plotting... "

python - <<END
import numpy as np
import matplotlib.pyplot as plt
list_of_files = [ ("$1.plotinfo", "Runtime")]
datalist = [ ( np.loadtxt(filename, skiprows=1, delimiter=" "), label ) for filename, label in list_of_files ]
plt.figure(figsize=(12.21,5.10))
for data, plotLabel in datalist:
        minNet=min(data[:,1]);
        maxNet=max(data[:,2]);
        jobNum=max(data[:,0]);
        plt.title("minijobs run period $firstPhase,$secondPhase");
        plt.xlabel("Seconds");
        plt.ylabel("minijobs");
        plt.xticks(np.arange(0,maxNet-minNet,(maxNet-minNet)/10.0))
        plt.grid(True);
        plt.xlim(0,maxNet-minNet);
        plt.ylim((0,jobNum));
        plt.plot([data[:,1]-minNet, data[:,2]-minNet], [data[:,0] , data[:,0]] , 'k-', lw=1)	
        plt.savefig("$1.png",bbox_inches="tight",dpi=100);
print "Done!"
END

rm $1.plotinfo

#echo ""
#echo "We had problem with reading timespan of these jobs:"
#printf "$badJobList"
#echo ""

elif [[ $2 == "" && $1 == "" ]]; then
	echo "USAGE:"
	echo "./plotMiniJobRunTime.sh runtime.log	{Shows all jobs running period}"
	echo "./plotMiniJobRunTime.sh runtime.log jobID	{Shows [jobID]  running period}"
fi