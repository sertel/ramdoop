j=0;
for i in $(hadoop job -list | awk '{print $1}')
do
	hadoop job -kill $i &
	j=$(( j + 1 ));
	echo "JOB $j"
	if [[ "$j" == "20" ]]; then
		echo "Sleeping";
		sleep 3
		j=0;
	fi
done
wait
echo "Done"
