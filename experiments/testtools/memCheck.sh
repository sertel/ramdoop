readNo=$(vmstat -D | grep "read sectors" | awk '{print $1}');
writeNo=$(vmstat -D | grep "written sectors" | awk '{print $1}');

while read line 
do
if [[ $line == *"%"* ]]; then
	newReadNo=$(vmstat -D | grep "read sectors" | awk '{print $1}');
	newWriteNo=$(vmstat -D | grep "written sectors" | awk '{print $1}');

	echo $line, $(free -m | grep "Mem\|Swap" | awk 'NR==1 {A="U:"$3",F:"$4",B:"$6",C:"$7};\
		NR==2 {A=A",sU:"$3",sF:"$4}; END{print "["A"]"}'), $(( newReadNo-readNo  )), $(( newWriteNo-writeNo ));
	
	readNo=$(vmstat -D | grep "read sectors" | awk '{print $1}');
	writeNo=$(vmstat -D | grep "written sectors" | awk '{print $1}');

	
fi
done <&0
