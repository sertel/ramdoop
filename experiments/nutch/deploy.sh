#!/bin/bash

cp src/org/apache/nutch/indexer/Indexer.java $@/src/java/org/apache/nutch/indexer
cp src/org/apache/nutch/indexer/IndexerMapReduce.java $@/src/java/org/apache/nutch/indexer
cp src/org/apache/nutch/indexer/IndexerOutputFormat.java $@/src/java/org/apache/nutch/indexer

cp src/org/apache/nutch/indexer/solr/SolrIndexer.java $@/src/java/org/apache/nutch/indexer/solr

cp nutch $@/bin/

cp src/HiBench/NutchData.java $@/../../common/autogen/src/HiBench/

echo "When trying to build nutch, you need to modify the lines in the nutch build script that try to unpack the hadoop distro."
echo "Just copy all Hadoop jar files into the lib dir and then give it a try!"
echo "Make sure you rebuild not only Nutch but HiBench as well!"