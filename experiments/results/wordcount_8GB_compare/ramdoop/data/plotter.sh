
whichTest=wordcount;
howManyTimes=3;
platName='Ramdoop';
slowStartFrom=1000000000;
slowStartTo=5000000000;
step=1000000000;
parameter='Tilesize';
	


maxFinishingTime=0;
for (( i=1; i<=howManyTimes; i++ ))
do
        if [ ! -e $i.Sent.plot ]; then continue;  fi
        if [ "$(wc -l < $i.Sent.plot)" -lt "2"  ]; then continue;  fi
	finishingTime=$(awk 'END{print $1}' $i.Sent.plot);
	if [ "$finishingTime" -gt "$maxFinishingTime" ]; then
		maxFinishingTime=$finishingTime;
	fi
done

maxFinishingTime=$(( maxFinishingTime / 60 ));

if [ "$maxFinishingTime" -gt "9" ]
then
	tickGap=$(( maxFinishingTime / 10 ));
else
	tickGap=1;
fi

for (( i = 1; i <= howManyTimes; i++ ))
do
        if [ ! -e $i.Sent.plot ]; then continue;  fi
        if [ "$(wc -l < $i.Sent.plot)" -lt "2"  ]; then continue;  fi
	finishingPoint=$(awk 'END{print $1}' $i.Sent.plot);
	finishingPoint=$(( finishingPoint / 60 ));

python - <<END
import numpy as np
import matplotlib.pyplot as plt
list_of_files = [ ("$i.Sent.plot", "Sent"), ("$i.Received.plot", "Received")]
datalist = [ ( np.loadtxt(filename, skiprows=1, delimiter=" "), label ) for filename, label in list_of_files ]
plt.figure(figsize=(12.21,5.10))
for data, plotLabel in datalist:
	maxNet=max(data[:,1]) / 1000000;
        plt.title("$whichTest $platName Network Traffic Peak\n $parameter set to $(awk 'NR==1 {print $2/100}' $i.io.log) GB");
        plt.xlabel("Minutes");
        plt.ylabel("Mega bytes");
	plt.xticks(np.arange(0, $maxFinishingTime, $tickGap))
	plt.grid(True);
	plt.xlim((0,$maxFinishingTime));
	plt.ylim((0,128));
        plt.plot( data[:,0]/60, data[:,1]/1000000);
	plt.plot([0, $finishingPoint], [maxNet, maxNet], color="r", linestyle="-.", linewidth=1);
	plt.text(0, maxNet, plotLabel, fontsize=10, color="r");
	plt.annotate("Completed\n $finishingPoint", xy=($finishingPoint, 0),\
			xycoords="data", xytext=($finishingPoint - $tickGap, 120),\
			textcoords="data", size=10, ha="center",\
			bbox=dict(boxstyle="round4,pad=.5", fc="0.9"),\
			arrowprops=dict(arrowstyle="->", connectionstyle="angle,angleA=0,angleB=-90,rad=10"));

        plt.savefig("NetworkTrafficPeak-$i.pdf",bbox_inches="tight",dpi=100, format="PDF");

print "."

END

done

python - <<END
import numpy as np
import matplotlib.pyplot as plt
list_of_files = [ ("io.ttl.plot", "Read")]
datalist = [ ( np.loadtxt(filename, skiprows=1, delimiter=" "), label ) for filename, label in list_of_files ]
plt.figure(figsize=(12.21,5.10))
for data, plotLabel in datalist:
        plt.title("$whichTest $platName Disk Access (actual)");
        plt.xlabel("$parameter (GB)");
        plt.ylabel("Sectors read (Million)");
	plt.xticks(np.arange($slowStartFrom, ($slowStartTo+$step), $step))
	plt.grid(True);
	plt.xlim(($slowStartFrom-$step), ($slowStartTo+$step));
        plt.bar(((data[:,1]/100)-($step / 4)), data[:,2]/1000000, ($step/2), color="r");
        plt.savefig("DiskRead.pdf",bbox_inches="tight",dpi=100, format="PDF");

print "."

END

pdfunite NetworkTrafficPeak-*.pdf $platName-$whichTest.pdf;

