import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.pyplot import *

################################
###### SOME FUNCTIONS   ########
################################
def savePlot(graphName):
	perform_pure_formatting()
	#plt.tight_layout()
	#plt.savefig("motivation.png",bbox_inches="tight",dpi=100);
	plt.savefig(graphName+".pdf",bbox_inches="tight",dpi=100, format="PDF");

def perform_pure_formatting():
    
    fig_width_pt = 240.0  # Get this from LaTeX using \showthe\columnwidth
    inches_per_pt = 1.0/72.27               # Convert pt to inch
    import math
    golden_mean = (math.sqrt(5)-1.0)/2.0         # Aesthetic ratio
    fig_width = fig_width_pt*inches_per_pt  # width in inches
#    print "width: ", fig_width
    fig_height = fig_width*golden_mean      # height in inches
#    print "height: ", fig_height
    fig_size =  [fig_width,fig_height]
    
    # check out this site for more options: http://matplotlib.sourceforge.net/users/customizing.html
    params = {'backend': 'pdf',
             'axes.labelsize': 12,
             'text.fontsize': 12,
             'lines.linewidth' : 0.6,
             'lines.markersize'  : 3,
             'xtick.labelsize': 12,
             #'xtick.major.pad' : 100,
             #'xtick.minor.pad' : 100,
             'ytick.labelsize': 12,
             'legend.fontsize' : 12,
             'legend.borderpad' : 0.25,
             'legend.labelspacing' : 0.7,
             'legend.columnspacing' :0.8,
             'figure.subplot.top' : 0.9,
             'figure.subplot.bottom' : 0.15,
             'figure.subplot.right' : 0.9,
             'figure.subplot.left' : 0.1,
             #'figure.subplot.wspace'  : 0.2,
             #'figure.subplot.hspace'  : 0.2,
             #'savefig.bbox' : 'tight',
             #'savefig.pad_inches' : 1.0,
             'text.usetex': True,
             'figure.figsize': fig_size}
    matplotlib.rcParams.update(params)
    return params






################################
###### Plotting graphs  ########
################################
#Impact of slowstart parameter on Hadoop performance
plt.figure(figsize=(12.21,5.10))
#plt.subplot2grid((2,4), (0, 0), rowspan=1)
plt.tick_params(\
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='on',      # ticks along the bottom edge are off
    top='on',         # ticks along the top edge are off
    labelbottom='on') # labels along the bottom edge are off
data=np.loadtxt("slowstart_overview.plotdata", skiprows=0, delimiter=" ")
plotLabel="slowstart_overview"
plt.title("(a)");
#plt.title("(a)\n10GB per node");
plt.xlabel("Slowstart");
plt.ylabel("Runtime (minute)");
plt.xticks(np.arange(0.0, 1.1, 0.1))
#plt.grid(True);
plt.xlim(-0.1, 1.1);
plt.ylim(0, 50);
#plt.bar(data[:,0]-0.04, data[:,1]/60.0, color=(0.3, 0.3, 0.3));
plt.plot(data[:,0], data[:,1]/60.0, color=(0.3, 0.3, 0.3), linewidth=4.0);
savePlot(plotLabel);





#Impact of Slowstart on disk I/O
plt.figure(figsize=(12.21,5.10))
#plt.subplot2grid((2,4), (1, 0), rowspan=1)
plt.tick_params(\
    axis='x',
    which='both',
    bottom='on',
    top='on',
    labelbottom='on')
data=np.loadtxt("slowstart_disk_io.plotdata", skiprows=1, delimiter=" ")
plotLabel="slowstart_disk_io"
plt.title("(b)");
#plt.title("(b)\n10GB per node");
plt.xlabel("Slowstart");
plt.ylabel("Total disk reads (GB)");
plt.xticks(np.arange(0.0, 1.1, 0.1))
#plt.grid(True);
plt.xlim(-0.1, 1.1);
plt.ylim(0, 400);
#plt.bar(data[:,0]-0.04, ((data[:,1]*512)/1073741824), 0.08, color=(0.3, 0.3, 0.3));
plt.plot(data[:,0], ((data[:,1]*512)/1073741824), color=(0.3, 0.3, 0.3), linewidth=4.0);
savePlot(plotLabel);





# Hadoop performance with Slowstart=0
plt.figure(figsize=(12.21,5.10))
#plt.subplot2grid((2,4), (0, 1), colspan=1)
plt.tick_params(\
    axis='x',
    which='on',
    bottom='off',
    top='on',
    labelbottom='on')
data=np.loadtxt("slowstart_0.0.plotdata", skiprows=1, delimiter=" ")
plotlabel="slowstart_0.0"
maxSend=max(data[:,1]) / 1000000;
minSend=min(data[:,1]) / 1000000;
maxReceive=max(data[:,2]) / 1000000;
minReceive=min(data[:,2]) / 1000000;
maximum=max(maxSend,maxReceive);
minimum=min(minSend,minReceive);
finishPoint=max(data[:,0])/60.0;
plt.title("(c)");
plt.text(49, 120, "Slowstart = 0.0 , Datasize= 10GB per node\nMax network traffic="+str(maximum)+", Min network traffic="+str(minimum),\
	horizontalalignment='right',verticalalignment='center', color=(0,0,0), weight='bold', size=10)
plt.xlabel("Execution Time (Minutes)");
plt.ylabel("Network I/O (MB/s)");
plt.xticks(np.arange(0, 55, 5))
#plt.grid(True);
plt.xlim((0,50));
plt.ylim((0,128));
plt.plot( data[:,0]/60, data[:,1]/1000000, color=(0.3, 0.3, 0.3));
plt.plot( data[:,0]/60, data[:,2]/1000000, color=(0.3, 0.3, 0.3));
plt.annotate("Completed\n"+str(('%.2f' % (finishPoint,))), xy=(finishPoint, 0),\
		xycoords="data", xytext=(finishPoint - 5, 100),\
		textcoords="data", size=10, ha="center",\
		bbox=dict(boxstyle="round4,pad=.5", fc="0.9"),\
		arrowprops=dict(arrowstyle="->", connectionstyle="angle,angleA=0,angleB=-90,rad=10"));
savePlot(plotlabel);





# Hadoop performance with Slowstart=1
plt.figure(figsize=(12.21,5.10))
#plt.subplot2grid((2,4), (1, 1), colspan=1)
plt.tick_params(\
    axis='x',
    which='both',
    bottom='on',
    top='off',
    labelbottom='on')
data=np.loadtxt("slowstart_1.0.plotdata", skiprows=1, delimiter=" ")
plotlabel="slowstart_1.0"
maxSend=max(data[:,1]) / 1000000;
minSend=min(data[:,1]) / 1000000;
maxReceive=max(data[:,2]) / 1000000;
minReceive=min(data[:,2]) / 1000000;
maximum=max(maxSend,maxReceive);
minimum=min(minSend,minReceive);
finishPoint=max(data[:,0])/60.0;
plt.title("(d)");
plt.text(49, 120, "Slowstart = 1.0 , Datasize= 10GB per node\nMax network traffic="+str(maximum)+", Min network traffic="+str(minimum),\
	horizontalalignment='right',verticalalignment='center', color=(0,0,0), weight='bold', size=10)
plt.xlabel("Execution Time (Minutes)");
plt.ylabel("Network I/O (MB/s)");
plt.xticks(np.arange(0, 55, 5))
#plt.grid(True);
plt.xlim((0,50));
plt.ylim((0,128));
plt.plot( data[:,0]/60, data[:,1]/1000000, color=(0.3, 0.3, 0.3));
plt.plot( data[:,0]/60, data[:,2]/1000000, color=(0.3, 0.3, 0.3));
plt.annotate("Completed\n"+str(('%.2f' % (finishPoint,))), xy=(finishPoint, 0),\
		xycoords="data", xytext=(finishPoint - 5, 100),\
		textcoords="data", size=10, ha="center",\
		bbox=dict(boxstyle="round4,pad=.5", fc="0.9"),\
		arrowprops=dict(arrowstyle="->", connectionstyle="angle,angleA=0,angleB=-90,rad=10"));
savePlot(plotlabel);




# Network traffic with compression
plt.figure(figsize=(12.21,5.10))
#plt.subplot2grid((2,4), (0, 2), colspan=1)
plt.tick_params(\
    axis='y',
    which='both',
    left='off',
    top='on',
    labelleft='off')
data=np.loadtxt("with_compression.plotdata", skiprows=1, delimiter=" ")
plotLabel="with_compression"
maxNet=max(data[:,1]) / 1000000;
finishPoint=max(data[:,0])/60.0;
plt.title("(e)");
plt.text(17, 120, "Slowstart = 0.0 , Datasize= 10GB per node , Compression= ON",\
	horizontalalignment='right',verticalalignment='center', color=(0,0,0), weight='bold', size=10)
plt.xlabel("Execution Time (Minutes)");
plt.ylabel("Network I/O (MB/s)");
plt.xticks(np.arange(0, 20, 3))
#plt.grid(True);
plt.xlim((0,18));
plt.ylim((0,128));
plt.plot( data[:,0]/60, data[:,1]/1000000, color=(0.3, 0.3, 0.3));
plt.plot( data[:,0]/60, data[:,2]/1000000, color=(0.3, 0.3, 0.3));
plt.annotate("Completed\n"+str(('%.2f' % (finishPoint,))), xy=(finishPoint, 0),\
		xycoords="data", xytext=(finishPoint - 5, 100),\
		textcoords="data", size=10, ha="center",\
		bbox=dict(boxstyle="round4,pad=.5", fc="0.9"),\
		arrowprops=dict(arrowstyle="->", connectionstyle="angle,angleA=0,angleB=-90,rad=10"));
savePlot(plotLabel);





# Network traffic without compression
plt.figure(figsize=(12.21,5.10))
#plt.subplot2grid((2,4), (1, 2), colspan=1)
plt.tick_params(\
    axis='y',
    which='both',
    left='off',
    top='on',
    labelleft='off')
data=np.loadtxt("without_compression.plotdata", skiprows=1, delimiter=" ")
plotLabel="without_compression"
maxNet=max(data[:,1]) / 1000000;
finishPoint=max(data[:,0])/60.0;
plt.title("(f)");
plt.text(17, 120, "Slowstart = 0.0 , Datasize= 10GB per node , Compression= OFF",\
	horizontalalignment='right',verticalalignment='center', color=(0,0,0), weight='bold', size=10)
plt.xlabel("Execution Time (Minutes)");
plt.ylabel("Network I/O (MB/s)");
plt.xticks(np.arange(0, 20, 3))
#plt.grid(True);
plt.xlim((0,18));
plt.ylim((0,128));
plt.plot( data[:,0]/60, data[:,1]/1000000, color=(0.3, 0.3, 0.3));
plt.plot( data[:,0]/60, data[:,2]/1000000, color=(0.3, 0.3, 0.3));
plt.annotate("Completed\n"+str(('%.2f' % (finishPoint,))), xy=(finishPoint, 0),\
		xycoords="data", xytext=(finishPoint - 5, 100),\
		textcoords="data", size=10, ha="center",\
		bbox=dict(boxstyle="round4,pad=.5", fc="0.9"),\
		arrowprops=dict(arrowstyle="->", connectionstyle="angle,angleA=0,angleB=-90,rad=10"));
savePlot(plotLabel);




"""
#Impact of compression on disk I/O
plt.figure(figsize=(12.21,5.10))
#plt.subplot2grid((2,4), (0, 3), rowspan=2)
plt.tick_params(\
    axis='x',
    which='both',
    bottom='on',
    top='on',
    labelbottom='on')
data=np.loadtxt("compression_disk_io.plotdata", skiprows=1, delimiter=" ")
plotLabel="compression_disk_io"
plt.title("(g)");
#plt.title("(g)\n10GB per node");
#plt.xlabel("Compression");
plt.ylabel("Total disk access (GB)");
plt.xticks([1.5,4.5],['Read from disk','Write to disk'])
#plt.grid(True);
plt.xlim(0,6);
#plt.ylim(200, 400);
"""
plt.text(1, 50, "Compression OFF", horizontalalignment='center',verticalalignment='bottom', rotation='vertical', color=(1,1,1), weight='bold')
plt.text(2, 50, "Compression ON", horizontalalignment='center',verticalalignment='bottom', rotation='vertical', color=(1,1,1), weight='bold')
plt.text(4, 50, "Compression OFF", horizontalalignment='center',verticalalignment='bottom', rotation='vertical', color=(1,1,1), weight='bold')
plt.text(5, 50, "Compression ON", horizontalalignment='center',verticalalignment='bottom', rotation='vertical', color=(1,1,1), weight='bold')
"""
plt.bar(data[:,0]-0.4, ((data[:,1]*512)/1073741824), 0.8, color=(0.7, 0.7, 0.7), label='Compression Off');
plt.bar(data[:,2]-0.4, ((data[:,3]*512)/1073741824), 0.8, color=(0.3, 0.3, 0.3), label='Compression ON');
plt.legend(loc='upper left', shadow=False)
savePlot(plotLabel);
"""


#Impact of compression on disk I/O
plt.figure(figsize=(12.21,5.10))
#plt.subplot2grid((2,4), (0, 3), rowspan=2)
plt.tick_params(\
    axis='x',
    which='both',
    bottom='on',
    top='on',
    labelbottom='on')
data=np.loadtxt("compression_disk_io_slowstart.plotdata", skiprows=1, delimiter=" ")
plotLabel="compression_disk_io_slowstart"
plt.title("(g)");
#plt.title("(g)\n10GB per node");
plt.xlabel("Slowstart");
plt.ylabel("Total disk access (GB)");
#plt.xticks([1.5,4.5],['Read from disk','Write to disk'])
plt.xticks(np.arange(-0.25, 1.25, 0.25))
#plt.grid(True);
plt.xlim(-0.1,1.1);
#plt.ylim(200, 400);
plt.bar(data[:,0]-0.04, ((data[:,1]*512)/1073741824), 0.04, color=(0.7, 0.7, 0.7), label='Compression Off');
plt.bar(data[:,0], ((data[:,2]*512)/1073741824), 0.04, color=(0.3, 0.3, 0.3), label='Compression ON');
plt.legend(loc='upper left', shadow=False)
savePlot(plotLabel);



