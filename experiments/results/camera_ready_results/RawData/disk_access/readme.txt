Here you can see the (actual) amount of data read from disk during execution.

First column:Value of SLOWSTART parameter
Second column: Bytes read when COMPRESSION was OFF
Third column: Bytes read when COMPRESSION was ON