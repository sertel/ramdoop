Here you can see the execution time of each run in Minutes.

First column: value of SLOWSTART parameter
Second column: Execution time when COMPRESSION was OFF
Third column: Execution time when COMPRESSION was ON