#!/bin/bash
source /opt/siavash/hadoop/HiBench/prepare.sh
windowWidth=70;

###################################################################################
###                       PARAMETERES, DIRECTORIES, FILES                       ###
###################################################################################
whichTest=sort;						### wordcount|sort|pagerank ###
howManyTimes=1;						###   ANY NUMBER OF TRIES   ###
delayBetweenTries=60;					###  ANY NUMBER OF SECONDS  ###
slowStartFrom=0						### ANY NUMBER FROM 0 to 100###
slowStartTo=100						### ANY NUMBER FROM 0 to 100###
drawPlots=false;					###        true | false     ###
usingRamdoop=true;					###        true | false     ###
flushRedis=true;                                        ###        true | false     ###
mainDir=/opt/siavash;					###  THIS IS THE MAIN DIR.  ###
hadoopMainDir=$mainDir/hadoop/hadoop-1.2.1;		###  HADOOP MAIN DIRECTORY  ###
hibenchMainDir=$mainDir/hadoop/HiBench;			###  HIBENCH MAIN DIRECTORY ###
toolsDir=$mainDir/testtools;				###   TOOLS MAIN DIRECTORY  ###

################### NO NEED TO CHANGE ANYTHING BELOW THIS LINE ####################

mainLogDir=$mainDir/LOGS;				### <<< WILL BE CREATED >>> ###
dumpDir=$mainLogDir/DUMPS;				### <<< WILL BE CREATED >>> ###
tempDir=$dumpDir/TEMP;					### <<< WILL BE CREATED >>> ###
resultDir=$dumpDir/RESULT;				### <<< WILL BE CREATED >>> ###
runLog=$dumpDir/run.log;				### <<< WILL BE CREATED >>> ###
plotDir=$mainLogDir/FINAL/$(date +"%Y%m%d%H%M%S");	### <<< WILL BE CREATED >>> ###


### Calculating the 'Slow start' parameter for each iteration
if [ "$howManyTimes" -gt "1" ]; then
	step=$(( (slowStartTo - slowStartFrom) / (howManyTimes - 1) ));
else
	step=0;
fi
slowStartParam=$(seq -f %02g $slowStartFrom $slowStartFrom);

###################################################################################
###                             FLUSHING REDIS                                  ###
###################################################################################
if $usingRamdoop;
then
	source /opt/siavash/hadoop/HiBench/enable_ramdoop.sh
else
	source /opt/siavash/hadoop/Hibench/disable_ramdoop.sh
fi


dataIsReady=1;

### Starting Iteration
for (( itter = 1; itter <= howManyTimes; itter++ ))
do
	clear;
	finalDir=$resultDir/$(date +"%Y%m%d%H%M%S");	### <<< WILL BE CREATED >>> ###

	###################################################################################
	###                             MAKING DIRECTORIES                              ###
	###################################################################################
	mkdir -p $tempDir;
	mkdir -p $finalDir;
	rm -f $mainDir/runtime.*

        ###################################################################################
        ###                           PRINTING RAMDOOP STATUS                           ###
        ###################################################################################
        message="Status of RUN_RAMDOOP is";     printf "$message";
        tput cuf $(( windowWidth - ${#message} )); echo $RUN_RAMDOOP;

	###################################################################################
	###                         STOPING TCPDUMP ON ALL NODES                        ###
	###################################################################################
	message="Stopping TCPDump agent on nodes... "; printf "$message";
	for dest in $(<$hadoopMainDir/conf/slaves)
	do
	  nul=$(ssh $dest 'sudo killall tcpdump' 2>&1)
	done
	tput cuf $(( windowWidth - ${#message} )); echo "Done";

	###################################################################################
	###                         STOPPING HADOOP ON ALL NODES                        ###
	###################################################################################
	message="Stopping HADOOP if its still running... ";	printf "$message";
	nul=$(stop-all.sh)
	tput cuf $(( windowWidth - ${#message} )); echo "Done";

	###################################################################################
	###                   COPY HADOOP CONFIGURATION TO OTHER NODES                  ###
	###################################################################################
		
	lineNo=$(grep -n mapred.reduce.slowstart.completed.maps $hadoopMainDir/conf/mapred-site.xml | awk -F":" '{print $1}');
	### Adding mapred.reduce.slowstart.completed.maps to mapred-site.xml
	if [[ $lineNo == "" ]]; then
		sed -i 's/<\/configuration>/<property>  \n<name>mapred.reduce.slowstart.completed.maps<\/name>\
			\n<value>0.00<\/value>\n<\/property>\n<\/configuration>/' $hadoopMainDir/conf/mapred-site.xml
		lineNo=$(grep -n mapred.reduce.slowstart.completed.maps $hadoopMainDir/conf/mapred-site.xml | awk -F":" '{print $1}');
	fi

	if [ "$slowStartParam" -gt "99" ]; then
		slowStartParam=100;
		sed -i $(( lineNo + 1 ))'s/<value>.\...<\/value>/<value>1.00<\/value>/' $hadoopMainDir/conf/mapred-site.xml
	elif [ "$slowStartParam" -lt "00" ]; then
		slowStartParam=00;
		sed -i $(( lineNo + 1 ))'s/<value>.\...<\/value>/<value>0.00<\/value>/' $hadoopMainDir/conf/mapred-site.xml
	else
		sed -i $(( lineNo + 1 ))'s/<value>.\...<\/value>/<value>0.'$slowStartParam'<\/value>/' $hadoopMainDir/conf/mapred-site.xml
	fi
	
	lineNo=$(( lineNo + 1 ));
	message="Hadoop will run with [slowstart] set to: "; printf "$message";
	tput cuf $(( windowWidth - ${#message} ));
	sed -n "${lineNo}p" $hadoopMainDir/conf/mapred-site.xml | sed "s/<value>//" | sed "s/<\/value>//" | sed "s/ //g"

	message="Copy configuration files on each node... "; printf "$message";
	for dest in $(<$hadoopMainDir/conf/slaves)
	do
	  nul=$(ssh $dest "mkdir -p $toolsDir");
	  nul=$(scp $hadoopMainDir/conf/core-site.xml ${dest}:$hadoopMainDir/conf/) &
	  nul=$(scp $hadoopMainDir/conf/mapred-site.xml ${dest}:$hadoopMainDir/conf/) &
	  nul=$(scp $hadoopMainDir/conf/hdfs-site.xml ${dest}:$hadoopMainDir/conf/) &
	  nul=$(scp $hadoopMainDir/conf/hadoop-env.sh ${dest}:$hadoopMainDir/conf/) &
	  nul=$(scp $hadoopMainDir/conf/masters ${dest}:$hadoopMainDir/conf/) &
	  nul=$(scp $hadoopMainDir/conf/slaves ${dest}:$hadoopMainDir/conf/) &
	  nul=$(scp $toolsDir/preprocess.sh ${dest}:/$toolsDir/) &
	  nul=$(scp $toolsDir/tcpdump ${dest}:/$toolsDir/) &
	  wait;
	  nul=$(ssh $dest "chmod +x $toolsDir/preprocess.sh")
	  nul=$(ssh $dest "chmod +x $toolsDir/tcpdump")
	done
	tput cuf $(( windowWidth - ${#message} )); echo "Done";
	
	###################################################################################
	###                         STARTING TCPDUMP ON ALL NODES                       ###
	###################################################################################
	message="Starting TcpDump agent on each node... "; printf "$message";
	for dest in $(<$hadoopMainDir/conf/slaves)
	do
	  ### Some parameters in the command below are hard coded! based on experiences, technical difficulties.
	  nul=$(ssh $dest "mkdir -p $mainLogDir; cd $toolsDir;\
			sudo ./tcpdump -i eth0 -n -nn -tt -q tcp and not udp and net\
			141.76.50 and not net 141.30 and not net\
			141.76.50.80 and not port 22 and not port 13531 > $mainLogDir/tcpdump.log;" 2>&1) &
	done
	tput cuf $(( windowWidth - ${#message} )); echo "Done";
	
	###################################################################################
	###                           STARTING HADOOP & LOGGING                         ###
	###################################################################################
	message="Starting Hadoop... "; printf "$message";
	echo $(date) > $runLog;
	nul=$(start-all.sh 2>&1);
	tput cuf $(( windowWidth - ${#message} )); echo "Done";

	###################################################################################
	###                     WAITING FOR HADOOP TO LEAVE SAFE_MODE                   ###
	###################################################################################
	message="Hadoop is in safemode! waiting."; printf "$message";
	while [[ $(hadoop dfsadmin -safemode get) != *"Safe mode is OFF"  ]]
	do
		message=$message".";
		printf ".";
		sleep 5;
	done
	tput cuf $(( windowWidth - ${#message} )); echo "Ready!";

        ###################################################################################
        ###                             FLUSHING REDIS                                  ###
        ###################################################################################
        if $usingRamdoop && $flushRedis;
        then
                message="Flushing redis... "; printf "$message";
                nul=$($hibenchMainDir/sweep_redis.sh);
                tput cuf $(( windowWidth - ${#message} )); echo "Done";
        fi

	###################################################################################
	###                             RUNNING HiBench TESTS                           ###
	###################################################################################
	#Preparing data for first time
	if [ $dataIsReady -eq 0 ]; then	
		printf "[check $runLog.prepare for more details]\n";
		message="Prepairing data for hadoop... "; printf "$message";
			$hibenchMainDir/$whichTest/bin/prepare.sh > $runLog.prepare 2>&1
		tput cuf $(( windowWidth - ${#message} )); echo "Done";

		#redo all steps to keep all tests uniform
		dataIsReady=1;
		itter=$(( itter-1 ));
		rm -rf $finalDir;
		continue;
	fi
		
	### Empty cach buffer and Get vmstat from each node
	message="Getting VMSTAT from nodes [Before]..."; printf "$message";
	for dest in $(<$hadoopMainDir/conf/slaves)
	do
		ssh $dest "echo 3 | sudo tee /proc/sys/vm/drop_caches > /dev/null"
		ssh $dest "vmstat -D" > $tempDir/$dest.vmstat.before.log
	done
	tput cuf $(( windowWidth - ${#message} )); echo "Done";
		
	### Running main Hadoop job
	printf "[check $runLog for more details]\n";
	message="Running main hadoop job..."; printf "$message";

	startTime=$(date +"%H:%M:%S");
	echo -e "JOB SUBMITTED $startTime" >> $runLog

	$hibenchMainDir/$whichTest/bin/run.sh 2>&1 | tee >> $runLog

	stopTime=$(date +"%H:%M:%S");
	echo -e "JOB COMPLETED $stopTime" >> $runLog

	echo -e "0 $whichTest $startTime $stopTime" >> $tempDir/timestamp.log;

#	/usr/bin/time -v --output=$tempDir/time.log $hibenchMainDir/$whichTest/bin/run.sh 2>&1 | tee $runLog | $toolsDir/memCheck.sh > $finalDir/$itter.memCheck.log
	tput cuf $(( windowWidth - ${#message} )); echo "Done";

	### Get vmstat from each node after successful run
	message="Getting VMSTAT from nodes [After]... "; printf "$message";
	for dest in $(<$hadoopMainDir/conf/slaves)
	do
		ssh $dest "vmstat -D" > $tempDir/$dest.vmstat.after.log
	done
	tput cuf $(( windowWidth - ${#message} )); echo "Done";

	###################################################################################
	###                         STOPPING HADOOP ON ALL NODES                        ###
	###################################################################################
	message="Stopping Hadoop... "; printf "$message";
	nul=$(stop-all.sh 2>&1);
	tput cuf $(( windowWidth - ${#message} )); echo "Done";


	###################################################################################
	###                         STOPING TCPDUMP ON ALL NODES                        ###
	###################################################################################
	message="Stopping TcpDump agent on nodes... "; printf "$message";
	for dest in $(<$hadoopMainDir/conf/slaves); do
	  nul=$(ssh $dest 'sudo killall tcpdump' 2>&1) &
	done
	wait;
	tput cuf $(( windowWidth - ${#message} )); echo "Done";

	###################################################################################
	###                                 LOCAL VARIABLES                             ###
	###################################################################################
	echo "*****************************************"
	echo "*****          Parsing logs         *****"
	echo "*****************************************"

<<'END'
	testNo=0;
	startTime=0;
	stopTime=0;
	###################################################################################
	###                     READING $runlog FOR EXTRACTING TIMESTAPS                ###
	###################################################################################
	while read line
	do
		if [[ $line == '========== running'*'bench ==========' ]]
		then
			###STARTING POINT OF A NEW TEST DETECTED
			testNo=$(( $testNo + 1 ));
			testName=${line/========== running };
			testName=${testName/ bench ==========};
		        startTime=0;
		        stopTime=0;
			echo $line
		fi

		if [[ $line == *'Running job'* && $startTime == '0' ]]
		then
			startTime=${line/ INFO*};
			startTime=${startTime/* };
		fi
			
		if [[ $line == *'Job complete'* ]]
		then
			stopTime=${line/ INFO*};
			stopTime=${stopTime/* };
			echo -e "$testNo $testName $startTime $stopTime" >> $tempDir/timestamp.log;
		fi	
	done < $runLog

	if [[ $stopTime == "0" ]]; then
		echo $(date) " Failed: $itter" >> $mainLogDir/permanent.fail
		itter=$(( itter-1 ));
		continue;
	fi
END

	###################################################################################
	###         SPLITTING LOGS INTO SMALLER CHUNKS BASED ON SEND/RECEIVE            ###
	###################################################################################
	message="Splitting log files on each node... "; printf "$message";
	while read node
	do
#		nul=$(ssh $node "cd $toolsDir; ./preprocess.sh" 2>&1) &
		ssh $node "cd $toolsDir; ./preprocess.sh" 2>&1
	done < $hadoopMainDir/conf/slaves
	wait;
	tput cuf $(( windowWidth - ${#message} )); echo "Done";

	###################################################################################
	###                     GATHERING ALL RESULTS FROM TCPDUMP AGENTS               ###
	###################################################################################
	message="Fetching log files from other nodes... "; printf "$message";
	for dest in $(<$hadoopMainDir/conf/slaves); do
	  nul=$(scp $dest:$mainLogDir/*.red $tempDir/ 2>&1) &
	done
	wait;
	tput cuf $(( windowWidth - ${#message} )); echo "Done";

	cd $tempDir;

	from=$(head -n 1 $tempDir/timestamp.log | awk '{print $3}');
	to=$(tail -n 1 $tempDir/timestamp.log | awk '{print $4}');

	message="Joining files first phase... $from - $to "; printf "$message";
	rm -f $tempDir/counter;
	i=0;
	for seconds in $(seq $(date -d "$from" +"%s") $(date -d "$to" +"%s"))
	do
		echo -e "$seconds $i" >> $tempDir/counter;
		i=$(( $i + 1 ));
	done

	for files in $(ls $tempDir/*.red)
	do
		tempTime=$(head -n 1 $files);
		tempTime=${tempTime/ *};
		awk -v p=$(( $tempTime - 1 )) '($1!=p+1){for(i=p+1; i<=$1-1; ++i)\
			print i " " 0; print i " " $2} ($1==p+1){print $1 " " $2}\
			{p=$1}' $files > $files.ready &
	done
	wait;
	tput cuf $(( windowWidth - ${#message} )); echo "Done";

	message="Joining files second phase [send]... "; printf "$message";
	joinCommand="";
	for files in $(ls $tempDir/*.snd.red.ready | sort)
	do
		if [[ $joinCommand == "" ]]; then
			joinCommand="join -1 1 -2 1 -a1 counter $files";
		else
			joinCommand=$joinCommand' | join - '$files;
		fi
	done
	if [[ $joinCommand != "" ]]; then
		eval $joinCommand > $tempDir/send.total;
	fi
	tput cuf $(( windowWidth - ${#message} )); echo "Done";

	message="Joining files second phase [receive]... "; printf "$message";
	joinCommand="";
	for files in $(ls $tempDir/*.rcv.red.ready | sort)
	do
			if [[ $joinCommand == "" ]]; then
					joinCommand="join -1 1 -2 1 -a1 counter $files";
			else
					joinCommand=$joinCommand' | join - '$files;
			fi
	done
	if [[ $joinCommand != "" ]]; then
			eval $joinCommand > $tempDir/receive.total;
	fi
	tput cuf $(( windowWidth - ${#message} )); echo "Done";

	message="Finalizing... "; printf "$message";
	printf "Timestamp Second " > $finalDir/Received.log;
	for files in $(ls $tempDir/*.rcv.red.ready | sort)
	do
		files=${files/*\/};
		printf "${files/.rcv*} " >> $finalDir/Received.log;
	done
	echo "" >> $finalDir/Received.log;
	awk '{print $0}' $tempDir/receive.total >> $finalDir/Received.log;
	sed -i 's/ /\t/g' $finalDir/Received.log;


	printf "Timestamp Second " > $finalDir/Sent.log;
	for files in $(ls $tempDir/*.snd.red.ready | sort)
	do
		files=${files/*\/};
			printf "${files/.snd*} " >> $finalDir/Sent.log;
	done
	echo "" >> $finalDir/Sent.log;
	awk '{print $0}' $tempDir/send.total >> $finalDir/Sent.log;
	sed -i 's/ /\t/g' $finalDir/Sent.log;

	###################################################################################
	###                            GENERATING PLOTS DATA                            ###
	###################################################################################
	mkdir -p $plotDir;
	
	echo "Second Byte" > $plotDir/$itter.Sent.plot
	awk  'NR>1{a=0;for(i=3;i<=NF;i++)if($i>a)a=$i;print $2" "a}' $finalDir/Sent.log >> $plotDir/$itter.Sent.plot
	
	echo "Second Byte" > $plotDir/$itter.Received.plot
	awk  'NR>1{a=0;for(i=3;i<=NF;i++)if($i>a)a=$i;print $2" "a}' $finalDir/Received.log >> $plotDir/$itter.Received.plot
	

	###################################################################################
	###                         CALCULATING DISK I/O AMOUNT                         ###
	###################################################################################
	readNo=$(paste  <(cat $tempDir/stream*.vmstat.before.log | grep "read sectors" | awk '{print $1}')\
		        <(cat $tempDir/stream*.vmstat.after.log  | grep "read sectors" | awk '{print $1}')\
		        | awk '{sum=sum+($2-$1)} END{print sum}'); 

	writeNo=$(paste <(cat $tempDir/stream*.vmstat.before.log | grep "written sectors" | awk '{print $1}')\
		        <(cat $tempDir/stream*.vmstat.after.log  | grep "written sectors" | awk '{print $1}')\
		        | awk '{sum=sum+($2-$1)} END{print sum}'); 

	echo "0 $slowStartParam $readNo $writeNo" > $plotDir/$itter.io.log


	###################################################################################
	###                      COLLECTING MORE DATA ABOUT EACH RUN                    ###
	###################################################################################
	mv $tempDir/timestamp.log $finalDir/;
#	mv $tempDir/time.log $finalDir/;
	mv $tempDir/*.vmstat.*.log $finalDir/;
	mkdir -p $finalDir/trash;
	mv $tempDir/* $finalDir/trash/.
	rm -f $tempDir/*;
	cp $runLog $finalDir/;
	cp $runLog $plotDir/$itter.runtime.log;
	cp -r $hadoopMainDir/conf $finalDir;
	mkdir $finalDir/HiBenchConf;
	cp $hibenchMainDir/$whichTest/bin/prepare.sh $finalDir/HiBenchConf/$whichTest-prepare.sh;
	cp $hibenchMainDir/$whichTest/bin/run.sh $finalDir/HiBenchConf/$whichTest-run.sh;
	cp $hibenchMainDir/$whichTest/conf/configure.sh $finalDir/HiBenchConf/$whichTest-configure.sh;

	mkdir -p $plotDir/temp
	mv $finalDir $plotDir/temp/

	tput cuf $(( windowWidth - ${#message} )); echo "Done";
	if [ "$itter" -lt "$howManyTimes" ]; then
		sleep $delayBetweenTries;
	fi
	
	slowStartParam=$(( slowStartParam + step ));
	slowStartParam=$(seq -f %02g $slowStartParam $slowStartParam);
done


###################################################################################
###                      GENERATING DISKS DATA [Read,Write]                     ###
###################################################################################

cd $plotDir;
echo -e "Node SlowStart MinorPF MajorPF" > io.ttl.plot;
cat $(ls *.io.log | sort -n) >> io.ttl.plot;


###################################################################################
###                             CREATING PLOT GENERATOR                         ###
###################################################################################

if $usingRamdoop
then
	platName="Ramdoop"
else
	platName="Hadoop"
fi

echo "
whichTest=$whichTest;
platName=$platName;
howManyTimes=$howManyTimes;
slowStartFrom=$slowStartFrom;
slowStartTo=$slowStartTo;
step=$step;
cd $plotDir;

" > $plotDir/plotter.sh;

echo '
maxFinishingTime=0;
for (( i=1; i<=howManyTimes; i++ ))
do
        if [ ! -e $i.Sent.plot ]; then continue;  fi
        if [ "$(wc -l < $i.Sent.plot)" -lt "2"  ]; then continue;  fi
	finishingTime=$(awk '"'"'END{print $1}'"'"' $i.Sent.plot);
	if [ "$finishingTime" -gt "$maxFinishingTime" ]; then
		maxFinishingTime=$finishingTime;
	fi
done

maxFinishingTime=$(( maxFinishingTime / 60 ));


for (( i = 1; i <= howManyTimes; i++ ))
do
        if [ ! -e $i.Sent.plot ]; then continue;  fi
        if [ "$(wc -l < $i.Sent.plot)" -lt "2"  ]; then continue;  fi
	finishingPoint=$(awk '"'"'END{print $1}'"'"' $i.Sent.plot);
	finishingPoint=$(( finishingPoint / 60 ));

python - <<END
import numpy as np
import matplotlib.pyplot as plt
list_of_files = [ ("$i.Sent.plot", "Sent"), ("$i.Received.plot", "Received")]
datalist = [ ( np.loadtxt(filename, skiprows=1, delimiter=" "), label ) for filename, label in list_of_files ]
plt.figure(figsize=(12.21,5.10))
for data, plotLabel in datalist:
	maxNet=max(data[:,1]) / 1000000;
        plt.title("$whichTest $platName Network Traffic Peak\nSlowstart set to $(awk '"'"'NR==1 {print $2/100}'"'"' $i.io.log)");
        plt.xlabel("Minutes");
        plt.ylabel("Mega bytes");
	plt.xticks(np.arange(0, $maxFinishingTime, $maxFinishingTime / 10))
	plt.grid(True);
	plt.xlim((0,$maxFinishingTime));
	plt.ylim((0,128));
        plt.plot( data[:,0]/60, data[:,1]/1000000);
	plt.plot([0, $finishingPoint], [maxNet, maxNet], color="r", linestyle="-.", linewidth=1);
	plt.text(0, maxNet, plotLabel, fontsize=10, color="r");
	plt.annotate("Completed\n $finishingPoint", xy=($finishingPoint, 0),\
			xycoords="data", xytext=($finishingPoint - ($maxFinishingTime / 10), 120),\
			textcoords="data", size=10, ha="center",\
			bbox=dict(boxstyle="round4,pad=.5", fc="0.9"),\
			arrowprops=dict(arrowstyle="->", connectionstyle="angle,angleA=0,angleB=-90,rad=10"));

        plt.savefig("NetworkTrafficPeak-$i.png",bbox_inches="tight",dpi=100);

print "."

END

done

python - <<END
import numpy as np
import matplotlib.pyplot as plt
list_of_files = [ ("io.ttl.plot", "Read")]
datalist = [ ( np.loadtxt(filename, skiprows=1, delimiter=" "), label ) for filename, label in list_of_files ]
plt.figure(figsize=(12.21,5.10))
for data, plotLabel in datalist:
        plt.title("$whichTest $platName Disk Access (actual)");
        plt.xlabel("Slowstart");
        plt.ylabel("Sectors read (Million)");
	plt.xticks(np.arange($slowStartFrom/100.00, ($slowStartTo+$step)/100.00, $step/100.00))
	plt.grid(True);
	plt.xlim(($slowStartFrom-$step)/100.00, ($slowStartTo+$step)/100.00);
        plt.bar((data[:,1]-($step / 4))/100.00, data[:,2]/1000000, ($step/2)/100.00, color="r");
        plt.savefig("DiskRead.png",bbox_inches="tight",dpi=100);

print "."

END
' >> $plotDir/plotter.sh

chmod +x $plotDir/plotter.sh;

if $drawPlots; then $plotDir/plotter.sh; fi

tput cuf 63;
echo "Finished!";
