RAMdoop speeds up cluster processing of Hadoop using Tiling and RAM-based processing.

--------------------------------------------------------------------------------------------------------------------------------

Installation:
-------------
Please point HADOOP_INSTALL_ROOT to your Hadoop installation.

A good website on how to set up a (local) hadoop cluster is this one:
http://www.michael-noll.com/tutorials/running-hadoop-on-ubuntu-linux-single-node-cluster/

Compilation:
------------
In the root directory of the project call:
ant deploy -DHADOOP_INSTALL_ROOT=/path/to/hadoop/root
To deploy the projects configuration use:
ant deploy-hadoop-config -DHADOOP_INSTALL_ROOT=/path/to/hadoop/root

Wrapper Activation:
-------------------
In order to activate our wrapper and therewith RAMdoop, just do:
export RUN_RAMDOOP=1
Now you can execute your MR program just as you are used to.

Cluster Setup:
--------------
For setup purposes please use the python scripts of the project. Please make sure you specify 
the JAVA_HOME location in hadoop-env.sh accordingly.

Configuration Parameters:
-------------------------
-D ramdoop.input.job.size
The size of a mini job in bytes. The specified tiling strategy will use this parameter.

-D mapred.job.reuse.jvm.num.tasks=-1
Not a RAMdoop parameter but definitely worthwhile setting to reduce the time spent spawning JVMs.

-D mapred.job.reuse.jvm.diff.jobs=true
A parameter that only works when our Hadoop extensions were installed. It will remove the aggressive 
JVM restart behavior of Hadoop across (mini) jobs.

-D ramdoop.output.prefix=http://localhost:637
A string prefix to be used to find the proper Redis instance for a partition. For example, a keys that map to 
partition 1 would be stored in the redis server running at http://localhost:6371

-D ramdoop.partition.diff
This parameter can be used in addition if the identification of the Redis instance does not start at zero. 
This value will be added to the calculated partition before the according Redis instance is identified.

-D ramdoop.tile.reader.batch.size
An optimization parameter to specify the size of the input buffers for the final tiling phase when reading from Redis.

-D ramdoop.tile.intermediate.strategy
A strategy for Redis to implement a deterministic scan. Currently TILE_KEYS (default) and DB_MOVE are supported.

API:
----
The class com.ramdoop.tiled.api.Tiler can be used implement a tiling strategy for the input data. It is passed to
RAMdoop via:
-Dramdoop.input.tiler=<fully_qualified_class_name>
