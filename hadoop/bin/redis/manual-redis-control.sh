#!/bin/bash

###############################################################################################
# Copyright (c) RAMdoop 2013. All Rights Reserved.                                            #
# This source code is licensed under the terms described in the associated LICENSE.TXT file.  #
###############################################################################################

REDIS_DAEMON=false;
VERBOSE=false;
ALL_MUTE=false;

REDIS_VERSION="2.6.16";
REDIS_PACKAGE_PREFIX="redis-";
REDIS_PACKAGE_POSTFIX=".tar.gz";
REDIS_TEMP_DIR=$1;
REDIS_CONF_DIR="$REDIS_TEMP_DIR/conf";
REDIS_INSTALL_DIR="$REDIS_TEMP_DIR/bin";
REDIS_SERVER_NAME="redis-server";
REDIS_CLI_NAME="redis-cli";
REDIS_BENCHMARK_NAME="redis-benchmark";
REDIS_CHECK_DUMP_NAME="redis-check-dump";
REDIS_CHECK_AOF_NAME="redis-check-aof";
REDIS_PACKAGE_NAME=$REDIS_PACKAGE_PREFIX$REDIS_VERSION$REDIS_PACKAGE_POSTFIX;


[ $VERBOSE == false ] && exec 2> /dev/null;

#######################################################
#           Starting/Stopping Redis server            #
#######################################################

if [ "$($REDIS_INSTALL_DIR/$REDIS_CLI_NAME ping)" == "PONG" ]; then
	if [ "$1" == "stop" ]; then
		[ $VERBOSE == true ] && echo "Shuting down Redis server..."
		$REDIS_INSTALL_DIR/$REDIS_CLI_NAME "shutdown"
		while [ "$($REDIS_INSTALL_DIR/$REDIS_CLI_NAME ping)" == "PONG" ]; do
			sleep 1
		done
		[ $ALL_MUTE == false ] && echo "***** Redis stopped successfully! *****"
	else
		[ $ALL_MUTE == false ] && echo "***** Redis is already running. *****"
	fi
else
	if [ "$1" == "stop" ]; then
		[ $ALL_MUTE == false ] && echo "***** Redis is not running *****"
	else
		if [ ! -f $REDIS_CONF_DIR"/redis.conf" ]; then
			[ $VERBOSE == true ] && echo "Configuration file is missing!"
			cd $REDIS_TEMP_DIR/$REDIS_PACKAGE_PREFIX$REDIS_VERSION;
			if [ -f "redis.conf" ]; then
				if $REDIS_DAEMON; then
					find . -type f -name \redis.conf -exec sed -i.bak 's|daemonize no|daemonize yes|g' {} +
				else
					find . -type f -name \redis.conf -exec sed -i.bak 's|daemonize yes|daemonize no|g' {} +
				fi
				cp redis.conf $REDIS_CONF_DIR
				[ $VERBOSE == true ] && echo "Original configuration file will be used"
			fi
		fi

		[ -f /etc/sysconfig/redis ] && . /etc/sysconfig/redis
		[ -x $REDIS_INSTALL_DIR"/redis-server" ] || exit 5
		[ -f $REDIS_CONF_DIR"/redis.conf" ] || exit 6

		[ $VERBOSE == true ] && echo "Starting "$REDIS_PACKAGE_PREFIX$REDIS_VERSION
		$REDIS_INSTALL_DIR"/redis-server" $REDIS_CONF_DIR"/redis.conf"

		while [ "$($REDIS_INSTALL_DIR/$REDIS_CLI_NAME ping)" != "PONG" ]; do
			sleep 1
		done
		[ $ALL_MUTE == false ] && echo "***** Redis started successfully! *****"
	fi
fi