#!/bin/bash

###############################################################################################
# Copyright (c) RAMdoop 2013. All Rights Reserved.                                            #
# This source code is licensed under the terms described in the associated LICENSE.TXT file.  #
###############################################################################################

REDIS_DAEMON=false;
VERBOSE=false;
ALL_MUTE=false;

REDIS_VERSION="2.6.16";
REDIS_PACKAGE_PREFIX="redis-";
REDIS_PACKAGE_POSTFIX=".tar.gz";
REDIS_TEMP_DIR=$1;
REDIS_CONF_DIR="$REDIS_TEMP_DIR/conf";
REDIS_INSTALL_DIR="$REDIS_TEMP_DIR/bin";
REDIS_SERVER_NAME="redis-server";
REDIS_CLI_NAME="redis-cli";
REDIS_BENCHMARK_NAME="redis-benchmark";
REDIS_CHECK_DUMP_NAME="redis-check-dump";
REDIS_CHECK_AOF_NAME="redis-check-aof";
REDIS_PACKAGE_NAME=$REDIS_PACKAGE_PREFIX$REDIS_VERSION$REDIS_PACKAGE_POSTFIX;


[ $VERBOSE == false ] && exec 2> /dev/null;

#######################################################
#         Checking Redis: installed or not            #
#######################################################

REDIS_INSTALLED=false;
if [[ 	-f $REDIS_INSTALL_DIR/$REDIS_SERVER_NAME && \
	-f $REDIS_INSTALL_DIR/$REDIS_CLI_NAME && \
	-f $REDIS_INSTALL_DIR/$REDIS_BENCHMARK_NAME && \
	-f $REDIS_INSTALL_DIR/$REDIS_CHECK_DUMP_NAME && \
	-f $REDIS_INSTALL_DIR/$REDIS_CHECK_AOF_NAME ]];
then
	[ $VERBOSE == true ] && echo "Redis exists! skipping installation phase."
	REDIS_INSTALLED=true;
else
	[ $VERBOSE == true ] && echo "Redis does not exist! Installation is in progress..."
fi

#######################################################
#         Downloading and installing Redis            #
#######################################################

if ! $REDIS_INSTALLED; then
	mkdir -p $REDIS_TEMP_DIR $REDIS_INSTALL_DIR $REDIS_CONF_DIR;
	cd $REDIS_TEMP_DIR;
	while [ ! -f $REDIS_PACKAGE_NAME ]; do
		wget "http://download.redis.io/releases/"$REDIS_PACKAGE_NAME
		sleep 5
	done
	tar xzvf $REDIS_PACKAGE_NAME
	cd $REDIS_PACKAGE_PREFIX$REDIS_VERSION
	make
	cp src/$REDIS_SERVER_NAME $REDIS_INSTALL_DIR
	cp src/$REDIS_CLI_NAME $REDIS_INSTALL_DIR
	cp src/$REDIS_BENCHMARK_NAME $REDIS_INSTALL_DIR
	cp src/$REDIS_CHECK_DUMP_NAME $REDIS_INSTALL_DIR
	cp src/$REDIS_CHECK_AOF_NAME $REDIS_INSTALL_DIR

	if $REDIS_DAEMON; then
		find . -type f -name \redis.conf -exec sed -i.bak 's|daemonize no|daemonize yes|g' {} +
	else
		find . -type f -name \redis.conf -exec sed -i.bak 's|daemonize yes|daemonize no|g' {} +
	fi
	cp redis.conf $REDIS_CONF_DIR
	chmod +x $REDIS_INSTALL_DIR"/redis-server"
fi
