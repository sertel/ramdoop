\section{Classic Map-Reduce}
\label{sec:mr}
The MapReduce (MR) programming model originates from the functional programming language LISP and was introduce into cluster processing by Google~\cite{Dean:2004:MSD:1251254.1251264}. 
The simple programming model encompasses a \texttt{map} and a \texttt{reduce}. 
In LISP a \texttt{map} function applies an operation against each item of a collection (potentially in parallel) while \texttt{reduce} performs an aggregation on the collection and returned a single value.
While LISP's data structure(s) reside in memory, MR targets massive amounts of potentially unstructured data. 
This section starts with a short introduction into the system architecture of MR and the resulting semantic difference in comparison to its LISP predecessors. 
The details of the MR framework are vital to understand to identify I/O hotspots and understand our optimizations to the computational model in \SYS{}.

\begin{figure}[t]
\centering
\includegraphics[scale=0.45]{pictures/mr-architecture.pdf}
\caption{System Architecture of Hadoop MapReduce.}
\label{mr-architecture}
\end{figure}


\subsection{System Architecture}
The input data over which an MR \emph{job} is defined resides in a distributed file system (DFS). 
Since MR clusters were originally build from commodity hardware, the DFS uses replication to tolerate server crashes. 
The more important aspect for data processing is that the file system already spreads data evenly across the nodes in the cluster on import to achieve a balanced computation later during execution.
Therefore, the DFS already splits the data into small \emph{chunks} (mostly at the size of 64~MB).
In the realm of an MR job, these are referred to as \emph{input splits}.
In addition to an input data reference, an MR job specification consists of the \texttt{map} and the \texttt{reduce} function. 
On execution, the MR framework creates \emph{tasks} which are defined over a data chunk and the according function. A \emph{map task}, or \emph{mapper} for short, is assigned a chunk of input data and the \texttt{map} function. 
At this point the MR notion slightly diverges from the LISP implementation because the input data might be unstructured. 
Respectively, the intention of the map task is not only to apply a function to each of the chunks but also to structure the data. 
Once the data is structured, it can is grouped, sorted and aggregated.
A worker that is assigned a map task, reads the data preferably from local disk, breaks it into \emph{records} and executes the map function on these records. 
The emitted key-value pairs are \texttt{partition}ed according to a predefined partitioning scheme and \texttt{sort}ed before the system stores them to local disk. 
The job specification can define an optional \texttt{combine} step for the intermediate results in order to perform a first data reduction.
Once all mappers have been executed, the framework creates \emph{reduce tasks}, a.k.a. \emph{reducers}, by assigning a key partition along with the \texttt{reduce} function to a worker. 
A reducer fetches all key-value pairs belonging to its partition from the remote nodes to \texttt{merge} and \texttt{sort} them.
Finally, it executes the \texttt{reduce} function over all values of a single key and writes the output back to the distributed file system. 
Since the Map-Reduce model requires that all mappers are done before reducers can start their work, it is common to distinguish a \emph{map phase} and \emph{reduce phase} in job execution.
%A key benefit of the MR framework is its transparent support for fault tolerant processing. If a map task fails, the mapper is restarted on another node that stores a replica of the same chunk.

%\begin{figure}[t]
%\centering
%\includegraphics[width=1.72in]{plotting/slowstart_runtime.pdf}
%\includegraphics[width=1.72in]{plotting/slowstart_disk_io.pdf}
%\caption{Impact of the \texttt{slow start} parameter on performance and disk I/O in the shuffle phase (lower is better).}
%\se{The point as to why we can not use sort here is that it is penalized by writing out the results which is an aspect that exhibits large differences but we are not interested in. We are investigating the disk-IO on the map-side as well as the network I/O for the shuffle phase. Therefore, we have to make sure that no combine happens!}
%\label{slow-start-disk-io}
%\end{figure}

\begin{figure}[t]
\centering
\includegraphics[width=3in]{plotting/motivation/SlowstartTime.pdf}
\caption{Impact of the \texttt{slow start} parameter on performance and disk I/O in the shuffle phase (lower is better).}
\label{slow-start-disk-io}
\end{figure}


\subsection{I/O Hotspots}
Two resources are heavily stressed in MR computations and are therefore fundamental for a good performance: the disk and the network.
Various parameters in Hadoop are provided to optimize I/O but each of them is a trade-off. 
For example, increasing in-memory buffers for sorting are meant to reduce disk access. 
However, a larger buffer automatically means more memory per task is occupied and therewith less tasks can be executed in parallel on a node and respectively in the cluster.
We especially look at the effects of the \texttt{slow start} parameter as one important I/O control of Hadoop.
It was introduced to remove the contention on the network by starting the reducers earlier. 
This allows the reducers to fetch the intermediate results incrementally in parallel to map phase execution instead of executing a bulk data retrieval after the map phase.
In the following, we identify I/O hot spots in the execution model by looking at the impact of the slow start parameter.


\begin{figure}[t]
\centering
\includegraphics[width=3in]{plotting/motivation/SlowstartIO.pdf}
\caption{Impact of the \texttt{slow start} parameter on performance and disk I/O in the shuffle phase (lower is better).}
\label{slow-start-disk-io}
\end{figure}

\subsubsection{Disk}
\se{Provide a motivating experiment that shows the hit rate of the disk cache to be very low.}
The first disk access in computation occurs in the map task when intermediate results are stored.
Since, the intermediate results of a map task need to be sorted but might not all fit in memory, the map task spills them to disk. 
After the last record was processed, the task retrieves all so called \emph{spill files} from disk, merges them, performs a final sort and writes the result back to disk.
The second disk access occurs in the reduce phase because not all intermediate result data might fit into memory forcing the task once more to spill data to disk.
Although, on the first sight, the \texttt{slow start} parameter seems to have no impact on disk I/O performance, we nevertheless investigate it in this regard.
Therefore, we run a sort job from the HiBench benchmarking suite from Hadoop with the default configuration over a cluster over 20 slave nodes~\cite{5452747}.
The default data size input to a job is 3.2~GB per node and 64~GB in total.

The depicted variation originates from the operation in the disk access at the map task because disk access in the reduce phase did not change.
The valuable insight is that whenever an application writes data to disk, it is cached in the disk buffer of the operating system. 
In case of the intermediate result files of the map tasks, it accounts: the longer time span between the write and read access of the shuffle phase the higher the likelihood that the operating system removed it from its disk buffer.
Hence, the sooner the reducer fetches intermediate results from disk, the less disk access occurs.
Figure~\ref{slow-start-disk-io} portrays exactly this characteristic.
Respectively, we can conclude the following for optimizing disk I/O:
While disk I/O in the reduce phase can only be reduced by increasing the in-memory buffer of the reduce task itself, the disk I/O in the map task can be optimized by fetching intermediate results before they get evicted from the disk buffer of the operating system.

\begin{figure}[t]
\centering
\includegraphics[width=3in]{plotting/motivation/NetioRate.pdf}
\caption{Impact of the \texttt{slow start} parameter on performance and disk I/O in the shuffle phase (lower is better).}
\label{slow-start-disk-io}
\end{figure}

\subsubsection{Network}
\se{Show a simple experiment that depicts network traffic and cleanly identifies the network bottleneck.}
\se{Here we should perform the following experiment: Evaluate the slow start parameter. Show the time it takes to finish the job with varying slow start parameter in one graph. Then show a graph with lines for slow start enabled and disable in one graph. This allows to identify the network io pattern of a map reduce job.}
\se{This says how bad remote access is: \cite{DBLP:conf/ipps/TandonCW13} while this says that disk locality is irrelevant: \cite{Ananthanarayanan:2011:DDC:1991596.1991613}. The explanation in the introduction of \cite{DBLP:conf/ipps/TandonCW13} is a really great motivation for our work!}
Respectively, it seems that the advocated slow start parameter is a very good tuning nob for I/O performance.

\se{The above papers mainly address the initial retrieval. The following works try to address data shuffling which is where we want to be: ~\cite{6569843, 180155}}

\se{This work is the reference to cite for the shuffle bottleneck argument which is due to network oversubscription \cite{Costa:2012:CEI:2228298.2228302}}

\se{This work shows that Hadoop performance suffers badly with the increase of intermediate data: \cite{Rao:2012:SFL:2391229.2391233}}
