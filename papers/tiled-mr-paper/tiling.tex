\section{Tiled Map-Reduce}
\label{sec:tiling}
Based on these observations, this section introduces the concept of tiled execution to Map-Reduce. 
First, we present a brief introduction into the basic concepts of tiling before we describe the possibility for tiling in the Map-Reduce execution model without invading the Hadoop software stack.

\subsection{Tiling Concepts}
\se{This section must introduce the basic tiling concepts and the idea of tiling briefly and relate them to the Map Reduce context. The paragraph ends with the assumption that most MR jobs actually can benefit from tiling.}
The concept of \emph{tiling} originates from the field of compiler techniques \cite{Wolfe:1987:IST:645818.669220,Hogstedt:2003:PET:766617.766627} and primarily targets optimization of nested loops through data locality and enabling parallelism. 
The main architectural model is a single shared memory multiprocessor system, where the I/O to be optimized for data locality is especially the cache hierarchy of the processors. 
In turn, bad I/O performance is characterized by a high cache miss rate resulting in a lot of memory accesses.  
In our work, we consider a cluster of shared nothing processes.
Instead of considering fine-grained cache access, we focus on the more coarse-grained level of memory access.
Our goal is to avoid expensive disk access by utilizing the disk cache of the operating system stored in main memory as efficient as possible.

The basic idea underlying a tiled loop execution is to execute loops in a sliced form instead of iteration after iteration as described in the sequential program code such that the cache hit increases.
In the Map-Reduce execution model, the slow start parameter is a solution to increase the hit rate on the disk buffer and remove the additional disk access during the shuffle phase.
However, it has another very important drawback:
resource management in a Hadoop cluster is slot based, e.g. each machine gets assigned a number of slots for executing map and reduce task.
Starting reducers early means occupying additional slots for a time where actually no computation is performed.
Since MapReduce clusters often run different jobs from different users this is a serious impediment on job throughput and optimal resource allocation.

\begin{figure}[t]
\centering
\includegraphics[scale=0.45]{pictures/tiled-execution.pdf}
\caption{Tiled execution of a MapReduce job.}
\label{tiled-job-execution}
\end{figure}

Our concept for tiling in MR is depicted in Figure~\ref{tiled-job-execution}.
It splits the input data of a job into several \emph{tiles}, e.g. small parts of the input data, and executes one job for each of the tiles.
Throughout the paper we refer to these jobs as \emph{mini jobs}.
A mini job is a normal MR job and inherits its map, combine and reduce functionality straight from the original job.
The same accounts for the partition function.
In order to preserve the computational semantics of the MR execution, we break the computation into two phases. 
The first phase performs the map phase, which loads the tile data from the DFS, performs the \texttt{map} and finally executes \texttt{combine} not only over the output of a single map task but across all map tasks of a mini job.
We achieve this by registering the \texttt{combine} additionally as the \texttt{reduce} of the mini job.
In contrast to the \texttt{reduce}, the \texttt{combine} function must be associative and commutative.
Respectively, it can be executed multiple times over the intermediate results.
To \texttt{partition} the intermediate results, we store the output of the mini jobs into different directories in the distributed file system.
Note that this does not necessarily need to be the DFS where the input and the final results are stored.
It can be a different DFS, for example with a replication factor set to 1.
Finally, the second phase executes a job for each partition to perform the \texttt{sort} and \texttt{reduce}. 
The result data is stored at the location specified in the parent job.
It is true that the job in the second phase does not look like a typical MR job.
We delay this discussion to Section~\ref{sec:ramdoop} where we present the execution details.

\subsection{Tile Classification}
%\se{Identify the 3 job types for tiling and classify the above investigated jobs towards them.}
%\se{This must address the issue of how tiles are being constructed! What makes a good tile?}
We start our investigation of the I/O aspects for tiling in MR with the retrieval of the input data and the associated definition of a tile.
A tile can be composed of any number of input splits from the input data set.
Tile creation primarily has the goal to reduce I/O operations and therewith contribute to the overall goal of an I/O efficient computation.
The default policy in MapReduce is to favour data locality for map task, e.g. to assign map tasks to the nodes which store the according chunks.
This saves network bandwidth and reduces the retrieval of the input data to only use disk I/O.
In arguably this is the easiest strategy to allocate tiles of a predefined size.
A single tile would then consist of a set of input splits which are all located on the same node.
This \emph{local} tiling strategy works best for jobs that have no knowledge on the input data and therewith no notion of data inter-dependencies such as a word frequency count.
However, consider for example a page rank computation.
Input to this computation is the page rank vector and the link matrix.
The Map Reduce implementation consists of two jobs that are executed consecutively.
The first job spreads the page rank $r$ (at time $T-1$) of a website to the neighbours evenly using the degree vector $d$ of the outgoing vertexes.
\[
\vec{r}^{\;T} = \frac{\vec{r}^{\;T-1}}{\vec{d}}
\]
In the second, the final update to the page rank of a website $v$ is computed using the spread rank from its incoming neighbour vertexes (from the previous step) and a teleport probability $\alpha$.
\[
r(v)^T = \sum_n r_n^T * \alpha + (1 -\alpha)*\vec{E}
\]
The initial information vector $\vec{E}$ can be used to personalize the page rank computation.

In Figure~\ref{page-rank-tiling}, we depict two possibilities to map the page rank algorithm to our tiling model.
The naive approach transforms each of the two jobs individually to the model above.
Since no \texttt{combine} functionality is present the jobs of the first phase become map-only jobs and the reducers map straight to the reducers of the second phase in the tiled execution.
As a result the execution would be composed out of four consecutive jobs multiplied by the number of tiles of each of the two page rank jobs and the number of reduce partitions.
This strategy favours input locality similar to the tiling for the word count computation.
Nevertheless, the disk access between the two page rank jobs remains as well as the likeliness to hit the disk in between the map and reduce jobs of the tiled execution because no reduction on the data is performed.
Additionally, two shuffle phases are performed similar to the classic Hadoop execution.
At this point, we identify a short-coming of the MR programming model.
The MR programming model does not incorporate any notion of data co-location. That is, the job always assumes that data for a key is unknown and therewith potentially spread across the whole cluster. 
For example, although the computation of the new page rank vector is solely scoped by the computation on a single row, the programming paradigm assumes that these records are spread in an arbitrarily across the whole cluster.
However, this data is retrieved via the Nutch web crawler which is build on MapReduce jobs itself.
Therefore, it naturally stores the incoming links to a certain website as the outcome of a job as consecutive (and even ordered) values in HDFS or HBase.
Similarly, the second job assumes that the output of the first job is again spread arbitrarily across the whole cluster.
The map function is essentially a no-op.
It is only executed to use the grouping functionality to group the page rank spread for a given web site.
The actual processing only happens in the reduce phase.
If partitioning would also be applied on the output of the first job then the grouping operation in the second job would become as redundant as its map function.
After all both jobs belong to the same algorithm and should therefore benefit each other.
An advanced tiling strategy creates tiles composed of a set of rows from the matrix and the page rank vector.
As a result, the whole page rank computation can be performed in a single tiled execution saving intermediate disk access between the two page rank jobs and one complete shuffle phase.
In this case, it is better to retrieve the vector from remote and locate the job onto the node which stores the according records.
Fetching the page rank vector can be fast as it most likely remains in the OS disk buffer, because it is requested by many tile jobs.

\begin{figure}[t]
\centering
\includegraphics[scale=0.45]{pictures/page-rank-tiling.pdf}
\caption{Tiled executions for the page rank algorithm.}
\label{page-rank-tiling}
\end{figure}

Finally, consider the indexing job implemented in Nutch which produces an inverted index of the forward link information gathered during a web crawl.
The job requires input data from three different data sets: crawling meta data, information about the links and the raw data of the crawled segments.
Once again the map function is a no-op.
And although no global result over the whole data set is required, the job nevertheless shuffles the data in the cluster to the reduce nodes.
CoHadoop is a prominent recent approach to argue for colocation of data~\cite{Eltabakh:2011:CFD:2002938.2002943}.
If the data of the three data sets would be colocated with respect to their keys, our tiled execution model adds the final part to perform nutch indexing without any data shuffling.

\subsection{I/O - Benefits of a Tiled Execution}
Each phase performs local I/O while shuffling is performed implicitly in between the two phases.

Note that many tiles might reside on the same node.
In order to increase the disk buffer hit rate two aspects need to be taken into account: i) the scheduler needs to execute tile jobs on the same node sequentially and ii) the \emph{tile size} needs to be adapted in such a way that the intermediate results of a tile job fit into the disk buffer.
